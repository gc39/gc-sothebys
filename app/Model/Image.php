<?php

class Image extends Model {   
     
    public $useTable = 'image';
        
    public function getImageFilename($id) {       
        
        $clientFolder = '';
        
        if(!empty($id) && $id != 0) {
            
            $image    = $this->findById($id);                
            $filename = $image ? $clientFolder . DS . $image['Image']['filename'] :  'no_image.gif';
            
        } else {
            $filename = '/no_image.gif';
        }
        
        return $filename;
    }
  
    
}