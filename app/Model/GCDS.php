<?php

class GCDS extends Model {   
     
    public function load($file) {
                
        try {
            $xml = Xml::build($file);
        } catch (XmlException $e) {            
            return false;
        }
       
        return Xml::toArray($xml);
        
    }       
    
    public function getClientSubdomain() {
        
        $host = isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : false;
        
        if($host) {
            
            $domainParts = explode('.', $_SERVER['HTTP_HOST']);
            
            if(count($domainParts) >= 3) {
                return $domainParts[0];
            } else {
                return false;
            }
            
        } else {
            return false;
        }
        
    }
    
    public function getPlayers($GCDS) {      
        
        foreach($GCDS as $module) {
            
            if(isset($module['gcdb']['onelan']['players']['player'])) {               
                
                if(is_array($module['gcdb']['onelan']['players']['player'])) {
                    foreach ($module['gcdb']['onelan']['players']['player'] as $player) {
                        $players[] = $player;
                    }
                } else {
                    $players[] =$module['gcdb']['onelan']['players']['player'];                    
                }    
            }
        }

        
        return isset($players) ? $players : false;
    }
    
    
    
    
    public function getAllMoldulesImagesFields($GCDS) {
     
        $imagesFieldsKeys = false;
        
        foreach($GCDS as $key => $module) {
            if(isset($module['gcdb']['fields']['field'])) {
                foreach($module['gcdb']['fields']['field'] as $field) {
                    if(isset($field['@key']) && isset($field['@type']) && $field['@type'] == 'image') {
                        $imagesFieldsKeys[$key][] = $field['@key'];
                    }
                }
                    
            }
        }
        
        return $imagesFieldsKeys;
        
    }
    
    public function getModuleNameByKey($findKey, $GCDS) {
   
        foreach($GCDS as $key => $module) {
            if($findKey == $key) {                
                return $module['gcdb']['content_name'];                                    
            }
        }
        
        return '';
        
    }
}