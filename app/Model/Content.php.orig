<?php

class Content extends Model {   
    
    public function getSchema($module) {
               
        $this->useTable = $module;
        return $this->schema();
        
    }    
     
    
    public function getAllRows($module, $order, $fieldsData, $page, $rows_per_page, &$pages) {
               
        $this->useTable = $module;
              
        $contentCount = $this->find('count');        
        
        $pages= ceil($contentCount/$rows_per_page);
        
        if($page == 1) {    
            $start = 0;
        } else {
            $start = $rows_per_page * ($page - 1);
        }
    
        
        if($order) {
            $content = $this->find('all', array('order' => $order, 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
        } else {
            $content = $this->find('all', array('order' => 'id ASC', 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
        }

        return $content;
        
    }
    
    public function getLiveRelatedContent($module, $conditions, $fieldID, $fieldShow) {
               
        $this->useTable = $module;
      
        $content     = $this->find('all', array('conditions' => $conditions, 'order' => 'id ASC'));
        $returnArray = array();
        
        foreach ($content as $row) {            
            $returnArray[] = array('itemName' => '['.$row['Content'][$fieldID].'] '.$row['Content'][$fieldShow], 'value' => $row['Content'][$fieldID]);
        }

        return $returnArray;
        
    }       
    
    public function validateUnique($module, $id, $field, $value) {
               
        $this->useTable = $module;
      
        $conditions = array('id <> '.$id. ' AND '.$field. ' = '.$value);
        
        return $this->find('all', array('conditions' => $conditions));
     
        
    }       

    public function getDistinct($module, $field) {
        
        $this->useTable = $module;   
        return $this->find('all', array('fields' => array('DISTINCT ('.$field.') AS '.$field) ));
        
    }
    
    public function findAllRows($searchTerm, $module, $order, $fieldsData, $page, $rows_per_page, &$pages) {
        
        $query = false;
        
        $this->useTable = $module;
        
         
        
        foreach($fieldsData as $field) {   
            
            if($module == 'app_novedades'){
                 
                if(isset($field['@db-type']) && (($field['@type'] == 'text') || ($field['@type'] == 'textarea') || ($field['@type'] == 'richtext') || ($field['@type'] == 'date') || (($field['@type'] == 'select') && $field['@key'] != 'send_notification') )) {               
                    $query[] = array($field['@key'].' LIKE' => '%'.$searchTerm.'%');    
                }   
            
            }else{
                if(isset($field['@db-type']) && (($field['@type'] == 'text') || ($field['@type'] == 'textarea') || ($field['@type'] == 'richtext') || ($field['@type'] == 'date') || ($field['@type'] == 'select') )) {               
                    $query[] = array($field['@key'].' LIKE' => '%'.$searchTerm.'%');
                }  
            }        
        }

        if(is_array($query)) {    
            $contentCount = $this->find('count', array('conditions' => array('OR' => $query)));           
        } else {        
            $contentCount = $this->find('count');         
        }
        
        $pages= ceil($contentCount/$rows_per_page);
        
        if($page == 1) {    
            $start = 0;
        } else {
            $start = $rows_per_page * ($page - 1);
        }
        
        
        if(is_array($query)) {
        
            if($order) {
                $content = $this->find('all', array('order' => $order, 'conditions' => array('OR' => $query), 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
            } else {
                $content = $this->find('all', array('order' => 'id ASC', 'conditions' => array('OR' => $query), 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
            }
         
            
        } else {        
            if($order) {
                $content = $this->find('all', array('order' => $order, 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
            } else {
                $content = $this->find('all', array('order' => 'id ASC', 'limit' => $rows_per_page, 'page' => $page,'offset' => $start));
            }
        }

        return $content;
        
    }   
    
    public function getById($module, $id, $fieldsData = false) {
               
        $this->useTable = $module;

        return $this->findById($id);
        
    }          
    
    public function getFieldTypeByKey($key, $fieldsData) {
        
        foreach($fieldsData as $field) {            
            if($field['@key'] == $key) {
                return $field['@type'];
            }
        }        
        
        return false;
        
    }
    
    public function customSave($conditions, $module) {
        
        $this->useTable = $module;
        
        return $this->save($conditions);
                
    }
       
     public function getItemModifications($module, $date, $service, $withService, $date_limit = false) {
        
        $this->useTable = $module.'_menu_diario_reportes';
               
        $shortDate = date('d-m-Y', strtotime($date));
        
       // echo $date_limit . '->' . $date.'<br>';
        
        if($date_limit === false) {
            
            if($withService) {
                $query = $this->find('all',                 
                    array('fields' => array( '*'), 
                        'conditions' => array(       
                            'servicio = "'.$service.'"',
                            'fecha = "'.$shortDate.'"',
                            'modified < TIMESTAMP("'.$date.'")',
                            'created <> modified'
                        )
                    )
                );
            } else {
                $query = $this->find('all',                 
                    array('fields' => array( '*'), 
                        'conditions' => array(                            
                            'fecha = "'.$shortDate.'"',
                            'modified < TIMESTAMP("'.$date.'")',
                            'created <> modified'
                        )
                    )
                );                
            }
            
        } else {
            
            if($withService) {
                $query = $this->find('all',                 
                    array('fields' => array( '*'), 
                        'conditions' => array(
                            'servicio = "'.$service.'"',
                            'fecha = "'.$shortDate.'"',
                            'modified BETWEEN TIMESTAMP("'.$date_limit.'") AND TIMESTAMP("'.$date.'")',
                            'created <> modified'
                        )
                    )
                );
            } else {
                $query = $this->find('all',                 
                    array('fields' => array( '*'), 
                        'conditions' => array(                            
                            'fecha = "'.$shortDate.'"',
                            'modified BETWEEN TIMESTAMP("'.$date_limit.'") AND TIMESTAMP("'.$date.'")',
                            'created <> modified'
                        )
                    )
                );                
            }    
        }
        
//        echo '<pre>';
//        echo $date.'<br>';
//        print_r($query);
//        echo '</pre>';       
//        die();
        
        return $query;
    }  
    
    
    public function getNoPublication($module, $date, $service, $withService, $date_limit = false) {
        
        App::import('Model', 'PublishHistory');
   
        $PublishHistory = new PublishHistory();
        
        $this->useTable = $module.'_menu_diario_reportes';
               
        $shortDate = date('d-m-Y', strtotime($date));
              
        if($withService) {
            $items = $this->find('all',                 
                array('fields' => array( '*'), 
                    'conditions' => array(
                        'servicio = "'.$service.'"',
                        'fecha = "'.$shortDate.'"'                        
                    )
                )
            );
        } else {
            $items = $this->find('all',                 
                array('fields' => array( '*'), 
                    'conditions' => array(                        
                        'fecha = "'.$shortDate.'"'                        
                    )
                )
            );           
        }

        $nopubitems = 0;
        
        foreach ($items as $item) {                   
            if(!$PublishHistory->havePublicationInPeriod($module.'_menu_diario', $item['Content']['modified'], $date)) {
                $nopubitems++;
            }
        }
        
        return $nopubitems;
    }  
    
    
    public function getFirstUploadMenuItem($service, $module, $date, $withService) {
        
        $this->useTable = $module;

        if($withService)  {
            return $this->find('first', 
                array('fields' => array( '*'), 
                    'conditions' => array(
                        'fecha = "'.$date.'"',
                        'servicio = "'.$service.'"'
                    ),
                    'order' => array('created ASC'),
                    'limit' => 1
                )
            );
        } else {
            return $this->find('first', 
                array('fields' => array( '*'), 
                    'conditions' => array(
                        'fecha = "'.$date.'"',                  
                    ),
                    'order' => array('created ASC'),
                    'limit' => 1
                )
            );
        }
    }
    
    public function customCreate($module) {
        
        $this->useTable = $module;
        
        $this->create();
        
        return true;
        
    }
    
    public function customDeleteAll($args, $module) {
        
        $this->useTable = $module;
        $this->deleteAll($args);
        
    }
    
   
    
    public function importFromCSV($file_path, $delete_all, $module) {
        
        $this->useTable = $module;
        
        if(empty($file_path)) {
            return 'FILE_NOT_FOUND';
        }
        
        $no_errors     = false;
        $return_status = true;        
        $colums_map    = array('C1' => 'dia', 'C2' => 'mes', 'C3' => 'nombre', 'C4' => 'cargo');        
        $month_names   = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
        
        ini_set("auto_detect_line_endings", true);
        
        if (($gestor = fopen($file_path, "r")) !== FALSE) {
            
            $row = 1;            
            
            while (($datos = fgetcsv($gestor, 1000, ";")) !== FALSE) {
                
                if($row == 1) {

                    $columns = count($datos);                

                    //pr($datos);
                      
                    for ($c = 0; $c < $columns; $c++) {
                        if(strtolower(trim($datos[$c])) == 'mes') { $month_index = $c; }
                        if(strtolower(trim($datos[$c])) == 'dia') { $day_index = $c; }
                        if(strtolower(trim($datos[$c])) == 'nombre') { $name_index = $c; }                        
                        if(strtolower(trim($datos[$c])) == 'cargo') { $position_index = $c; }                        
                    }

                } elseif(count($datos) >= 3) {

                    if(!isset($month_index) || !isset($day_index) || !isset($name_index)) {
                        $return_status = 'MISSING_COLUMNS';
                    } else {
                        
                      // pr($datos);

                        //pr($datos[0]);
                        $month = trim($datos[$month_index]);
                        $day   = trim($datos[$day_index]);
                        $name  = trim($datos[$name_index]);
 
                        if(!empty($month) && !empty($day) && !empty($name)) {
                            
                            $position  = isset($position_index) ? trim($datos[$position_index]) : '';
                            
                            if(!in_array(strtolower($month), $month_names)) {
                                $return_status = 'INVALID_DATA';
                            } else {
                                if(!is_numeric($day)) {
                                    $return_status = 'INVALID_DATA';
                                } else {
                                    if(empty($name)) {
                                        $return_status = 'INVALID_DATA';
                                    } else {
                                        // todo ok    
                                        $no_errors = true;
                                                                                       
                                    }

                                }
                            }
                        } 
                    }

                }


                $row++;
            }
            
            fclose($gestor);

            if($no_errors) {
                
                if($delete_all) {                    
                    $this->customDeleteAll(array('id <> 0'), $module);
                }
                
                $gestor = fopen($file_path, "r");
                $row    = 1;
                
                while (($datos = fgetcsv($gestor, 1000, ";")) !== FALSE) {

                    if($row != 1 && count($datos) >= 3) {

                        $month = trim($datos[$month_index]);
                        $day   = trim($datos[$day_index]);
                        $name  = trim($datos[$name_index]);
 
                        if (!preg_match('!!u', $name)) {
                            $name  = utf8_encode($name);                           
                        }
                                                
                        if(!empty($month) && !empty($day) && !empty($name)) {
                            
                            $position  = isset($position_index) ? trim($datos[$position_index]) : '';
                            
                            if (!preg_match('!!u', $position)) {
                                $position  = utf8_encode($position);                           
                            }                        
                        
                            $id_row = $this->generateID($module);

                            $this->create();
                            $this->save(array('id_row' => $id_row, 'key' => 'C1', 'value' => $day));
                            $this->create();
                            $this->save(array('id_row' => $id_row, 'key' => 'C2', 'value' => ucwords($month)));
                            $this->create();
                            $this->save(array('id_row' => $id_row, 'key' => 'C3', 'value' => ucwords($name)));
                            $this->create();
                            $this->save(array('id_row' => $id_row, 'key' => 'C4', 'value' => ucwords($position)));
                        }
                    }

                    $row++;
                    
                } 
                
                fclose($gestor);
                
            }
            
           
            
        } else {
            $return_status = 'FILE_NOT_FOUND';
        }
        
        return $return_status;
    }
    
    
    public function importFromCSVGeneric($file_path, $delete_all, $module, $map, $separator = ';') {
        
        $this->useTable = $module;
        
        if(empty($file_path)) {
            return 'FILE_NOT_FOUND';
        }                      
        
        $no_errors       = true;
        $return_status   = true;        
        $missing_columns = false;
        
        foreach ($map['map'] as $mapItem) {
            
            $mapColumnsIndexs[strtolower($mapItem['@column'])] = '';
            $mapColumnsDB[strtolower($mapItem['@column'])]     = $mapItem['@field'];
            
            if($mapItem['@optional'] == 'no') {
                $requiredColumns[strtolower($mapItem['@column'])] = strtolower($mapItem['@column']);
            }            
        }
        
        
        ini_set("auto_detect_line_endings", true);
        
        if (($gestor = fopen($file_path, "r")) !== FALSE) {
            
            $row = 1;            
            
            while (($datos = fgetcsv($gestor, 3000, $separator)) !== FALSE) {
                
                if($row == 1) {

                    $columns = count($datos);                

                    if($columns == 1) {
                        $missing_columns = true;
                        $no_errors       = false;   
                        $return_status = 'MISSING_COLUMNS_SEPARATOR';  
                    }
                    
                    for ($c = 0; $c < $columns; $c++) {
                        if(isset($mapColumnsIndexs[strtolower(trim($datos[$c]))])) { 
                            $mapColumnsIndexs[strtolower(trim($datos[$c]))] = $c;                             
                        }                   
                    }
                    
                    foreach ($requiredColumns as $requiredColumn) {
                        if(trim($mapColumnsIndexs[$requiredColumn]) == '') {
                           $missing_columns = true;
                           $no_errors       = false;                                   
                        }
                    }
                        
                    
                    
                } elseif(count($datos) > 1) {

                    if($missing_columns) {                        
                        $return_status = 'MISSING_COLUMNS';                           
                    } else {                                             
                                               
                        if($columns != count($datos)) {   
                            $return_status = 'BAD_COLUMN_NUMBERS';     
                            $no_errors     = false;                                     
                        }    
                    }
                } 
                
                $row++;
                

            }
                  
            fclose($gestor);         

            if($no_errors) {
                               
                if($delete_all) {                    
                    $this->customDeleteAll(array('id <> 0'), $module);
                }
                
                $gestor = fopen($file_path, "r");
                $row    = 1;
                
                while (($datos = fgetcsv($gestor, 1000, $separator)) !== FALSE) {

                    if($row != 1) {

                        $saveArray = array();
                        
//                        echo '<pre>';
//                                print_r($map['map']);
//                                echo '</pre>'     ;
//                                die();
//                                   
                                
                        foreach ($mapColumnsDB as $column => $keyDB) {
                            
                            if( isset($datos[$mapColumnsIndexs[$column]])) {       
                              
                                $value = trim($datos[$mapColumnsIndexs[$column]]) == '' ? ' ' : trim($datos[$mapColumnsIndexs[$column]]);                                
                                
                            } else {
                                $value = '';
                            }  

                                  
                            if(!preg_match('!!u', $value)) {
                                $value = utf8_encode($value);                           
                            }     
                            
                            if($column == '_db_order') {
                                $value = $row - 1;
                            }                                                       

                            foreach ($map['map'] as $field) {
                                
                                if((isset($field['@replace']) && isset($field['@replace-with'])) && ($keyDB == $field['@field'])) {
                                    $saveArray[$keyDB] = str_replace($field['@replace'], $field['@replace-with'], $value);
                                } else {
                                    $saveArray[$keyDB] = $value;
                                }
                                
                            }
                                              
                        }   
                      
                        $this->create();
                        $this->save($saveArray);
                        
                    }

                    $row++;
                    
                } 
                
                fclose($gestor);
                
            }
            
        } else {
            $return_status = 'FILE_NOT_FOUND';
        }             
        
        return $return_status;
    }     
    
    public function importFromCSVGeneric2($file_path, $delete_all, $module, $map, $separator = ';') {
        
        $this->useTable = $module;
        
        if(empty($file_path)) {
            return 'FILE_NOT_FOUND';
        }                      
        
        $no_errors       = true;
        $return_status   = true;        
        $missing_columns = false;
        
        foreach ($map['map'] as $mapItem) {
            
            $mapColumnsIndexs[strtolower($mapItem['@column'])] = '';
            $mapColumnsDB[strtolower($mapItem['@column'])] = $mapItem['@field'];
            
            if($mapItem['@optional'] == 'no') {
                $requiredColumns[strtolower($mapItem['@column'])] = strtolower($mapItem['@column']);
            }            
        }
        
        
        ini_set("auto_detect_line_endings", true);
        
        if (($gestor = fopen($file_path, "r")) !== FALSE) {
            
            $row = 1;            
            
            while (($datos = fgetcsv($gestor, 3000, $separator)) !== FALSE) {
                
                if($row == 1) {

                    $columns = count($datos);                

                    if($columns == 1) {
                        $missing_columns = true;
                        $no_errors       = false;   
                        $return_status = 'MISSING_COLUMNS_SEPARATOR';  
                    }
                    
                    for ($c = 0; $c < $columns; $c++) {
                        if(isset($mapColumnsIndexs[strtolower(trim($datos[$c]))])) { 
                            $mapColumnsIndexs[strtolower(trim($datos[$c]))] = $c;                             
                        }                   
                    }
                    
                    foreach ($requiredColumns as $requiredColumn) {
                        if(trim($mapColumnsIndexs[$requiredColumn]) == '') {
                           $missing_columns = true;
                           $no_errors       = false;                                   
                        }
                    }
                        
                    
                    
                } elseif(count($datos) > 1) {

                    if($missing_columns) {                        
                        $return_status = 'MISSING_COLUMNS';                           
                    } else {                                             
                                               
                        if($columns != count($datos)) {   
                            $return_status = 'BAD_COLUMN_NUMBERS';     
                            $no_errors     = false;                                     
                        }    
                    }
                } 
                
                $row++;
                

            }
                  
            fclose($gestor);         

            if($no_errors) {
                               
                if($delete_all) {                    
                    $this->customDeleteAll(array('id <> 0'), $module);
                }
                
                $gestor = fopen($file_path, "r");
                $row    = 1;
                
                while (($datos = fgetcsv($gestor, 1000, $separator)) !== FALSE) {

                    if($row != 1) {

                        $saveArray = array();
                        
                 //       echo '<pre>';
                 //       print_r($map['map']);
//                /        echo '<hr>';
//                        print_r($mapColumnsIndexs);
                    //    echo '<hr>';
                       
//                                   
                                
                        foreach ($mapColumnsDB as $column => $keyDB) {
                            
                            if( isset($datos[$mapColumnsIndexs[$column]])) {       
                              
                                $value = trim($datos[$mapColumnsIndexs[$column]]) == '' ? ' ' : trim($datos[$mapColumnsIndexs[$column]]);
                                        
                                
                                if(isset($map['map'][$mapColumnsIndexs[$column]]['@replace']) && isset($map['map'][$mapColumnsIndexs[$column]]['@replace-with'])) {                        
                                    
//                                    //print_r($map['map'][$mapColumnsIndexs[$column]]);
//                                   
//                                    
//                                    //if(isset($_GET['sss'])) {
//                                       // die('reemplazar'.$column);
//                                   // }
//                                    
//                                    echo '<hr>';
//                                    
//                                    echo $map['map'][$mapColumnsIndexs[$column]]['@column'].':';
//                                    echo $value.'|';
//                                    
//                                    $value = str_replace($map['map'][$mapColumnsIndexs[$column]]['@replace'], $map['map'][$mapColumnsIndexs[$column]]['@replace-with'], $value);                                                                     
//                                    
//                                    echo $value ;                                    
//                                    echo '<hr>';
                                    
                                }
                                
                            } else {
                                $value = '';
                            }  

                                  
                            if(!preg_match('!!u', $value)) {
                                $value = utf8_encode($value);                           
                            }     
                            
                            if($column == '_db_order') {
                                $value = $row - 1;
                            }                                                       

                            foreach ($map['map'] as $field) {
                                
                                if((isset($field['@replace']) && isset($field['@replace-with'])) && ($keyDB ==$field['@field'])) {
                                    $saveArray[$keyDB] = str_replace($field['@replace'], $field['@replace-with'], $value);
                                } else {
                                    $saveArray[$keyDB] = $value;
                                }
                                
                            }
                            
                            
                            
                            //print_r($map['map'][$mapColumnsIndexs[$column]]);
                            
                                                     
                        }   
                        
                     //   print_r($saveArray);
                     //   echo '</pre>';
                      //  die();
                      
                        $this->create();
                        $this->save($saveArray);
                        
                    }

                    $row++;
                    
                } 
                
                fclose($gestor);
                
            }
            
        } else {
            $return_status = 'FILE_NOT_FOUND';
        }             
        
        //die($return_status);
        return $return_status;
    }    
  
    public function compareFields($fields_A, $fields_B) {
        
  
        if(count($fields_A) != count($fields_B)) {       
            return false;            
        }
        
        foreach ($fields_A as &$field) {
            $maxLength   = isset($field['@maxlength']) ? $field['@maxlength'] : '';
            $compare_A[] = array($field['@key'], $field['@type'], $field['@label'], $maxLength);
        }
        
        foreach ($fields_B as &$field) {
            $maxLength   = isset($field['@maxlength']) ? $field['@maxlength'] : '';
            $compare_B[] = array($field['@key'], $field['@type'], $field['@label'], $maxLength);
        }
        
        if($compare_A == $compare_B) {
            return true;
        } else {
            return false;
        }
        
    }
    
    public function generatePreview($module, $clientFolder, $moduleData) {
        $this->generate($module, $clientFolder, 'test', $moduleData);
    }
    
    public function generatePublish($module, $clientFolder, $idPublish, $moduleData) {
        $this->generate($module, $clientFolder, 'prod', $moduleData , $idPublish);
      //  if($module == 'pf_talca_menu_diario'){
      //      $this->generate($module, $clientFolder, 'mobile', $moduleData , $idPublish);
      //  }
    }
    
    private function generate($module, $clientFolder, $mode, $moduleData, $idPublish = 0) {
                
        $this->useTable = $module;            
        
        $content = $this->find('all', array('order' => 'id ASC'));       
             
        if($content) {
            $content = Set::classicExtract($content,'{n}.Content');       
            $content = $this->getImagesRelatedContent($content, $module, $moduleData);
        }
                
        $resources = $content['resources'];
        $content   = json_encode($content['content']);
   
        file_put_contents(WWW_ROOT . 'files' . DS . $clientFolder .DS . 'templates'. DS . 'json' . DS . $mode . DS . $module . '-data.json', $content);
        
        if($mode == 'prod') {
            
            @mkdir(WWW_ROOT . 'files' . DS . $clientFolder .DS . 'publications'. DS . $idPublish);
            file_put_contents(WWW_ROOT . 'files' . DS . $clientFolder .DS . 'publications'. DS . $idPublish . DS . $module . '-data.json', $content);
            
            foreach ($resources as $image) {                      
                copy(WWW_ROOT . 'files' . DS . $clientFolder .DS . $image, WWW_ROOT . 'files' . DS . $clientFolder .DS . 'publications'. DS . $idPublish . DS . $image);
            }
        }
        
    }
    
    function getImagesRelatedContent($content, $module, $moduleData) {
        
        App::import('Model', 'Image');
   
        $ImageModel = new Image();
          
        $imagesFields = array();
        $imageList    = array();
        
        foreach ($moduleData['fields']['field'] as $field) {
            if(isset($field['@type']) && $field['@type'] == 'image') {
                $imagesFields[] = $field['@key'];
            }
        }
        
        foreach ($content as &$row) {
            foreach ($imagesFields as $imageField) {
                $filename         = str_replace('/', '', $ImageModel->getImageFilename($row[$imageField]));
                $imageList[]      = $filename == 'no_image.gif' ? '' : $filename;
                $row[$imageField] = $filename == 'no_image.gif' ? '' : $filename;
            }
        }
        
        foreach ($moduleData['fields']['field'] as $field) {
            if(isset($field['@type']) && $field['@type'] == 'static-image') {
                $imagesFields[] = $field['@key'];
            }
        }
        
        foreach ($content as &$row) {
            foreach ($imagesFields as $imageField) {
                $filename         = str_replace('/', '', $row[$imageField]);
                $imageList[]      = $filename == 'no_image.gif' ? '' : $filename;
                $row[$imageField] = $filename == 'no_image.gif' ? '' : $filename;
            }
        }
               
        return array('content' => $content, 'resources' => $imageList);        
        
    }    
    
}