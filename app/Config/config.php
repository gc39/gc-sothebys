<?php

define('AMB', 'dev');
//define('AMB', 'prod');

define('CLIENT', 'sothebys');
define('ENABLESENDMAIL', false);

$CLIENT = CLIENT;
$AMB = (AMB == 'dev') ? 'dev' : 'digitalboard';

define('STANDALONE_MODE', false);

define('MODULES_CONFIG_PATH', ROOT . DIRECTORY_SEPARATOR . APP_DIR . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR);

define('ROWS_PER_PAGE', 3000);
define('ROWS_PER_PAGE_IN_MEDIA', 32);
define('ROWS_PER_PAGE_IN_MEDIA_POPUP', 8);
define('SESSION_TIME_MINS_MODULE', 120);

define('CARPETA_SHELL_EXEC', "/home/$AMB/public_html/sub_dominios/$CLIENT/app/Shell_exec/");
define('CARPETA_CRON_JOBS', "/home/$AMB/public_html/sub_dominios/$CLIENT/app/Cron_jobs/");
define('PHP_RUTA', '/usr/local/bin/php');

// define('URLAPI', '');

define('LOCALSERVER', 'localhost');
define('LOCALUSERDB', 'dev_us');
define('LOCALPASSDB', 'q1w2e3r4t5_01');
define('LOCALDB', 'dev_sothebys');

define('RUTAORIGENCARPETASCRONJOB', "/home/dev/public_html/sub_dominios/$CLIENT/app/webroot/files/$CLIENT/templates_images");
define('RUTAORIGENLOGOS', "/home/dev/public_html/sub_dominios/$CLIENT/app/webroot/files/$CLIENT/templates/img/logos/");
