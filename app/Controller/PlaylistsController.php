<?php
 
App::uses('AppController', 'Controller');

class PlaylistsController extends AppController {

    public  $uses    = array('GCDS', 'DSM', 'Playlist', 'PlaylistItem');
    private $Modules = false;
    
    public function beforeFilter() {        
        
        parent::beforeFilter();        
        
        $userLogged = parent::checkLogged();
        
        if(isset($userLogged)){
			
			$this->Modules = parent::initModules();  
            $client = $this->GCDS->getClientSubdomain();
		   
			if(!$this->Modules) {
				die('Error al cargar el XML de configuracion de modulos');
			}			
            
			$modulesMenu = parent::getModulesMenu($this->Modules);
			
			$this->set('modulesMenu', $modulesMenu);                
			$this->set('userLogged', $userLogged);    
            $this->set('client', $client);
			$this->set('viewTitle', 'Playlists');  
		}
        
    }      

    public function index($id = false) {

        $playlistAvailables = $this->Playlist->find('all');
        
        $this->set('playlistAvailables', $playlistAvailables);                 
        
        if($id === false) { 
            
            $this->set('currentPlaylist', false);   
       
            return false;
          
        }
        
        $currentPlaylist = $this->Playlist->findById($id);
        
        if(!empty($currentPlaylist)) {
            $currentPlaylistItems = $this->PlaylistItem->findAllByPlaylistId($id, array(), array('order'  => 'asc'));
        }
        
        $this->set('currentPlaylistItems', !empty($currentPlaylistItems) ? $currentPlaylistItems : array());   
        $this->set('currentPlaylist', !empty($currentPlaylist) ? $currentPlaylist : false);   
    
    }
    
    
}