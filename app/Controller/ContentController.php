<?php
// paris
App::uses('AppController', 'Controller');

class ContentController extends AppController
{

    public  $uses       = array('Content', 'Image', 'ImageCrop', 'PublishHistory', 'PublishPlayerStatus', 'PublishAlerts', 'PublishAlertsLogs', 'GCDS');
    private $modules    = false;
    private $moduleKey  = false;
    private $moduleData = false;

    public function beforeFilter()
    {

        parent::beforeFilter();

        $userLogged = parent::getLoggedUser();

        if (isset($_GET['fromJOB']) || is_array($userLogged)) {

            $this->modules = parent::initModules();

            if (!$this->modules) {
                die('Error al cargar el XML de configuracion de modulos');
            }

            // Consulta los modulos que son corporativos por base de datos
            // $server_db = LOCALSERVER;
            // $user_db = LOCALUSERDB;
            // $password_db = LOCALPASSDB;
            // $db_db = LOCALDB;

            // $current_url = $_SERVER["REQUEST_URI"];
            // $current_url_array = explode('/', $current_url);
            // $current_url = $current_url_array[1];


            // $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
            // if (!$obj_conexion) {
            //     echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
            //     die();
            // }

            // $select_qry = 'SELECT config_modulos.id AS id, config_modulos.tienda AS tienda FROM config_modulos WHERE config_modulos.id NOT IN (SELECT id FROM config_excepciones_compartidos)';

            // if (!$resultado = $obj_conexion->query($select_qry)) {
            //     echo "Lo sentimos, este sitio web está experimentando problemas I.";
            //     die();
            // }
            
            // $jobs_modulos = array();
            // while ($registro = $resultado->fetch_assoc()) {
            //     $tmp_array = array();
            //     $tmp_array[] = $registro['tienda'];
            //     $jobs_modulos[] = $tmp_array;
            // }
            // $this->set('modules_publicacionmultiple', $jobs_modulos);
            // $obj_conexion->close();

            // Fin de la consulta

            $modulesMenu = parent::getModulesMenu($this->modules);
            $client = $this->GCDS->getClientSubdomain();

            $this->moduleKey = isset($this->request->params['pass'][0]) ? $this->request->params['pass'][0] : false;
            $this->moduleKey = (!$this->moduleKey && isset($this->request->params['module'])) ? $this->request->params['module'] : $this->moduleKey;

            if (!isset($this->modules[$this->moduleKey])) {
                die('Modulo no configurado');
            } else {
                $this->moduleData = $this->modules[$this->moduleKey];
            }

            if (!isset($_GET['fromJOB']) && ($userLogged['User']['rol'] != 'PLATFORM_ADMIN' && !in_array($this->moduleKey, $userLogged['User']['modules']['view']))) {
                return $this->redirect('/users/login');
            } else {

                $this->set('userLogged', $userLogged);
                $this->set('client', $client);
                $this->set('modulesMenu', $modulesMenu);
                $this->set('activeMenu', $this->moduleKey);
                $this->set('moduleKey', $this->moduleKey);
                $this->set('viewTitle', $this->moduleData['gcdb']['content_name']);
            }
        } else {

            if ($this->request->is('ajax')) {
                die('NO_LOGGED');
            } else {
                return $this->redirect('/users/login');
            }
        }
    }

    public function uploadLocal()
    {
        $this->layout = 'ajax';
        $this->autoRender = false;

        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {

                $status = array(
                    'success' => false,
                    'message' => 'No se pudo resolver el cliente'
                );
            }
        } else {
            $clientFolder = '';
        }

        if (isset($_FILES['file']['tmp_name'])) {
            // filename
            $file_name = $_FILES['file']['name'];

            // sourcefile
            $temp_file_location = $_FILES['file']['tmp_name'];

            // destination
            $dest_file = '/home/digitalboard/public_html/sub_dominios/' . $clientFolder . '/app/webroot/files/' . $clientFolder . '/templates_images_test/medioscompartidos/' . $file_name;

            copy($temp_file_location, $dest_file);
            echo $dest_file;
        }
    }

    public function index($module, $page = 1)
    {

        $isSearch     = false;
        $searchTerm   = '';

        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {
                die('No se pudo determinar el subdominio del cliente');
            }
        } else {
            $clientFolder = '';
        }

        if (isset($this->request->data['import_from_csv'])) {

            $delete_all    = isset($this->request->data['delete_all']) ? true : false;
            // die($this->request->data['field_separator']);

            if (isset($this->moduleData['gcdb']['import_map'])) {

                if ($clientFolder != 'demo') {
                    $status_import = $this->Content->importFromCSVGeneric($this->params['form']['file']['tmp_name'], $delete_all, $module, $this->moduleData['gcdb']['import_map'], $this->request->data['field_separator']);
                } else {
                    $status_import = $this->Content->importFromCSVGeneric2($this->params['form']['file']['tmp_name'], $delete_all, $module, $this->moduleData['gcdb']['import_map'], $this->request->data['field_separator']);
                }
            } else {
                $status_import = $this->Content->importFromCSV($this->params['form']['file']['tmp_name'], $delete_all, $module);
            }

            if ($status_import !== true) {
                if ($status_import == 'MISSING_COLUMNS_SEPARATOR') {
                    $message = 'Faltan columnas, realice otro intento seleccionado otro separador de columnas.';
                }
                if ($status_import == 'MISSING_COLUMNS') {
                    $message = 'Faltan columnas, lea con atencion las columnas requeridas en la ventana de importación';
                }
                if ($status_import == 'FILE_NOT_FOUND') {
                    $message = 'Error al subir el archivo';
                }
                if ($status_import == 'INVALID_DATA') {
                    $message = 'Hay campos con datos inválidos';
                }
                if ($status_import == 'BAD_COLUMN_NUMBERS') {
                    $message = 'Número de columnas encontradas no coinciden con lo esperado';
                }
            }else{
                
                $message = 'Se importo el CSV con éxito, debe publicar el contenido para verlo reflejado en las pantallas.';
            }

            $this->Session->setFlash($message, 'flash_custom');
        }

        if (isset($this->request->data['duplicate-action'])) {

            if ($this->request->data['id-row-duplicate'] && $this->request->data['duplicate-destination']) {

                foreach ($this->moduleData['gcdb']['fields']['field'] as $field) {
                    if ($field['@key'] != 'id') {
                        $fields[] = $field['@key'];
                    }
                }

                $fields[] = 'created';
                $fields[] = 'modified';

                $db = ConnectionManager::getDataSource('default');

                $originIDs = explode(',', $this->request->data['id-row-duplicate']);

                foreach ($originIDs as $sourceID) {
                    $db->query('INSERT INTO ' . $this->request->data['duplicate-destination'] . ' (' . implode(',', $fields) . ') SELECT ' . implode(',', $fields) . ' FROM ' . $module . ' WHERE ' . $module . '.id = ' . $sourceID);
                }

                CakeSession::write('moduleEdit.' . $this->moduleKey, $this->moduleKey);
                $this->Session->setFlash('Contenido duplicado con éxito.', 'flash_custom');

                $this->redirect('/' . $module);
            }
        }

        if (isset($this->request->data['delete-action'])) {

            if ($this->request->data['id-row-delete']) {

                $db = ConnectionManager::getDataSource('default');

                $originIDs = explode(',', $this->request->data['id-row-delete']);

                foreach ($originIDs as $sourceID) {
                    $db->query('DELETE FROM ' . $module . ' WHERE ' . $module . '.id = ' . $sourceID);
                }

                CakeSession::write('moduleEdit.' . $this->moduleKey, $this->moduleKey);
                $this->Session->setFlash("Contenido eliminado con éxito. ", 'flash_custom');

                //$this->redirect('/'.$module);
                if (isset($this->request->data['s']) && !empty($this->request->data['s'])) {
                    $this->redirect('/' . $this->moduleKey . '/?s=' . $this->request->data['s']);
                } else {
                    $this->redirect('/' . $this->moduleKey);
                }
            }
        }

        if (isset($this->params->query['s']) && !empty($this->params->query['s'])) {
            $isSearch = true;
        }

        if ($this->moduleData['gcdb']['job_module']) {

            // Produccion
            $server_db = 'localhost';
            $user_db = LOCALUSERDB;
            $password_db = LOCALPASSDB;
            $db_db = LOCALDB;

            $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
            if (!$obj_conexion) {
                echo "<h3>No se ha podido conectar a la base de datos.</h3><hr><br>";
                die();
            }

            $select_qry = 'SELECT CronJob_Modulos.id, CronJob_Modulos.Modulo, CronJob_Modulos.Origen, CronJob_Carpetas.Servicio AS OrigenServicio, CronJob_Modulos.Destino, CronJob_Carpetas.LogoSodexo, CronJobFtp.Ip, CronJobFtp.`Port`, CronJobFtp.Usuario, CronJobFtp.Clave FROM CronJob_Carpetas INNER JOIN CronJob_Modulos ON CronJob_Carpetas.Id_CronJob_Modulos = CronJob_Modulos.id INNER JOIN CronJobFtp ON CronJob_Modulos.id_CronJobFtp = CronJobFtp.id WHERE CronJob_Modulos.Activado = 1';
            if (!$resultado = $obj_conexion->query($select_qry)) {
                echo "Lo sentimos, este sitio web está experimentando problemas II.";
                die();
            }
            $jobs_modulos = array();
            while ($registro = $resultado->fetch_assoc()) {
                $jobs_modulos[$registro['id']] = $registro['Modulo'] . ';' . $registro['Destino'];
                //echo $registro['id'] . ';' . $registro['Modulo'] . ';' . $registro['Destino'] . ';' . $registro['Activado'] . '<br>';
            }
            $job_module = true;
            $this->set('job_module', $job_module);
            $this->set('job_modules', $jobs_modulos);
            $this->set('job_module_result', $resultado);
        }

        $fieldsData     = $this->moduleData['gcdb']['fields']['field'];
        $orderBy        = isset($this->request->query['orderBy']) ? $this->request->query['orderBy'] : '';
        $orderDirection = isset($this->request->query['order']) ? $this->request->query['order'] : 'ASC';

        if (empty($orderBy)) {
            $orderParams = false;
        } else {
            $orderParams = $orderBy . ' ' . $orderDirection;
        }

        if ($isSearch) {

            $searchTerm  = trim($this->params->query['s']);

            $contentList = $this->Content->findAllRows($searchTerm, $this->moduleKey, $orderParams, $fieldsData, $page, ROWS_PER_PAGE, $pages);
        } else {

            $contentList = $this->Content->getAllRows($this->moduleKey, $orderParams, $fieldsData, $page, ROWS_PER_PAGE, $pages);
        }

        $availableDuplicateDestinations = array();

        foreach ($this->modules as $module_key => $module) {

            if ($this->Content->compareFields($this->moduleData['gcdb']['fields']['field'], $module['gcdb']['fields']['field'])) {
                $availableDuplicateDestinations[$module_key] = $module['gcdb']['content_name'];
            }
        }

        if ($isSearch || !empty($orderBy)) {

            $queryString = $isSearch ? '?s=' . $searchTerm : '';

            if ($queryString == '') {
                $queryString = '?';
            } else {
                $queryString = $queryString . '&';
            }

            $queryString = !empty($orderBy) ? $queryString . 'orderBy=' . $orderBy . '&order=' . $orderDirection : $queryString;
        } else {
            $queryString = '';
        }


        if ($this->moduleData['gcdb']['reports_app']) {
            $report = true;
        } else {
            $report = false;
        }

        // if($clientFolder == 'demo'){
        $this->set('sessionModuleEdit', CakeSession::read('moduleEdit'));
        $this->set('module', $this->moduleKey);
        // }

        $this->set('report', $report);
        $this->set('clientFolder', $clientFolder);
        $this->set('orderBy', $orderBy);
        $this->set('orderDirection', $orderDirection);
        $this->set('availableDuplicateDestinations', $availableDuplicateDestinations);
        $this->set('clientFolder', $clientFolder);
        $this->set('searchTerm', $searchTerm);
        $this->set('queryString', $queryString);
        $this->set('isSearch', $isSearch);
        $this->set('pages', $pages);
        $this->set('page', $page);
        $this->set('customFields', $fieldsData);
        $this->set('contentList', $contentList);
    }

    function downloadCSV()
    {
        //$this->layout = 'ajax';
        if ($this->moduleData['gcdb']['custom_csv'] == NULL) {
            $this->autoRender = false;
            $importMap = $this->moduleData['gcdb']['import_map']['map'];
            $namefileOri = $this->moduleData['gcdb']['content_name'];
            $nameFile = preg_replace('/\s+/', '_', $namefileOri);
            $nameFile2 = str_replace(':', '', $nameFile);
            $actual = '';
            $max = sizeof($importMap);
            $csvArr = $this->moduleData['gcdb']['data_csv']['csv'];
            $servicios = $this->moduleData['gcdb']['fields']['field'];
            $position = 0;
            $service = false;


            foreach ($importMap as $index => $data) {

                if (strtolower($data['@column']) == 'tipo') {
                    $position = $index;
                }
                if (strtolower($data['@column']) == 'servicio') {

                    $service = true;
                }

                if ($max == ($index + 1)) {
                    $actual .= $data['@column'] . PHP_EOL;
                } else {
                    $actual .= $data['@column'] . ';';
                }
            }
            if ($service) {

                foreach ($servicios as $key => $value) {

                    if ($value['@key'] == 'servicio') {
                        foreach ($value['option'] as $option) {

                            $auxArray = [];

                            for ($x = 0; $x < $max; $x++) {
                                if ($x == $max - 1) {
                                    $auxArray[$x] = '' . PHP_EOL;
                                } else {
                                    $auxArray[$x] = ';';
                                }
                            }

                            foreach ($csvArr as $csv) {
                                for ($i = 1; $i <= $csv['@cant']; $i++) {
                                    $auxArray[$position - 1] = $option['@value'] . ';';
                                    $auxArray[$position] = $csv['@name'] . ';';
                                    $actual .= implode("", $auxArray);
                                }
                            }
                        }
                    }
                }
            } else {

                $auxArray = [];

                for ($x = 0; $x < $max; $x++) {
                    if ($x == $max - 1) {
                        $auxArray[$x] = '' . PHP_EOL;
                    } else {
                        $auxArray[$x] = ';';
                    }
                }

                foreach ($csvArr as $csv) {
                    for ($i = 1; $i <= $csv['@cant']; $i++) {
                        $auxArray[$position] = $csv['@name'] . ';';
                        $actual .= implode("", $auxArray);
                    }
                }
            }

            ob_start();
            //header('Content-Type: text/html; charset=UTF-8');
            header('Content-type: application/vnd-ms-excel; charset=utf-8');
            header('Content-Encoding: UTF-8');
            header('Content-disposition: attachment; filename=' . $nameFile2 . '.csv');
            header('Expires: 0');
            header('Cache-Control: no-cache');
            // echo utf8_decode($actual);
            echo utf8_decode(trim($actual));
            header('Content-Length: ' . ob_get_length());

            ob_flush();
        } else {
            $file_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
            $file_link = $file_link . '/' . $this->moduleData['gcdb']['custom_csv'];
            $file_name = basename($file_link);
            $file_link = str_replace('/./', '/', $file_link);
            $fn         =   file_put_contents($file_name, file_get_contents($file_link));
            header("Expires: 0");
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header("Content-type: application/file");
            header('Content-length: ' . filesize($file_name));
            header('Content-disposition: attachment; filename="' . basename($file_name) . '"');
            readfile($file_name);
        }
        die();
    }

    public function edit()
    {

        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {

                $status = array('success' => false, 'message' => 'No se pudo resolver el cliente');
            }
        } else {
            $clientFolder = '';
        }

        $id          = isset($this->request->params['id']) ? $this->request->params['id'] : false;
        $fieldsData  = $this->moduleData['gcdb']['fields']['field'];

        if (isset($this->request->data['edit-content-id'])) {

            $saveData = array();

            //var_dump($this->request->data);

            // modificacion para sistema de monitoreo
            $date_value = '';

            foreach ($fieldsData as $field) {

                if ($field['@type'] == 'image') {
                    $value = empty($this->request->data[$field['@key']]) ? 0 : $this->request->data[$field['@key']];
                } elseif ($field['@type'] == 'date') {

                    if (empty($this->request->data[$field['@key']])) {
                        $value = '0000-00-00';
                    } else {

                        $dateParts = explode('/', $this->request->data[$field['@key']]);

                        if (isset($field['@no-transform-to-mysql']) && $field['no-transform-to-mysql'] = true) {
                            $value =  count($dateParts) == 3 ? $dateParts[0] . '-' . $dateParts[1] . '-' . $dateParts[2] : '0000-00-00';
                        } else {
                            $value = count($dateParts) == 3 ? $dateParts[2] . '-' . $dateParts[1] . '-' . $dateParts[0] : '0000-00-00';
                        }
                        // modificacion para sistema de monitoreo
                        if (count($dateParts) == 3)
                            $date_value = $dateParts[2] . '-' . $dateParts[1] . '-' . $dateParts[0];
                    }
                } elseif ($field['@type'] == 'select') {

                    if (isset($field['@multiple']) && $field['@multiple'] === 'true') {

                        if (isset($field['@rendermodules']) && $field['@rendermodules'] === 'true') {
                            $value = implode('|#|', $this->request->data['publish']);
                        } else {
                            $value = implode('|#|', $this->request->data[$field['@key']]);
                        }

                        //$value = implode('|#|', $this->request->data[$field['@key']]);
                    } else {
                        $value = $this->request->data[$field['@key']];
                    }
                } else {
                    $value = $this->request->data[$field['@key']];
                }

                $saveData[$field['@key']] = $value;
            }

            if ($this->request->data['edit-content-id'] == 0) {
                $this->Content->customCreate($this->moduleKey);
            } else {
                $saveData['id'] = $this->request->data['edit-content-id'];
            }

            $this->Content->customSave($saveData, $this->moduleKey);

            $this->Session->setFlash('Guardado con Exíto!', 'flash_custom');

            // if($clientFolder == 'demo'){
            // modificacion para sistema de monitoreo
            //file_get_contents("http://api.digitalboard.cl/sodexo/modules/{$this->moduleKey}/upload/{$date_value}");
            //CakeSession::write('moduleEdit.' . $this->moduleKey, $this->moduleKey);
            //  }

            //$response_updated = file_get_contents("http://api.digitalboard.cl/sodexo/modules/{$this->moduleKey}/updated");
            ////$response_updated = file_get_contents("http://api.digitalboard.cl/sodexo/modules/{$this->moduleKey}/upload");

            if (isset($this->request->data['s']) && !empty($this->request->data['s'])) {
                $this->redirect('/' . $this->moduleKey . '/?s=' . $this->request->data['s']);
            } else {
                $this->redirect('/' . $this->moduleKey);
            }
        }

        if ($id == 'new') {

            $id = 0;

            foreach ($fieldsData as $field) {
                $content['Content'][$field['@key']] = '';
            }
            /////////////////////////////
            // if($clientFolder == 'demo'){
            //     CakeSession::write('moduleEdit.'.$this->moduleKey, $this->moduleKey );
            // }

            if ($this->moduleKey == 'cafeteria_unidad1_menu_diario') {
                //echo "cafeteria unidad 1";

                //$url = 'https://postman-echo.com/post';
                //$data_string = '{"clave": 2}';
                //$headers =  array(
                //    'Content-Type: application/json',
                //    'Content-Length: ' . strlen($data_string)
                //);
                //$response = ContentController::curl_to_host('POST', $url, $headers, $data_string, $resp_headers);
                //var_dump($response);
                ////$response = json_decode($response);
                //die();
            }

            //$response_updated = file_get_contents("http://api.digitalboard.cl/sodexo/modules/{$this->moduleKey}/load");

        } else {
            $content = $this->Content->getById($this->moduleKey, $id);
        }



        $this->set('customFields', $fieldsData);
        $this->set('content', $content);
        $this->set('contentID', $id);
    }


    public function delete()
    {
        if (isset($this->params['url']['id'])) {
            $id = $this->params['url']['id'];
        } else {
            return false;
        }

        $this->layout = 'ajax';
        $this->autoRender = false;
        $this->Content->customDeleteAll(array('id_row' => $id), $this->moduleKey);

        return true;
    }

    private function makePreviewImageMode()
    {
        $userLogged = parent::getLoggedUser();
        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {

                $status = array('success' => false, 'message' => 'No se pudo resolver el cliente');
            }
        } else {
            $clientFolder = '';
        }


        define('AMBIENTE', 'test');
        include(WWW_ROOT . 'files' . DS . $clientFolder . DS . 'templates_gd' . DS . $this->moduleKey . '.php');

        //echo $this->moduleKey.'<br>';
        //echo $clientFolder.'<br>';
    }

    public function makePreview($urlIndex = false)
    {

        $this->layout = 'ajax';

        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {
                $status = array('success' => false, 'message' => 'No se pudo resolver el cliente');
            }
        } else {
            $clientFolder = '';
        }

        if (!isset($status)) {

            $status = array('success' => true);

            if (isset($this->moduleData['gcdb']['publish_url']['@related-module'])) {

                $this->Content->Preview($this->moduleKey, $clientFolder, $this->moduleData['gcdb']);

                $relatedModules = explode('|', $this->moduleData['gcdb']['publish_url']['@related-module']);

                foreach ($relatedModules as $relatedModule) {

                    $this->Content->setSource($relatedModule);
                    $this->Content->generatePreview($relatedModule, $clientFolder, $this->modules[$relatedModule]['gcdb']);
                }
            } else {
                $this->Content->generatePreview($this->moduleKey, $clientFolder, $this->moduleData['gcdb']);
            }
        }

        $urlIndex = isset($_GET['urlIndex']) ? $_GET['urlIndex'] : false;

        define('URLINDEX', $urlIndex);

        $this->makePreviewImageMode();


        $this->set('urlIndex', $urlIndex);
        $this->set('status', $status);
        $this->set('clientFolder', $clientFolder);
    }

    public function makePublishImageMode()
    {

        define('URLINDEX', 0);
        define('AMBIENTE', 'prod');
        $userLogged = parent::getLoggedUser();

        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {

                $status = array('success' => false, 'message' => 'No se pudo resolver el cliente');
            }
        } else {
            $clientFolder = '';
        }

        include(WWW_ROOT . 'files' . DS . $clientFolder . DS . 'templates_gd' . DS . $this->moduleKey . '.php');
    }

    public function makeSync()
    {
        $clientFolder = $this->getClientSubdomain();

        switch ($clientFolder) {
            case 'cruzverde':
                $url = "http://190.151.100.10:9500/process";
                $result = file_get_contents($url);

                switch ($result) {
                    case 'initiated':
                        echo '<div class="modal-content">';
                        echo '  <div class="modal-body with-padding">';
                        echo '    <div class="alert alert-block alert-info fade in block-inner">';
                        echo '      <h6><i class="icon-screen"></i> Estado</h6>';
                        echo '      <hr>';
                        echo '      <p>Se inició el proceso de sincronización</p>';
                        echo '      <div class="text-left">';
                        echo '        <a class="btn btn-default" href="#" data-dismiss="modal">Cerrar</a>';
                        echo '      </div>';
                        echo '    </div>';
                        echo '  </div>';
                        echo '</div>';
                        break;
                    case 'busy':
                        echo '<div class="modal-content">';
                        echo '  <div class="modal-body with-padding">';
                        echo '    <div class="alert alert-block alert-info fade in block-inner">';
                        echo '      <h6><i class="icon-screen"></i> Alerta</h6>';
                        echo '      <hr>';
                        echo '      <p>Se está ejecutando un proceso previo de sincronización.</p>';
                        echo '      <div class="text-left">';
                        echo '        <a class="btn btn-default" href="#" data-dismiss="modal">Cerrar</a>';
                        echo '      </div>';
                        echo '    </div>';
                        echo '  </div>';
                        echo '</div>';
                        break;
                    default:
                        echo '<div class="modal-content">';
                        echo '  <div class="modal-body with-padding">';
                        echo '    <div class="alert alert-block alert-info fade in block-inner">';
                        echo '      <h6><i class="icon-screen"></i> Alerta</h6>';
                        echo '      <hr>';
                        echo '      <p>Hay un error en la sincronización.</p>';
                        echo '      <div class="text-left">';
                        echo '        <a class="btn btn-default" href="#" data-dismiss="modal">Cerrar</a>';
                        echo '      </div>';
                        echo '    </div>';
                        echo '  </div>';
                        echo '</div>';
                        break;
                }


                break;

            default:
                echo '<div class="modal-content">';
                echo '  <div class="modal-body with-padding">';
                echo '    <div class="alert alert-block alert-info fade in block-inner">';
                echo '      <h6><i class="icon-screen"></i> Error</h6>';
                echo '      <hr>';
                echo '      <p>Operación no permitida</p>';
                echo '      <div class="text-left">';
                echo '        <a class="btn btn-default" href="#" data-dismiss="modal">Cerrar</a>';
                echo '      </div>';
                echo '    </div>';
                echo '  </div>';
                echo '</div>';
                break;
        }

        die();
    }

    // Funcion que borra la marca de la ultima publicacion
    function deletePublishFile($directorio)
    {
        $files = glob($directorio . '/*');
        foreach ($files as $file) {
            if (is_file(($file))) {
                if (strpos($file, 'Publish_') !== false) {
                    unlink($file);
                }
                //if (strpos($file, 'Cronjob_') !== false) {
                //    unlink($file);
                //}
            }
        }
    }

    public function makePublish()
    {
        $this->layout = 'ajax';

        if (STANDALONE_MODE === false) {

            $clientFolder = $this->getClientSubdomain();

            if ($clientFolder === false) {

                $status = array('success' => false, 'message' => 'No se pudo resolver el cliente');
            }
        } else {
            $clientFolder = '';
        }

        $userLogged = parent::getLoggedUser();

        $userLogged['User']['id'] = isset($_GET['fromJOB']) ? $_GET['fromJOB'] : $userLogged['User']['id'];

        if (!isset($this->moduleData['gcdb']['publish_url']) && !isset($status)) {
            $status = array('success' => false, 'message' => 'No se definio URL de publicación');
        }

        if (isset($this->moduleData['gcdb']['notification_button'])) {
            $this->set('showSendNotificationsButton', true);
        } else {

            $this->set('showSendNotificationsButton', false);
        }

        if (!isset($status)) {

            date_default_timezone_set('America/Santiago');




            // $Cliente = $this->moduleData['gcdb']['cliente'];
            // $Sucursal =  $this->moduleData['gcdb']['sucursal'];
            // $Tienda =  $this->moduleData['gcdb']['tienda'];

            // if ($Cliente !== NULL && $Sucursal !== NULL){
            //     // Borrar marca vieja de publicacion
            //     $this->deletePublishFile(WWW_ROOT . 'files' . DS . $clientFolder . DS . 'templates_images' . DS . $Tienda);

            //     $marca_hoy = WWW_ROOT . 'files' . DS . $clientFolder . DS . 'templates_images' . DS . $Tienda . DS .  'Publish_' . date("Ymd") . '.txt';
            //     $fileHandler = fopen($marca_hoy, 'w+');
            //     fclose($fileHandler);
            // }

            $this->PublishHistory->create();
            $this->PublishHistory->save(array('user_id' => $userLogged['User']['id'], 'module' => $this->moduleKey, 'send_date' =>  date('Y-m-d H:i:s'), 'status' =>  'PUBLISHED'));

            $publishID = $this->PublishHistory->getLastInsertId();

            if (isset($this->moduleData['gcdb']['publish_url']['@related-module'])) {
                $this->Content->generatePublish($this->moduleKey, $clientFolder, $publishID, $this->moduleData['gcdb']);

                $relatedModules = explode('|', $this->moduleData['gcdb']['publish_url']['@related-module']);



                foreach ($relatedModules as $relatedModule) {

                    //  pr($this->modules[$relatedModule]['gcdb']);



                    $this->Content->setSource($relatedModule);
                    $this->Content->generatePublish($relatedModule, $clientFolder, $publishID,  $this->modules[$relatedModule]['gcdb']);
                }

                //                    $this->Content->setSource($this->moduleData['gcdb']['publish_url']['@related-module']);
                //                    $this->Content->generatePublish($this->moduleData['gcdb']['publish_url']['@related-module'], $clientFolder, $publishID, $this->modules[$this->moduleData['gcdb']['publish_url']['@related-module']]['gcdb']);

            } else {
                $this->Content->generatePublish($this->moduleKey, $clientFolder, $publishID, $this->moduleData['gcdb']);
            }

            $CURL = curl_init();

            curl_setopt_array($CURL, array(
                CURLOPT_TIMEOUT => 2,
                CURLOPT_RETURNTRANSFER => false,
                CURLOPT_URL => $this->moduleData['gcdb']['publish_url'] . '?module=' . $this->moduleKey . '&client=' . $clientFolder . '&publishID=' . $publishID
            ));

            $output = @curl_exec($CURL);

            curl_close($CURL);

            $this->makePublishImageMode();

            $status = array('success' => true);
            ////////
            //  if($clientFolder == 'demo'){
            CakeSession::delete('moduleEdit.' . $this->moduleKey);
        }

        $this->set('status', $status);
        $this->set('clientFolder', $clientFolder);
        $this->set('moduleKey', $this->moduleKey);
    }




    public function sendNotifications()
    {

        // $this->layout = 'ajax';

        // if (STANDALONE_MODE === false) {

        //     $clientFolder = $this->getClientSubdomain();

        //     if ($clientFolder === false) {

        //         $status = array('success' => false, 'message' => 'No se pudo resolver el cliente');
        //     }
        // } else {
        //     $clientFolder = '';
        // }

        // $userLogged = parent::getLoggedUser();


        // $CURL = curl_init();

        // curl_setopt_array($CURL, array(
        //     CURLOPT_TIMEOUT => 2,
        //     CURLOPT_RETURNTRANSFER => false,
        //     CURLOPT_URL => 'http://sodexo.digitalboard.cl/files/sodexo/mobile/send_notifications.php'
        // ));

        // $output = @curl_exec($CURL);

        // curl_close($CURL);


        // $this->set('output', $output);
        // $this->set('clientFolder', $clientFolder);
        // $this->set('moduleKey', $this->moduleKey);
    }

    private function createUpdateFile($serial = false)
    {

        date_default_timezone_set('America/Santiago');

        $this->layout = 'ajax';

        if (STANDALONE_MODE === false) {

            $client_folder = $this->getClientSubdomain();

            if ($client_folder === false) {
                die('No se pudo determinar la carpeta de imagenes del cliente');
            }
        } else {
            $client_folder = '';
        }

        if ($serial) {

            @mkdir(WWW_ROOT . 'files' . DS . $client_folder . DS . 'templates' . DS . 'updates' . DS . $serial);
            $final_path = WWW_ROOT . 'files' . DS . $client_folder . DS . 'templates' . DS . 'updates' . DS . $serial . DS . 'update.csv';
        } else {
            $final_path = WWW_ROOT . 'files' . DS . $client_folder . DS . 'templates' . DS . 'updates' . DS . 'update.csv';
        }

        $fileData = fopen($final_path, 'w+');

        $txt = date('d-m-Y') . ';' . date("H:i:s");
        fwrite($fileData, $txt);
    }

    function getLiveRelatedContent()
    {

        $this->layout = 'ajax';
        $this->autoRender = false;

        $displayFields =  $this->request->params['displayFields'];

        $displayFieldsParts = explode(':', $displayFields);

        $term = $_GET['term'];

        $conditions[] = $displayFieldsParts[0] . ' LIKE "%' . $term . '%" OR ' . $displayFieldsParts[1] . ' LIKE "%' . $term . '%"';

        return json_encode($this->Content->getLiveRelatedContent($this->moduleKey, $conditions, $displayFieldsParts[0], $displayFieldsParts[1]));
    }

    function validateUnique()
    {

        $this->layout = 'ajax';
        $this->autoRender = false;

        $returnStatus = $this->Content->validateUnique($this->moduleKey, $_GET['id'], $_GET['field'], $_GET[$_GET['field']]);


        $returnMessage = (!empty($returnStatus)) ? 'Este identificador debe ser unico' : 'true';

        return json_encode($returnMessage);
    }

    // curl en php
    static public function curl_to_host($method, $url, $headers, $data, &$resp_headers)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_HEADER, 1);

        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        $rtn = curl_exec($ch);
        $error_msg = curl_error($ch);
        curl_close($ch);

        //var_dump($error_msg);

        $rtn = explode("\r\n\r\nHTTP/", $rtn, 2);
        $rtn = (count($rtn) > 1 ? 'HTTP/' : '') . array_pop($rtn);

        //die();
        list($str_resp_headers, $rtn) = explode("\r\n\r\n", $rtn, 2);

        $str_resp_headers = explode("\r\n", $str_resp_headers);
        array_shift($str_resp_headers);
        $resp_headers = array();
        foreach ($str_resp_headers as $k => $v) {
            $v = explode(': ', $v, 2);
            $resp_headers[$v[0]] = $v[1];
        }

        return $rtn;
    }
}
