<?php
 
App::uses('AppController', 'Controller');

class ReportsAppController extends AppController {

    public  $uses       = array('Content', 'Image', 'ImageCrop', 'PublishHistory', 'PublishPlayerStatus','PublishAlerts', 'PublishAlertsLogs', 'GCDS', 'ReportsApp');
    private $modules    = false;
    private $moduleKey  = false;
    private $moduleData = false;        
    
    public function beforeFilter() {        
        
        parent::beforeFilter();  
        
        $userLogged = parent::getLoggedUser();	
      
        if(isset($_GET['fromJOB']) || is_array($userLogged)) {
		  
            $this->modules = parent::initModules();                               
		   
            if(!$this->modules) {
                die('Error al cargar el XML de configuracion de modulos');
            }
            
            $client = $this->GCDS->getClientSubdomain();      
            $modulesMenu = parent::getModulesMenu($this->modules);           					
            $this->moduleKey = isset($this->request->params['pass'][0]) ? $this->request->params['pass'][0] : false;                       
            $this->moduleKey = (!$this->moduleKey && isset($this->request->params['module'])) ? $this->request->params['module'] : $this->moduleKey;
            
            if(!isset($this->modules[$this->moduleKey])) {
                die('Modulo no configurado2');
            } else {
                $this->moduleData = $this->modules[$this->moduleKey];
            }

            if(!isset($_GET['fromJOB']) && ($userLogged['User']['rol'] != 'PLATFORM_ADMIN' && !in_array($this->moduleKey, $userLogged['User']['modules']['view']))) {            
                return $this->redirect('/users/login');
            } else {
                
                $this->set('userLogged', $userLogged); 
                $this->set('client', $client);
                $this->set('modulesMenu', $modulesMenu);        
                $this->set('activeMenu', $this->moduleKey);           
                $this->set('moduleKey', $this->moduleKey);  
                $this->set('viewTitle', $this->moduleData['gcdb']['content_name']);  
            }  
            
        } else {
            
            if($this->request->is('ajax')) {
                die('NO_LOGGED');
            } else {
                return $this->redirect('/users/login');
            }

        }
        
    }      

 
    public function index($module, $page = 1) {

        //CONESTO APUNTAS A LA TABLA DE LOS REPORTES
        //$this->moduleData['gcdb']['reports_app']
         $isSearch     = false;
        $searchTerm   = '';
        $orderByHeader = '';
       
        if(STANDALONE_MODE === false) {
            
            $clientFolder = $this->getClientSubdomain();
            
            if($clientFolder === false) {
                die('No se pudo determinar el subdominio del cliente');
            }
            
        } else {
            $clientFolder = '';
        }

        if(isset($this->params->query['s']) && !empty($this->params->query['s'])) {         
            $isSearch = true;       
        } 

        $fieldsData     = ['rut', 'plato', 'servicio', 'tipo', 'fecha', 'fechaPedido']; 
        $orderBy        = isset($this->request->query['orderBy']) ? $this->request->query['orderBy'] : '';
        $orderDirection = isset($this->request->query['order']) ? $this->request->query['order'] : 'ASC';
        $orderParams = empty($orderBy)? $orderParams = false :  $orderParams = $orderBy . ' ' . $orderDirection; 
 
        if($isSearch) {    
            
            $searchTerm  = trim($this->params->query['s']);
            $contentList = $this->ReportsApp->findAllRows($searchTerm, $this->moduleData['gcdb']['reports_app'], $orderParams, $fieldsData, $page, ROWS_PER_PAGE, $pages);                              
        //    $contentCountList = $this->ReportsApp->getCountPlatos($searchTerm, $this->moduleData['gcdb']['reports_app'], $fieldsData, $page, ROWS_PER_PAGE, $pages);
            
        } else {   
            
            $contentList = $this->ReportsApp->getAllOrder($this->moduleData['gcdb']['reports_app'], $orderParams, $page, ROWS_PER_PAGE, $pages);                               
          //  $contentCountList = $this->ReportsApp->getCountPlatos($this->moduleData['gcdb']['reports_app'], array(), $page, ROWS_PER_PAGE, $pages);
        }

        foreach ($contentList as $cont){
            
            if(isset($auxPlatosCount[$cont['ReportsApp']['plato']]['count'])){
                $auxPlatosCount[$cont['ReportsApp']['plato']]['count'] = $auxPlatosCount[$cont['ReportsApp']['plato']]['count']++;
              
            }else{
                $auxPlatosCount[$cont['ReportsApp']['plato']]['count'] = 1;
                 $auxPlatosCount[$cont['ReportsApp']['plato']]['plato'] = $cont['ReportsApp']['plato'];
            }
            
           
        }

        foreach ($contentList as &$content){
            
            $content['ReportsApp']['fecha'] = $this->formatDate($content['ReportsApp']['fecha']);
            $content['ReportsApp']['fechaPedido'] = $this->formatDate($content['ReportsApp']['fechaPedido']);
        }

        if($isSearch || !empty($orderBy)) {
                                  
            $queryString = $isSearch ? '?s=' .$searchTerm : '';
            
            if($queryString == '') {
                $queryString = '?';
            } else {
                $queryString = $queryString.'&';
            }
            
            $queryString = !empty($orderBy) ? $queryString . 'orderBy=' . $orderBy . '&order=' . $orderDirection : $queryString;
            
        } else {
            $queryString = '';
        }
        
        $this->set('pages', $pages);
        $this->set('page', $page);  
        $this->set('contentList', $contentList);
        $this->set('orderDirection', $orderDirection);
        $this->set('orderBy', $orderBy);
        $this->set('searchTerm', $searchTerm);
        $this->set('queryString', $queryString);
        $this->set('isSearch', $isSearch);

    }
    
    
    public function reportsAppCount($module, $page = 1){
        
         $isSearch     = false;
        $searchTerm   = '';
        $orderByHeader = '';
       
        if(STANDALONE_MODE === false) {
            
            $clientFolder = $this->getClientSubdomain();
            
            if($clientFolder === false) {
                die('No se pudo determinar el subdominio del cliente');
            }
            
        } else {
            $clientFolder = '';
        }

        if(isset($this->params->query['s']) && !empty($this->params->query['s'])) {         
            $isSearch = true;       
        } 

        $fieldsData     = ['plato', 'servicio', 'tipo', 'fecha', 'fechaPedido']; 
 
        if($isSearch) {
            $searchTerm  = trim($this->params->query['s']);
            $contentList = $this->ReportsApp->findAllRowsCount($searchTerm, $this->moduleData['gcdb']['reports_app'], false, $fieldsData, $page, ROWS_PER_PAGE, $pages);                              
        } else {   
            $contentList = $this->ReportsApp->findAllRowsCount(false, $this->moduleData['gcdb']['reports_app'], false, false, $page, ROWS_PER_PAGE, $pages);                               
        }

        if($isSearch) {
                                  
            $queryString = $isSearch ? '?s=' .$searchTerm : '';
            
            if($queryString == '') {
                $queryString = '?';
            } else {
                $queryString = $queryString.'&';
            }
            
            
        } else {
            $queryString = '';
        }
        
        $this->set('pages', $pages);
        $this->set('page', $page);  
        $this->set('contentList', $contentList);
        $this->set('searchTerm', $searchTerm);
        $this->set('queryString', $queryString);
        $this->set('isSearch', $isSearch);
    }
    
    
    /*
    function indexAjax(){
         $this->layout = 'ajax';
         
       // $oTsByTypes                 = $this->Dashboard->getOtsByTypes($year);
        $this->set('oTsByTypes', 'asds');
    }*/
    
    
    function downloadReportsApp(){
        $this->layout = 'ajax';
        $type = $this->request->params['type'];
        
        $this->set('type',$type);
        
    }

    function downloadReports(){
        //$this->layout = 'ajax';
        $this->autoRender = false;
        $date   = isset($this->request->params['date']) ? $this->request->params['date'] : '01-01-2019'; 
         $dateToday = date("d-m-Y");
         
        if($date){
            $dateSplit = explode("-", $date);
            $dateFormat = $dateSplit[2] . '-' . $dateSplit[1] . '-' . $dateSplit[0];

        }else{
            $status['succes'] = false;
            $this->set('status', $status);
        }

        $contentList = $this->ReportsApp->getAll($this->moduleData['gcdb']['reports_app'], $dateFormat, '');           

        $actual = 'Rut Cliente; Plato; Servicio; Fecha; Fecha Pedido'.PHP_EOL;
            
        foreach ($contentList as $content){
             $actual .= $content['ReportsApp']['rut'].'; ' .$content['ReportsApp']['plato']. '; '. $content['ReportsApp']['servicio'].'; '. $this->formatDate($content['ReportsApp']['fecha']) . '; ' . $this->formatDate($content['ReportsApp']['fechaPedido']) . PHP_EOL;
            
        }
        
        ob_start();
        header('Content-disposition: attachment; filename='. $this->moduleData['gcdb']['reports_app'] . '_' . $dateFormat . '_' . $dateToday . '.csv');
        header('Content-Type: text/csv');
        header('Expires: 0');  
        header('Cache-Control: no-cache');             
        echo $actual;
        header('Content-Length: '. ob_get_length());   
        ob_flush();
        die();
 
    }
    
    function downloadReportsCount(){
       
        $this->autoRender = false;
        $date   = isset($this->request->params['date']) ? $this->request->params['date'] : '01-01-2019'; 
        $dateToday = date("d-m-Y");
        
        
        if($date){
            $dateSplit = explode("-", $date);
            $dateFormat = $dateSplit[2] . '-' . $dateSplit[1] . '-' . $dateSplit[0];

        }else{
            $status['succes'] = false;
            $this->set('status', $status);
        }
        
        $contentList = $this->ReportsApp->getAll($this->moduleData['gcdb']['reports_app'], $dateFormat, 'count');
       
        
        $actual = 'Tipo de Plato; Plato; Cantidad;'.PHP_EOL;
        
        foreach ($contentList as $content){
 
             $actual .= $content['ReportsApp']['tipo'].'; ' .$content['ReportsApp']['plato']. '; '. $content[0]['cantidad'] . PHP_EOL;
        }

        ob_start();
        header('Content-disposition: attachment; filename='. $this->moduleData['gcdb']['reports_app'] . '_' . $dateFormat .'_'.$dateToday.'.csv');
        header('Content-Type: text/csv');
        header('Expires: 0');  
        header('Cache-Control: no-cache');             
        echo $actual;
        header('Content-Length: '. ob_get_length());   
        ob_flush();
        die();
        
        
        
    }
    

    function formatDate($date){
        
        $dateFullSplit = explode(" ", $date);
        $dateExplode = explode("-", $dateFullSplit[0]);
        
        return $dateExplode[2] . '-' . $dateExplode[1] . '-' . $dateExplode[0];

    }
    
}