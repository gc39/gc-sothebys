<?php
 
App::uses('AppController', 'Controller');

class GalleryController extends AppController {

    public  $uses    = array('GCDS', 'Content', 'Image');
    private $Modules = false;
    
    public function beforeFilter() {                      
        
        parent::beforeFilter();      
        
        $userLogged = parent::checkLogged();
        
        if(isset($userLogged)){
			
			$this->Modules = parent::initModules(); 
            $client = $this->GCDS->getClientSubdomain();
		   
			if(!$this->Modules) {
				die('Error al cargar el XML de configuracion de modulos');
			}
			
			$modulesMenu = parent::getModulesMenu($this->Modules);
			
			$this->set('modulesMenu', $modulesMenu);
            $this->set('client', $client);
			$this->set('userLogged', $userLogged);                        
			$this->set('activeMenu', 'gallery');           
			$this->set('viewTitle', 'Galeria');  
		}
        
    }      

    
    
    public function isLogged() {
        
        $this->layout     = 'ajax';
        $this->autoRender = false;
        
        if(!CakeSession::read('LoggedUserNew')) { 			
            return false;			
        } else {            
            return true;
        }
    }
    
    public function all($page = 1) {
	
		if(isset($this->request->query['search']) && !empty($this->request->query['search']) ){
		
			$imagesList = $this->Image->find('all', array('conditions'=>array('description LIKE "%'.$this->request->query['search'].'%"'), 'order' => 'id DESC'));
			
		}else{
		
			$imagesList = $this->Image->find('all', array('order' => 'id DESC'));
		
		}
		
        $fromUpload = (isset($this->request->query['upload']))? true : false; 
        
        $pages = 1;
        
        if(STANDALONE_MODE === false) {
            
            $clientFolder = $this->getClientSubdomain();
            
            if($clientFolder === false) {
                die('No se pudo determinar la carpeta de imagenes del cliente');
            }
            
        } else {
            $clientFolder = '';
        }
        
        if($imagesList) {                      
            $pages      = ceil(count($imagesList) / ROWS_PER_PAGE_IN_MEDIA);  
			if(isset($this->request->query['search']) && !empty($this->request->query['search'])){
				$imagesList = $this->Image->find('all', array('conditions'=>array('description LIKE "%'.$this->request->query['search'].'%"'), 'order' => 'id DESC', 'limit'=> ROWS_PER_PAGE_IN_MEDIA, 'page'=> $page));
			}else{
				$imagesList = $this->Image->find('all', array('order' => 'id DESC', 'limit'=> ROWS_PER_PAGE_IN_MEDIA, 'page'=> $page));
			}
        }                
		
        $this->set('fromUpload', $fromUpload);
        $this->set('page', $page);
        $this->set('pages', $pages);
        $this->set('clientFolder', $clientFolder);
        $this->set('imagesList', $imagesList);
        $this->set('selectedImageID', false);
        $this->set('fromMediaModal', false); 
        $this->set('search', isset($this->request->query['search'])? $this->request->query['search'] : '' );
        
    }
  
    public function mediaModal($selectedImageID = false, $inputField = false, $moduleKey = false, $page = 1) {

        $this->layout = 'ajax';
        
        if(STANDALONE_MODE === false) {
            
            $clientFolder = $this->getClientSubdomain();
            
            if($clientFolder === false) {
                die('No se pudo determinar la carpeta de imagenes del cliente');
            }
            
        } else {
            $clientFolder = '';
        }
        
        if(isset($this->Modules[$moduleKey]['gcdb']['gallery_tag'])) {
            $galleryTag = $this->Modules[$moduleKey]['gcdb']['gallery_tag'];
        } else {
            $galleryTag = 'GLOBAL';
        }
        
       // echo $galleryTag;
        
        if(isset($this->request->query['search']) && !empty($this->request->query['search']) ){
		
			$imagesList = $this->Image->find('all', array('conditions'=>array('description LIKE "%'.$this->request->query['search'].'%"'), 'order' => 'id DESC'));
			
		} else {		
          
            
            //if($clientFolder == 'demo') {
                $imagesList = $this->Image->find('all', array('order' => 'id DESC', 'conditions' => array('branch' => $galleryTag)));		
//            } else {
//                $imagesList = $this->Image->find('all', array('order' => 'id DESC'));	
//            }
		}
		
		$fromUpload = (isset($this->request->query['upload']))? true : false; 
        $pages = 1;
        
        if($imagesList) {                      
            $pages = ceil(count($imagesList) / ROWS_PER_PAGE_IN_MEDIA_POPUP);  
			if(isset($this->request->query['search']) && !empty($this->request->query['search'])){
				$imagesList = $this->Image->find('all', array('conditions'=>array('description LIKE "%'.$this->request->query['search'].'%"'), 'order' => 'id DESC', 'limit'=> ROWS_PER_PAGE_IN_MEDIA_POPUP, 'page'=> $page));
			}else{
                
               // if($clientFolder == 'demo') {
                    $imagesList = $this->Image->find('all', array('order' => 'id DESC', 'conditions' => array('branch' => $galleryTag), 'limit'=> ROWS_PER_PAGE_IN_MEDIA_POPUP, 'page'=> $page));		
//                } else {
//                    $imagesList = $this->Image->find('all', array('order' => 'id DESC', 'limit'=> ROWS_PER_PAGE_IN_MEDIA_POPUP, 'page' => $page));	
//                }				
			}
        }                

        $this->set('fromUpload', $fromUpload);
        $this->set('page', $page);
        $this->set('pages', $pages);         
        $this->set('imagesList', $imagesList);       
        $this->set('clientFolder', $clientFolder);
        $this->set('moduleKey', $moduleKey);
        $this->set('selectedImageID', $selectedImageID);
        $this->set('inputField', $inputField);
        $this->set('fromMediaModal', true);
		$this->set('search', isset($this->request->query['search'])? $this->request->query['search'] : '' );
        
    }
    
    public function viewVideo() {
        
        $this->layout = 'ajax';    
        $this->autoRender = false;

        $clientFolder = $this->getClientSubdomain();

        $uri = $_GET['video'];
        $uri_fields = explode('/', $uri);
        if ($uri_fields[1] == 'home'){
            $uri = $_SERVER['REQUEST_SCHEME'] . '://'.$clientFolder.'.digitalboard.app/files/'.$clientFolder.'/templates_images_test/medioscompartidos/' . $uri_fields[count($uri_fields) - 1];
        }
        
        echo '<video src="'.$uri.'" controls width="500"></video>'; 
        
    }
    
    public function cropModal() {
        
        $this->layout = 'ajax';              
        
        if(STANDALONE_MODE === false) {
            
            $clientFolder = $this->getClientSubdomain();
            
            if($clientFolder === false) {
                die('No se pudo determinar el subdominio del cliente');
            }
            
        } else {
            $clientFolder = '';
        }
        
        $expectedWidth  = explode(':', $this->request->params['expectedSize']);
        $expectedHeight = $expectedWidth[1];
        $expectedWidth  = $expectedWidth[0];        
        $moduleKey      = isset($this->request->params['moduleKey']) ? $this->request->params['moduleKey'] : '';
        
        $imageURL = '/files/'. $clientFolder . $this->Image->getImageFilename($this->request->params['id']);
        
        $this->set('id', $this->request->params['id']);
        
        $this->set('moduleKey', $moduleKey);
        $this->set('expectedWidth', $expectedWidth);
        $this->set('expectedHeight', $expectedHeight);
        
        $this->set('inputField', $this->request->params['fieldKey']);
        $this->set('imageURL', $imageURL);
        $this->set('clientFolder', $clientFolder);
               
        $this->render('crop_modal_new');
       
    }
    
    public function upload($moduleKey) {
       
        
        $this->layout = 'ajax';
        $this->autoRender = false;                                     
        
        
        if(isset($this->Modules[$moduleKey]['gcdb']['gallery_tag'])) {
            $galleryTag = $this->Modules[$moduleKey]['gcdb']['gallery_tag'];
        } else {
            $galleryTag = 'GLOBAL';
        }
        
        echo $galleryTag;
        
        if(STANDALONE_MODE === false) {
            
            $client_folder = $this->getClientSubdomain();
            
            if($client_folder === false) {
                die('No se pudo determinar la carpeta de imagenes del cliente');
            }
            
        } else {
            $client_folder = '';
        }

        if(isset($_FILES['file']['tmp_name'])) {  

            $save_image     = false;                             
            $final_path     = WWW_ROOT .'files' . DS . $client_folder . DS . strtolower($this->request->data['name']);
           
            move_uploaded_file($_FILES['file']['tmp_name'], $final_path);
            
            $image_data     = getimagesize($final_path);                          
            
            switch ($image_data[2]) {
                case IMAGETYPE_JPEG:
                    $image_original = imagecreatefromjpeg($final_path);
                    $save_image     = imagejpeg($image_original,  $final_path, 90);
                    break;              
                case IMAGETYPE_PNG:
                    
                    $image_original = imagecreatefrompng($final_path);
                    
                    if($this->is_alpha_png($final_path) && ($client_folder == 'demo' || $client_folder == 'carlsonwagonlit' || $client_folder == 'espaciourbano')) {
                        imagealphablending($image_original, true); // setting alpha blending on
                        imagesavealpha($image_original, true); 
                        $save_image     = imagepng($image_original,  $final_path, 4);    
                    } else {                    
                        $save_image     = imagejpeg($image_original,  $final_path, 90);
                    }
                    
                    break;
                case IMAGETYPE_GIF:
                    $image_original = imagecreatefromgif($final_path);
                    $save_image     = imagegif($image_original,  $final_path);
                    break;
                default:
                    $image_original = imagecreatefromjpeg($final_path);
                    $save_image     = imagejpeg($image_original,  $final_path, 90);
            }               
            
            if($save_image) {   
				
				if($client_folder == 'demo'){
					$this->generateThumb($final_path);
				}
                $this->Image->create();         
                
                
                //if($client_folder == 'demo') {
                    $this->Image->save(array('filename' => strtolower($this->request->data['name']), 'description' => $_FILES['file']['name'], 'branch' => $galleryTag));                
//                } else {
//                    $this->Image->save(array('filename' => strtolower($this->request->data['name']), 'description' => $_FILES['file']['name']));                
//                }
                
                //echo 'OK';
                
            }                                            
        } 

    }
    
    private function is_alpha_png($fn){
        return (ord(@file_get_contents($fn, NULL, NULL, 25, 1)) == 6);
    }




	private function generateThumb($final_path){

		$toWidth = 200;
		$toHeight = 300; 
		list($width, $height) = getimagesize($final_path);
  
		$xscale=$width/$toWidth;
		$yscale=$height/$toHeight;
		
		if ($yscale>$xscale){
			$new_width = round($width * (1/$yscale));
			$new_height = round($height * (1/$yscale));

		} else {
			$new_width = round($width * (1/$xscale));
			$new_height = round($height * (1/$xscale));
		}

		$imageResized = imagecreatetruecolor($new_width, $new_height);
		
		$imageTmp     = imagecreatefromjpeg ($final_path);
		imagecopyresampled($imageResized, $imageTmp,
		0, 0, 0, 0, $new_width, $new_height, $width, $height);
		
		$path = explode( DS , $final_path );
		$index = count( $path ) -1;
		$path[$index] = 'thumb_'.$path[$index];
		$final_path = implode( DS, $path );
		
		imagejpeg($imageResized,  $final_path, 90);
	}
	
    public function save() {
        
        $this->layout = 'ajax';
        $this->autoRender = false;
        
        foreach ($this->params['url'] as $key => $value) {
            
            $id = strstr($key, 'image_id_');                       
            
            if($id) { 
                
                $id     = str_replace('image_id_', '', $id);
                $return = array('id' => $id, 'value' => $value);
                
                $this->Image->save(array('id' => $id, 'description' => $value));
                
            }
        }
        
        return isset($return) ? json_encode($return) : json_encode(array('empty'));
        
    }
    
    public function delete($id) {
        
        $this->layout = 'ajax';
        $this->autoRender = false;        
  
        $imageFields = $this->GCDS->getAllMoldulesImagesFields($this->Modules);        
        
        //pr($imageFields);
        
        if($imageFields) {
                        
            foreach($imageFields as $table => $fields) {
                
                foreach($fields as $field) {
                    $this->Content->useTable = $table;
                    $this->Content->query('UPDATE '.$table.' SET '.$field.' = "" WHERE '.$field.' = '.$id);
                }                
                
            }
        }
        
        $this->Image->delete($id);        
        
        return true;
        
    }     
    
    public function cropImage() {
        
        $this->layout = 'ajax';
        $this->autoRender = false;        

        if(STANDALONE_MODE === false) {
            
            $clientFolder = $this->getClientSubdomain();
            
            if($clientFolder === false) {
                die('No se pudo determinar el subdominio del cliente');
            } else {
                $clientFolder = $clientFolder;
            }
            
        } else {
            $clientFolder = '';
        }
        
        if($clientFolder == 'demo') {
            
            $info = pathinfo($_GET['filename']);

            $image_size = getimagesize(WWW_ROOT . DS . $_GET['filename']);

            $original_width  = $image_size[0];
            $original_height = $image_size[1];                
            $original_type   = $image_size[2];                

            $target_width  = $_GET['image_width']; 
            $target_height = $_GET['image_height'];                
            
            switch ($original_type) {
                case IMAGETYPE_JPEG:
                    $image_original = imagecreatefromjpeg(WWW_ROOT . DS . $_GET['filename']);
                    break;              
                case IMAGETYPE_PNG:
                    
                    $image_original = imagecreatefrompng(WWW_ROOT . DS . $_GET['filename']);                    
                    break;
                
                case IMAGETYPE_GIF:
                    $image_original = imagecreatefromgif(WWW_ROOT . DS . $_GET['filename']);
                    break;
                default:
                    $image_original = imagecreatefromjpeg(WWW_ROOT . DS . $_GET['filename']);
            }   
            

            $image_target       = imagecreatetruecolor($target_width, $target_height);
            $image_target_final = imagecreatetruecolor($_GET['w'], $_GET['h']);

            if($original_type == IMAGETYPE_PNG && $this->is_alpha_png(WWW_ROOT . DS . $_GET['filename'])) {
                
                imagealphablending($image_original, true); 
                imagesavealpha($image_original, true);    
                imagealphablending($image_target, true); 
                imagesavealpha($image_target, true);     
                imagealphablending($image_target_final, true); 
                imagesavealpha($image_target_final, true); 
                
                imagecopyresampled($image_target, $image_original, 0, 0, 0, 0, $target_width, $target_height, $original_width, $original_height);        
                imagecopyresampled($image_target_final, $image_target, 0, 0, $_GET['x'], $_GET['y'],  $_GET['w'], $_GET['h'], $_GET['w'], $_GET['h']);
                
                
            } else {
                    
                imagecopyresampled($image_target, $image_original, 0, 0, 0, 0, $target_width, $target_height, $original_width, $original_height);        
                imagecopyresampled($image_target_final, $image_target, 0, 0, $_GET['x'], $_GET['y'],  $_GET['w'], $_GET['h'], $_GET['w'], $_GET['h']);
                
            }
            
            if($original_type == IMAGETYPE_GIF) {
                $filename = 'crop_'.rand(1,999).time().'.gif';
                imagegif($image_target_final,  WWW_ROOT . DS .'files' . DS . $clientFolder . DS . $filename);            
            } elseif($original_type == IMAGETYPE_PNG) {

                if($this->is_alpha_png(WWW_ROOT . DS . $_GET['filename'])) {
                    
                    $filename = 'crop_alpha_'.rand(1,999).time().'.png';
                                        
                    imagepng($image_target_final,   WWW_ROOT . DS .'files' . DS . $clientFolder . DS . $filename, 4);    
                    
                 } else {    
                     
                    $filename = 'crop_'.rand(1,999).time().'.jpg';
                    imagejpeg($image_target_final,   WWW_ROOT . DS .'files' . DS . $clientFolder. DS  . $filename, 90);
                 }


            } else {
                $filename = 'crop_'.rand(1,999).time().'.jpg';
                imagejpeg($image_target_final,  WWW_ROOT . DS .'files' . DS . $clientFolder . DS . $filename, 90);
            }

            $this->Image->create();                
            $this->Image->save(array('filename' => $filename, 'description' => $filename));                

            $id = $this->Image->getLastInsertId();

            return json_encode(array('filename' => $clientFolder .  DS . $filename, 'id' => $id));
            
            
        } else {
            
            //die('b');
            $info = pathinfo($_GET['filename']);

            $extension = strtolower($info['extension']);

            $image_size = getimagesize(WWW_ROOT . DS . $_GET['filename']);

            $original_width  = $image_size[0];
            $original_height = $image_size[1];                
            $original_type   = $image_size[2];

            $target_width  = $_GET['image_width']; 
            $target_height = $_GET['image_height'];                

//            switch ($extension) {
//                case 'jpg':
//                    $image_original = imagecreatefromjpeg(WWW_ROOT . DS . $_GET['filename']);
//                    break;
//                case 'jpeg':
//                    $image_original = imagecreatefromjpeg(WWW_ROOT . DS . $_GET['filename']);
//                    break;
//                case 'png':
//                    $image_original = imagecreatefrompng(WWW_ROOT . DS . $_GET['filename']);
//                    break;
//                case 'gif':
//                    $image_original = imagecreatefromgif(WWW_ROOT . DS . $_GET['filename']);
//                    break;
//                default:
//                    $image_original = imagecreatefromjpeg(WWW_ROOT . DS . $_GET['filename']);
//            }
            
            switch ($original_type) {
                case IMAGETYPE_JPEG:
                    $image_original = imagecreatefromjpeg(WWW_ROOT . DS . $_GET['filename']);
                    break;              
                case IMAGETYPE_PNG:                    
                    $image_original = imagecreatefrompng(WWW_ROOT . DS . $_GET['filename']);                    
                    break;                
                case IMAGETYPE_GIF:
                    $image_original = imagecreatefromgif(WWW_ROOT . DS . $_GET['filename']);
                    break;
                default:
                    $image_original = imagecreatefromjpeg(WWW_ROOT . DS . $_GET['filename']);
            }   


            $image_target   = ImageCreateTrueColor($target_width, $target_height);
            $image_target_final   = ImageCreateTrueColor($_GET['w'], $_GET['h']);

            imagecopyresampled($image_target, $image_original, 0, 0, 0, 0, $target_width, $target_height, $original_width, $original_height);        
            imagecopyresampled($image_target_final, $image_target, 0, 0, $_GET['x'], $_GET['y'],  $_GET['w'], $_GET['h'], $_GET['w'], $_GET['h']);

            if($extension == 'gif') {
                $filename = 'crop_'.rand(1,999).time().'.gif';
                imagegif($image_target_final,  WWW_ROOT . DS .'files' . DS . $clientFolder .  DS . $filename);            
            } else {
                $filename = 'crop_'.rand(1,999).time().'.jpg';
                imagejpeg($image_target_final,  WWW_ROOT . DS .'files' . DS . $clientFolder .  DS . $filename, 90);
            }

            $this->Image->create();                
            $this->Image->save(array('filename' => $filename, 'description' => $filename));                

            $id = $this->Image->getLastInsertId();

            return json_encode(array('filename' => $clientFolder .  DS . $filename, 'id' => $id));
            
            
        }
    }
    
    public function cropImage2() {
        
        $this->layout = 'ajax';
        $this->autoRender = false;        

        if(STANDALONE_MODE === false) {
            
            $clientFolder = $this->getClientSubdomain();
            
            if($clientFolder === false) {
                die('No se pudo determinar el subdominio del cliente');
            } else {
                $clientFolder = $clientFolder;
            }
            
        } else {
            $clientFolder = '';
        }
        
        $image_info    = getimagesize($this->params['form']['croppedImage']['tmp_name']);
        $original_type = $image_info[2];
            
         switch ($original_type) {
            case IMAGETYPE_JPEG:
                $extension = '.jpg';
                break;              
            case IMAGETYPE_PNG:                    
                $extension = '.png';               
                break;                
            case IMAGETYPE_GIF:
                $extension = '.gif';
                break;
            default:
                $extension = '.jpg';
        }  
        
        $filename   = 'c_'.rand(1,999).time().rand(1,999).$extension;
        $final_path = WWW_ROOT .'files' . DS . $clientFolder . DS . $filename;
           
        if(move_uploaded_file($this->params['form']['croppedImage']['tmp_name'], $final_path)) {
            
            $this->Image->create();                
            $this->Image->save(array('filename' => $filename, 'description' => $filename));                

            $id = $this->Image->getLastInsertId();

            return json_encode(array('status' => 'OK', 'filename' => $clientFolder .  DS . $filename, 'id' => $id));
        } else {
            return json_encode(array('status' => 'ERROR'));            
        }
        
    }

    public function cropImage3($moduleKey) {
        
        $this->layout = 'ajax';
        $this->autoRender = false;        

        if(STANDALONE_MODE === false) {
            
            $clientFolder = $this->getClientSubdomain();
            
            if($clientFolder === false) {
                die('No se pudo determinar el subdominio del cliente');
            } else {
                $clientFolder = $clientFolder;
            }
            
        } else {
            $clientFolder = '';
        }
        
        if(isset($this->Modules[$moduleKey]['gcdb']['gallery_tag'])) {
            $galleryTag = $this->Modules[$moduleKey]['gcdb']['gallery_tag'];
        } else {
            $galleryTag = 'GLOBAL';
        }
      
        
        $image_info    = getimagesize($this->params['form']['croppedImage']['tmp_name']);
        $original_type = $image_info[2];
            
         switch ($original_type) {
            case IMAGETYPE_JPEG:
                $extension = '.jpg';
                $image_original = imagecreatefromjpeg($this->params['form']['croppedImage']['tmp_name']);
                break;              
            case IMAGETYPE_PNG:                    
                $extension = '.png';   
                $image_original = imagecreatefrompng($this->params['form']['croppedImage']['tmp_name']);
                break;                
            case IMAGETYPE_GIF:
                $extension = '.gif';
                $image_original = imagecreatefromgif($this->params['form']['croppedImage']['tmp_name']);
                break;
            default:
                $extension = '.jpg';
                $image_original = imagecreatefromjpeg($this->params['form']['croppedImage']['tmp_name']);
        }  
        
        $filename   = 'c_'.rand(1,999).time().rand(1,999).$extension;
        $final_path = WWW_ROOT .'files' . DS . $clientFolder . DS . $filename;
           
        
         
        if(imagejpeg($image_original, $final_path, 80)) {
            
            $this->Image->create();   
            
            //if($clientFolder == 'demo') {
                $this->Image->save(array('filename' => $filename, 'description' => 'NEW_'.$filename, 'branch' => $galleryTag ));                
//            } else {
//                $this->Image->save(array('filename' => $filename, 'description' => 'NEW_'.$filename));                
//            }

            $id = $this->Image->getLastInsertId();

            return json_encode(array('status' => 'OK', 'filename' => $clientFolder .  DS . $filename, 'id' => $id));
        } else {
            return json_encode(array('status' => 'ERROR'));            
        }
        
       
        
    }
}
