<?php
 
App::uses('AppController', 'Controller');

class DashboardController extends AppController {

    public  $uses      = array('GCDS', 'Content', 'Image');
    private $GCDS_Data = false;
    
    public function beforeFilter() {        
        
        if(CakeSession::read('LoggedUser')){
             CakeSession::write('LoggedUserNew', CakeSession::read('LoggedUser'));
        }
        if($_GET['test']){
            
        echo '<pre>';
        print_r(CakeSession::read('LoggedUserNew'));
        echo '</pre>';
        die();
               
        }

        parent::beforeFilter();    
       
        $userLogged = parent::checkLogged();
          
        $this->Modules = parent::initModules();      
        $client = $this->GCDS->getClientSubdomain();
       
        if(!$this->Modules) {
            die('Error al cargar el XML de configuracion de modulos');
        }
           
        $this->set('userLogged', $userLogged);                   
        $this->set('client', $client);
        $this->set('modulesMenu', parent::getModulesMenu($this->Modules));                        
        $this->set('activeMenu', 'dashboard');           
        $this->set('viewTitle', 'Home');      
        
    }      

    public function index() {
       if(STANDALONE_MODE === false) {
            
            $clientFolder = $this->getClientSubdomain();
            
            if($clientFolder === false) {
                die('No se pudo determinar el subdominio del cliente');
            }
            
        } else {
            $clientFolder = '';
        }
       /* 
        if($clientFolder == 'demo'){
            $this->render('index_2');
        }*/
    }
  
}
