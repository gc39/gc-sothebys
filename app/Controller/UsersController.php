<?php
 
App::uses('AppController', 'Controller');

class UsersController extends AppController {

    public  $uses      = array('GCDS', 'User', 'Permission');    
        
    public function beforeFilter() {        
        
        parent::beforeFilter();                        
        
        $this->Modules = parent::initModules(); 
        $client = $this->GCDS->getClientSubdomain();
       
        if(!$this->Modules) {
            die('Error al cargar el XML de configuracion de modulos');
        }
        
        $modulesMenu = parent::getModulesMenu($this->Modules);
  
        $this->set('modulesMenu', $modulesMenu); 
        $this->set('client', $client);
        $this->set('activeMenu', 'users');           
        $this->set('viewTitle', 'Usuarios y Permisos');      
        
    }     

    public function index() {
        
        $userLogged = parent::checkLogged();
        
        parent::checkRol(array('PLATFORM_ADMIN'));
        
        $usersList = $this->User->find('all', array('order' => 'created'));
        
        foreach($usersList as &$user) {            
            if($user['User']['rol'] != 'PLATFORM_ADMIN') {
                $user['User']['modules'] = $this->Permission->find('all', array('conditions' => array('user_id' => $user['User']['id']))) ;                  
            }            
        }
         
        $this->set('usersList', $usersList);           
        $this->set('userLogged', $userLogged);           
        
    }
    
    public function add() {
    
        parent::checkLogged();
        parent::checkRol(array('PLATFORM_ADMIN'));

        if ($this->request->is('post')) {
                        
            if (isset($this->request->data['password'])) {
                $this->request->data['password'] = md5($this->request->data['password']);                    
            }
            
            if ($this->User->save($this->request->data)) {
               
                if($this->request->data['rol'] != 'PLATFORM_ADMIN') {
                    
                    $id_user       = $this->User->getLastInsertId();                
                    
                    $user_modules  = isset($this->request->data['access']) ? $this->request->data['access'] : array();
                    $allow_publish = isset($this->request->data['publish']) ? $this->request->data['publish'] : array();
                    $allow_edit    = isset($this->request->data['edit']) ? $this->request->data['edit'] : array();
                    
                    foreach($user_modules as $user_module) {

                        if(in_array($user_module, $allow_edit)) {						
                            $allow_edit_save = 1;
                        } else {						
                            $allow_edit_save = 0;
                        }

                        if(in_array($user_module, $allow_publish)) {						
                            $allow_publish_save = 1;
                        } else {						
                            $allow_publish_save = 0;
                        }

                        $saveArray[] = array('module' => $user_module, 'user_id' => $id_user, 'allow_view' => 1, 'allow_edit' => $allow_edit_save, 'allow_publish' => $allow_publish_save);

                    }

                    if(isset($saveArray)) {
                        $this->Permission->create();
                        $this->Permission->saveMany($saveArray);
                    }
                    
                }
                
                $this->Session->setFlash('Usuario creado con éxito!', 'flash_custom');

                return $this->redirect('/users');
                
            }                                      
        }
    }

    public function edit($id) {
        
        parent::checkLogged();
        parent::checkRol(array('PLATFORM_ADMIN'));
        
        $user = $this->User->findById($id);
        
        if($user) {
            
            if($user['User']['rol'] != 'PLATFORM_ADMIN') {
              
                $user['User']['modules']['view'] = $this->Permission->find('all', array('conditions' => array('user_id' => $id, 'allow_view' => 1))) ;                  
                $user['User']['modules']['view'] = Set::extract('{n}.Permission.module', $user['User']['modules']['view']);
				
				$user['User']['modules']['publish'] = $this->Permission->find('all', array('conditions' => array('user_id' => $id, 'allow_publish' => 1))) ;                  
                $user['User']['modules']['publish'] = Set::extract('{n}.Permission.module', $user['User']['modules']['publish']);
              
                $user['User']['modules']['edit'] = $this->Permission->find('all', array('conditions' => array('user_id' => $id, 'allow_edit' => 1))) ;                  
                $user['User']['modules']['edit'] = Set::extract('{n}.Permission.module', $user['User']['modules']['edit']);
                
                $user['User']['modules']['edit']    = is_array($user['User']['modules']['edit']) ? $user['User']['modules']['edit'] : array();
                $user['User']['modules']['view']    = is_array($user['User']['modules']['view']) ? $user['User']['modules']['view'] : array();
                $user['User']['modules']['publish'] = is_array($user['User']['modules']['publish']) ? $user['User']['modules']['publish'] : array();
                        
            } else {
                $user['User']['modules']['view']    = array();
                $user['User']['modules']['publish'] = array();
                $user['User']['modules']['edit']    = array();
            }              
            
            if ($this->request->is('post')) {                                
                
               
                if (!empty($this->request->data['password'])) {
                    $this->request->data['password'] = md5($this->request->data['password']);                    
                } else {
                    unset($this->request->data['password']);                    
                }                            
                
                if ($this->User->save($this->request->data)) {
            
                    $this->Permission->deleteAll(array('user_id' => $id));                                                

                    if($this->request->data['rol'] != 'PLATFORM_ADMIN') {
                        
                        $user_modules  = isset($this->request->data['access']) ? $this->request->data['access'] : array();
                        $allow_publish = isset($this->request->data['publish']) ? $this->request->data['publish'] : array();
                        $allow_edit    = isset($this->request->data['edit']) ? $this->request->data['edit'] : array();

                        foreach($user_modules as $user_module) {

                            if(in_array($user_module, $allow_edit)) {						
                                $allow_edit_save = 1;
                            } else {						
                                $allow_edit_save = 0;
                            }

                            if(in_array($user_module, $allow_publish)) {						
                                $allow_publish_save = 1;
                            } else {						
                                $allow_publish_save = 0;
                            }

                            $saveArray[] = array('module' => $user_module, 'user_id' => $id, 'allow_view' => 1, 'allow_edit' => $allow_edit_save, 'allow_publish' => $allow_publish_save);

                        }
                        
                        if(isset($saveArray)) {
                            $this->Permission->create();
                            $this->Permission->saveMany($saveArray);
                        }
                  
                    }
                        
                    $this->Session->setFlash('Usuario actualizado con éxito!', 'flash_custom');                    
                    
                    return $this->redirect('/users');
                }                             
            }  
        }
               
        $this->set('user', $user);           
        
    }
  
    public function delete($id) {
        
        parent::checkLogged();
        parent::checkRol(array('PLATFORM_ADMIN'));
        
        $this->autoRender = false;
        
        $this->User->deleteAll(array('id' => $id));                                                
        $this->Permission->deleteAll(array('user_id' => $id));                                                

        $this->Session->setFlash('Usuario eliminado con éxito!', 'flash_custom');                    

        return $this->redirect('/users');      
        
    }    

    public function forgot() {     
        
        $this->layout = 'login';
		
		if(STANDALONE_MODE === false) {
            
            $clientFolder = $this->getClientSubdomain();
            
            if($clientFolder === false) {
                die('No se pudo determinar el subdominio del cliente');
            }
            
        } else {
            $clientFolder = '';
        }
		
		$message = "Correo incorrecto";
		$messageSuccess = "Se envió la nueva contraseña a su correo";
	
		if ($this->request->is('post')) {     
            
            $user = $this->User->findByEmail($this->request->data['email']);
      
            if($user) {
                
				$letter = ['A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z'];
				$randomLetter1 = $letter[rand(0,24)];
				$randomLetter2 = $letter[rand(0,24)];
				$randomNumber = rand(100000, 1000000);
				$code = $randomLetter1."".$randomLetter2."".$randomNumber;

				$this->Session->setFlash($messageSuccess, 'flash_custom');

				$mail = $user['User']['email'];
				$name = $user['User']['fullname'];
				$titleMail = 'Nueva Contraseña';
				$messageMail = ucwords($name).',<br><br> Su nueva contraseña de DigitalBoard es:<br><br> <b>'.$code.'</b> <br><br> Para ingresar has clik acá: http://'.$clientFolder.'.digitalboard.cl/users/login';
				
				$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
				$cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$cabeceras .= 'To: '.$name.' <'.$mail.'>' . "\r\n";
				$cabeceras .= 'From: DigitalBoard <no-responder@digitalboard.cl>' . "\r\n";
				
				mail($mail,$titleMail,$messageMail,$cabeceras);
				
				$this->User->id = $user['User']['id'];
				if ($this->User->id) {
					$this->User->saveField('password', md5($code));
				}
            } else {
                $this->Session->setFlash($message, 'flash_custom');
            }
        }
    }
 
     
    public function login() {
        
        CakeSession::delete('LoggedUser');
        
        $this->layout = 'login';                
        
        if ($this->request->is('post')) {     
            
            $user = $this->User->findByEmailAndPassword($this->request->data['email'], md5($this->request->data['password']));
            
            if($user) {
            
                $user['User']['modules']['view'] = $this->Permission->find('all', array('conditions' => array('user_id' => $user['User']['id'], 'allow_view' => 1))) ;                  
                $user['User']['modules']['view'] = Set::extract('{n}.Permission.module', $user['User']['modules']['view']);
				
				$user['User']['modules']['publish'] = $this->Permission->find('all', array('conditions' => array('user_id' => $user['User']['id'], 'allow_publish' => 1))) ;                  
                $user['User']['modules']['publish'] = Set::extract('{n}.Permission.module', $user['User']['modules']['publish']);
              
                $user['User']['modules']['edit'] = $this->Permission->find('all', array('conditions' => array('user_id' => $user['User']['id'], 'allow_edit' => 1))) ;                  
                $user['User']['modules']['edit'] = Set::extract('{n}.Permission.module', $user['User']['modules']['edit']);
                              
                CakeSession::write('LoggedUser', $user);

                if(isset($this->request->query['return'])) {
                    return $this->redirect('/'.$this->request->query['return']);                
                } else {
                    return $this->redirect('/');
                }
                
            } else {
                CakeSession::delete('LoggedUser');
            }
        }
    }        
    
    public function updatePassword() {
        
        $this->autoRender = false; 
        $this->layout     = 'ajax';                
        
        if ($this->request->is('post')) {     
                    
            $loggedUser = CakeSession::read('LoggedUser');
            $user = $this->User->findByEmailAndPassword($loggedUser['User']['email'], md5($this->request->data['current-password']));                        
            
            if($user) {
                
                $this->User->id = $user['User']['id'];
                
				if ($this->User->id) {
					$this->User->saveField('password', md5($this->request->data['new-password']));
				}
                
                $response['status']  = 'OK';
                $response['message'] = 'Se modifico la contraseña con éxito.';
                
            } else {                
                $response['status']  = 'ERROR';
                $response['message'] = 'Contraseña actual no valida';                
            }
            
            echo json_encode($response);
            
        }
    }    
}