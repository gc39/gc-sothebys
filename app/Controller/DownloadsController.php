<?php
 
App::uses('AppController', 'Controller');

class DownloadsController extends AppController {
    
    private $Modules = false;
    
    public function beforeFilter() {                      
        
        parent::beforeFilter();      
           
        if(CakeSession::read('LoggedUserNew')) { 			
            	
            $userLogged = parent::checkLogged();
            
			$this->Modules = parent::initModules();                               
		   
			if(!$this->Modules) {
				die('Error al cargar el XML de configuracion de modulos');
			}
			
			$modulesMenu = parent::getModulesMenu($this->Modules);
			
			$this->set('modulesMenu', $modulesMenu);                
			$this->set('userLogged', $userLogged);                        
			$this->set('activeMenu', '');           
			$this->set('viewTitle', 'Descargas');  
            
		} else {
            
          
            return $this->redirect('/users/login?return=downloads');
            die();
            
        }
        
    }      

    

    
    public function index() {
	
		
    }
  
}
