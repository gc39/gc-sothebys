<?php
 
App::uses('AppController', 'Controller');

class MonitorController extends AppController {

    public  $uses    = array('GCDS', 'DSM');
    private $Modules = false;
    
    public function beforeFilter() {        
        
        parent::beforeFilter();        
        
        $userLogged = parent::checkLogged();
        
        if(isset($userLogged)){
			
			$this->Modules = parent::initModules();  
            $client = $this->GCDS->getClientSubdomain();
		   
			if(!$this->Modules) {
				die('Error al cargar el XML de configuracion de modulos');
			}			
            
			$modulesMenu = parent::getModulesMenu($this->Modules);
			
			$this->set('modulesMenu', $modulesMenu);                
			$this->set('userLogged', $userLogged);   
            $this->set('client', $client);
			$this->set('activeMenu', 'monitor');           
			$this->set('viewTitle', 'Monitor');  
		}
        
    }      

    public function index() {

        $availablePlayers = $this->GCDS->getPlayers($this->Modules);

        //pr($availablePlayers);
		
		
//        die();

        $players = false;
        $existPlayer = false;
        if($availablePlayers) {
                        
            foreach($availablePlayers as $playerSerial) {
                
                if(!isset($players[$playerSerial])) {
                    $jsonResponse = $this->DSM->getPlayerStatus($playerSerial);
                
                    $players[$playerSerial] = $jsonResponse;  	
                    $imageData = $this->DSM->generateScreencap($playerSerial);
                    $players[$playerSerial]->imageData = $imageData ? 'data:image/png;base64,'.base64_encode($imageData) : false;
					
                }           
				if( $playerSerial != 0 ){
					$existPlayer = true; 
				}
            }
        }
        
        //pr($players);
        //die();
        $this->set('players', $players);
        $this->set('existPlayer', $existPlayer);
    }
    
    
}