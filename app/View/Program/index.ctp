
<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3><i class="icon-play" style="font-size: 24px;margin-top: -4px;"></i> Programación</h3>
    </div>
</div>
<!-- /page header -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="/">Home</a></li> 
        <li class="active">Programación</li> 
    </ul>

</div>


<ul class="nav nav-tabs">
    <li class="active"><a href="/program"><i class="icon-play"></i> Programacion</a></li>    
    <li><a href="/playlists"><i class="icon-playlist"></i> Playlists</a></li>    
</ul>

<br>


<div id="program" class="block">
    <div id="fullcalendar"></div>
</div> 


<style>
    
    .fc-toolbar h2 {
        text-transform: capitalize;
    }
    
    .fc-toolbar button {
        position: relative;
        padding: 0px 11px;
        height: 32px;
    }
    
    .fc-publishProgram-button {
        background: #546672 !important;
        border: 1px solid #546672 !important;
        color: #FFF !important;
        text-shadow: none !important;
        box-shadow: none !important;
    }
    
    .fc-addPlaylist-button {
        background: #65B688 !important;
        border: 1px solid #65B688 !important;
        color: #FFF !important;
        text-shadow: none !important;
        box-shadow: none !important;
    }
    
</style>

<script>

    $(document).ready(function() {                

        $('#fullcalendar').fullCalendar({
            customButtons: {
                addPlaylist: {
                    text: '+ Añadir Playlist',
                    click: function() {
                        alert('clicked the custom button!');
                    }
                },
                publishProgram: {
                    text: 'Publicar Programación...',
                    click: function() {
                        console.debug($('#fullcalendar').fullCalendar( 'clientEvents' ));
                    }
                }
            },
            header: {
                left: 'title',
                center: '',
                right: 'month, agendaWeek, agendaDay, prev, next, addPlaylist, publishProgram'
            },
            views: {
                agenda: { 
                    titleFormat: 'MMMM D YYYY'                
                }
            },
            slotDuration: '00:30:00',
            defaultView: 'agendaWeek',
            editable: true,
            allDaySlot: false,
            eventOverlap: false,
            events: [{
                        title: 'Playlist 1',
                        start: '2016-07-12T09:00:00',
                        end: '2016-07-12T16:00:00'
                    },
                    {
                        title: 'Playlist 2',
                        start: '2016-07-13T15:00:00',
                        end: '2016-07-13T16:00:00',
                        dow: [ 1, 3,5 ]
                    }
                ]
        })

    })
   

   
</script>