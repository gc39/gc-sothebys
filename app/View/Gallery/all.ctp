
<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3>Galeria</h3>
    </div>
</div>
<!-- /page header -->


<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="/">Home</a></li> 
        <li class='active'>Galeria</li> 
    </ul>

</div>
<!-- /breadcrumbs line -->

<div class="tabbable page-tabs">           
    <?php echo $this->element('media-manager', array('userLogged' => $userLogged)); ?>     
</div>