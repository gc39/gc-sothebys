<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="icon-crop"></i>Cortar</h4>
</div>

<div class="modal-body with-padding with-scroll with-max-height">       
    <img src="" class="image-to-crop" />
</div>

<div class="modal-footer">    
    <div class="pull-left transform-controls">
        <a href="#" id="autosize-control">Auto-ajustar xxImagen</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="#" id="undo-transform-control">Deshacer Auto-ajustar</a>
    </div>
    <button class="btn btn-warning" data-dismiss="modal">Cerrar</button>
    <button id="save-crop" class="btn btn-primary" data-dismiss="modal">Guardar</button>
</div>

<script>
    
    var jcropAPI      = false;
    var imageWidth    = false;
    var imageHeight   = false;            
    var maxHeight     = false;
    var maxWidth      = false;    
    var imageURL      = false;
    

    $(document).ready(function() {
        
        imageURL  = '<?=$imageURL?>';
        maxHeight = <?=$expectedHeight?>;
        maxWidth  = <?=$expectedWidth?>;         
        
        $('.image-to-crop').attr('src', imageURL);
        
        var img = new Image();                
        
        img.onload = function() {
                    
            imageWidth  = this.width;
            imageHeight = this.height;
                    
            createCrop();
            
        }
        
        img.src = imageURL;

        $('#autosize-control').click(function(e) {                                                           
            
            e.preventDefault();
            
            $('#undo-transform-control').show();
            
            jcropAPI.destroy();
   
            if(maxWidth > maxHeight) {             

                ratio = maxWidth / imageWidth;

                $('.image-to-crop').attr('width', maxWidth);
                $('.image-to-crop').attr('height', parseInt(imageHeight * ratio));  
                $('.image-to-crop').removeAttr('style');                    

            } else if(maxWidth < maxHeight) {

                ratio = maxHeight / imageHeight;

                $('.image-to-crop').attr('height', maxHeight);
                $('.image-to-crop').attr('width', parseInt(imageWidth * ratio));  
                $('.image-to-crop').removeAttr('style');

            } else {                    

                if(imageWidth > imageHeight) {

                    ratio = maxHeight / imageHeight;
              
                    $('.image-to-crop').attr('height', maxHeight);
                    $('.image-to-crop').attr('width', parseInt(imageWidth * ratio));  
                    $('.image-to-crop').removeAttr('style');

                }

                if(imageWidth < imageHeight) {

                }

                if(imageWidth == imageHeight) {

                }
            }
 
            createCrop();
            
        });
        
        
        $('#undo-transform-control').click(function() {
        
            jcropAPI.destroy();
            
            $('.image-to-crop').removeAttr('height');
            $('.image-to-crop').removeAttr('width');
            $('.image-to-crop').removeAttr('style');
            
            $('#undo-transform-control').hide();
            
            createCrop();
            
        });
        
        $('#save-crop').click(function() {                                                           
     
            width  = $('.image-to-crop').width();
            height = $('.image-to-crop').height();
            
            var coords = jcropAPI.tellSelect();  
                       
            $.getJSON( "/gallery/cropImage", { filename:  imageURL, x: coords.x, y: coords.y, w: coords.w, h: coords.h, image_width: width, image_height: height }, function( data ) {
                    
                filename = '/files/' + data.filename;
                imageID  = data.id;
                                              
                $('#edit-form #thumb-<?=$inputField?>').attr('src', filename);   
                $('#edit-form #thumb-link-<?=$inputField?>').attr('href', '/gallery/mediaModal/' + imageID + '/<?=$inputField?> + '/' + <?=$moduleKey?>');   
                $('#edit-form #button-link-<?=$inputField?>').attr('href', '/gallery/mediaModal/' + imageID + '/<?=$inputField?> + '/' + <?=$moduleKey?>');                           
                $('#edit-form #crop-link-<?=$inputField?>').attr('href', '/gallery/cropModal/' + imageID + '/<?=$inputField?>/' + $('#edit-form #crop-link-<?=$inputField?>').data('expected-width') + ':'  + $('#edit-form #crop-link-<?=$inputField?>').data('expected-height') + '/' + <?=$moduleKey?>); 
                $('#edit-form input#<?=$inputField?>').val(imageID);

            });
        });        
        
    });      
    
    function createCrop() {                              

        $('.image-to-crop').Jcrop({
            bgOpacity:   0.4,
            keySupport: false,
            minSize: [maxWidth, maxHeight],
            maxSize: [maxWidth, maxHeight],
            setSelect: [0, 0, maxWidth, maxHeight]}, 
            function() {
                jcropAPI = this;
        });      

    }

</script>