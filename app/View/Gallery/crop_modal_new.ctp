<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="icon-crop"></i>Cortar</h4>
</div>

<div class="modal-body with-padding with-scroll with-max-height">  
    
    <div>
        <img src="<?=$imageURL?>" id="image-to-crop" style="max-width: 100%" />
    </div>
</div>

<div class="modal-footer">    
    
    <div class="pull-left text-left transform-controls col-md-8">
        
        
        <button id="zoom-out" type="button" class="btn btn-icon btn-info"><i class="icon-zoom-out2"></i></button>
        <button id="zoom-in"  type="button" class="btn btn-icon btn-info"><i class="icon-zoom-in2"></i></button>
        
        &nbsp;&nbsp;&nbsp;<i class="icon-info"></i> Utilice zoom y mueva la imagen para un corte preciso.
    </div>
    
    <button id="dismiss-crop" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
    <button id="save-crop" class="btn btn-primary">Guardar</button>
    
</div>

<script>
    
    var jcropAPI      = false;
    var imageWidth    = false;
    var imageHeight   = false;            
    var maxHeight     = false;
    var maxWidth      = false;    
    var imageURL      = false;
    

    $(document).ready(function() {
    
        $('#zoom-out').click(function() {  
            $('#image-to-crop').cropper('zoom', -0.1);
        });
        
        $('#zoom-in').click(function() {  
            $('#image-to-crop').cropper('zoom', 0.1);
        });
        
        $('#image-to-crop').cropper({
            built: function() {
                $('#image-to-crop').cropper('setCropBoxData' ,{x:0, y:0, width: <?=$expectedWidth?>, height: <?=$expectedHeight?>})
            },
            minContainerWidth: <?=$expectedWidth  + 150?>,
            minContainerHeight: <?=$expectedHeight + 150?>,    
            cropBoxResizable: false,
            dragMode: 'move',
            responsive: false,
            toggleDragModeOnDblclick: false
        });
         
        $('#save-crop').click(function() {                                                           
          
            
                          
            
            $('#image-to-crop').cropper('getCroppedCanvas', {width: <?=$expectedWidth?>, height: <?=$expectedHeight?>}).toBlob(function (blob) {
                
                var formData = new FormData();

                formData.append('croppedImage', blob);

                $('#save-crop').text('Cortando imagen, por favor espere...');
                $('#dismiss-crop').hide();
                $('#image-to-crop').cropper('disable');
                
                $.ajax("/gallery/cropImage3/<?=$moduleKey?>", {
                    method: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                    
                        data = jQuery.parseJSON(data);
                                              
                        filename = '/files/' + data.filename;
                        imageID  = data.id;
                                         
                        $('#edit-form #thumb-<?=$inputField?>').attr('src', filename);   
                        $('#edit-form #thumb-link-<?=$inputField?>').attr('href', '/gallery/mediaModal/' + imageID + '/<?=$inputField?>/<?=$moduleKey?>');   
                        $('#edit-form #button-link-<?=$inputField?>').attr('href', '/gallery/mediaModal/' + imageID + '/<?=$inputField?>/<?=$moduleKey?>');                           
                        $('#edit-form #crop-link-<?=$inputField?>').attr('href', '/gallery/cropModal/' + imageID + '/<?=$inputField?>/' + $('#edit-form #crop-link-<?=$inputField?>').data('expected-width') + ':'  + $('#edit-form #crop-link-<?=$inputField?>').data('expected-height') + '/<?=$moduleKey?>' ); 
                        $('#edit-form input#<?=$inputField?>').val(imageID);

                        $('#dismiss-crop').trigger('click');
                        
                    },
                    error: function () {
                        
                        $('#image-to-crop').cropper('enable');
                        $('#save-crop').text('Guardar');
                        $('#dismiss-crop').show();

                        alert('Se producio un error al cortar la imagen');
                        
                    }
                });
            });
            
        });        
        
    });            

</script>