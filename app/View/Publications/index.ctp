
<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3>Centro de Publicación <small></small></h3>
    </div>
</div>
<!-- /page header -->


<!-- /breadcrumbs line -->

<ul id="publication-status-filter" class="nav nav-tabs nav-jusdtified">
    <li class="active"><a href="#" data-toggle="tab" data-type="all"><i class="icon-paragraph-justify2"></i> Todas las Publicaciones <span class="label label-default hide">12</span></a></li>    
    <li><a href="#" data-toggle="tab" data-type="published"><i class="icon-checkmark3"></i> Completas <span class="label label-default hide">12</span></a></li>
    <li><a href="#" data-toggle="tab" data-type="pending"><i class="icon-busy"></i> Pendientes  <span class="label label-default hide">7</span></a></li>
    <li class="pull-right"><a href="#" data-toggle="tab" data-type="aborted"><i class="icon-close"></i> Canceladas <span class="label label-default hide">7</span></a></li>    
</ul>

<br>
<?php
//pr($publications);

foreach ($publications as $publication) {
    $status = $publication['PublishHistory']['status'] == 'published' ? 'Publicacion Completa' : 'Algunos Reproductores Pendiente';
    $statusClass = $publication['PublishHistory']['status'] == 'published' ? 'success' : 'danger';
    ?>

    <div class="panel publications panel-default panel-<?= $statusClass ?> status-all status-<?=$publication['PublishHistory']['status']?>" style="margin-bottom: 20px">
        <div class="panel-heading">
            <h5 class="panel-title" style="font-size: 13px;">Publicacion de <?= $publication['PublishHistory']['displayName'] ?> (#<?= $publication['PublishHistory']['id'] ?>) - Enviada el <?= $publication[0]['formatDate'] ?> - Por <?= $publication['User']['fullname']?></h5>
            <span class="pull-right label label-primary"><?= $status ?></span>
        </div>
        
        <?php if(isset($publication['playerStatus']) && !empty($publication['playerStatus'])) {?>
            <div class="panel-body">
        
                <?php
                foreach ($publication['playerStatus'] as $player) {

                $status = $player['PublishPlayerStatus']['status'] == 'completed' ? 'Publicacion Completa' : 'Pendiente';
                $statusClass = $player['PublishPlayerStatus']['status'] == 'completed' ? 'icon-checkmark3' : 'icon-busy';
                ?>
                 
                   <button type="button" class="btn btn-default"><i class="<?=$statusClass?>"></i> Reproductor #<?=$player['PublishPlayerStatus']['serial']?> 
                        
                        <?php if(isset($player['PublishPlayerStatus']['player_name']) && !empty($player['PublishPlayerStatus']['player_name'])) {
                        echo ' - '.$player['PublishPlayerStatus']['player_name'];}
                        ?>
                   
                   </button>

                <?php } ?>
                
            </div>
        <?php }?>
    </div>
    <?php }
?>
<script>
    
    $('#publication-status-filter a').click(function() {
        status = 'status-' + $(this).data('type');

        $('.panel.publications').hide();
        $('.panel.publications.' + status).show();
    })    
</script>