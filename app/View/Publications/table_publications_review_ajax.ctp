<div id="table-content-tab" style="margin: 15px;">
    <div class="table-responsive with-horizontal-scroll">

        
                
        <?php if(!empty($publishList)){ ?>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="150">Fecha - Hora</th>                                        
                        <th width="190">Usuario</th>
                        <th width="140">Modulo</th>  
                        <th width="320">Imagen Digitalboard</th>                                        
                        <th width="320">Imagen FTP</th>
                    </tr>                               
                </thead>
                <tbody>
                
                    <?php foreach ($publishList as $index => $publish){?>
                        <tr>
                            <td><span class="subtitle"><?=$publish['PublishHistory']['send_date']?></span></td>
                            <td><span class="subtitle"><?=$publish['User']['fullname']?></span></td>  
                            <td><span class="subtitle"><?=$publish['PublishHistory']['module_edit']?></span></td>  
                            <?php if($index == 0){?>
                                <td>
                                    <?php foreach ($imagesDigital as $images){?>
                                    <img class="img-media" style="height: 230px !important;" src="<?=$images?>" /> 
                                    <?php } ?>
                                </td>   
                                <td>
                                    <?php foreach ($imagesFTP as $imagesftp){?>
                                        <img class="img-media" style="height: 230px !important;" src="data:image/jpg;base64,<?=  base64_encode($imagesftp)?>" /> 
                                    <?php ob_flush(); 

                                    } ?>
                                </td>
                            <?php } else { ?>
                                <td>--</td>   
                                <td>--</td>
                            <?php } ?>
                        </tr> 
                    <?php } ?>
                
                </tbody>
            </table>
            <?php }else if($moduleFTP != ''){ ?>
                <div class="alert alert-info fade in">No hay publicaciones para el día de hoy</div>
            <?php } ?>
            

    </div>
</div>

<script>
$('#loading-div-icon').addClass('hidden');
</script>

