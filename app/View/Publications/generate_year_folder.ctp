
<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3>Centro de Publicación <small></small></h3>
    </div>
</div>
<!-- /page header -->

<div class="panel panel-default">
       
    <div class="panel-body">
        <div class="bg-success with-padding block-inner success-folder mensaje" style="display:none;">Se crearon las carpetas! (DB, FTP, MI)</div>
        <div class="bg-danger with-padding block-inner danger-folder-ftp mensaje" style="display:none;">No se pudieron crear las carpetas FTP</div>
        <div class="bg-danger with-padding block-inner danger-folder-content mensaje" style="display:none;">El contenido Remoto no se ha podido crear</div>
        <div class="bg-danger with-padding block-inner danger-folder mensaje" style="display:none;">No se crearon las carpetas</div>
        <form action="#" id="generateFolder" name="generateFolder">
        <div class="col-md-12">
            <div class="form-group">
                <label for="client">cliente:</label>
                <input class="form-control" type="text" name="client" id="client" required="">
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="branch">Sucursal:</label>
                <input class="form-control" type="text" name="branch" id="branch" required="">
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="services">Servicio:</label>
                <select name="services[]" id="services" multiple="" style="width:100%;" required="" >
                    <option value="selectBranch">Seleccione Sucursal</option>
                    <option value="Alm">Almuerzo</option>
                    <option value="Cen">Cena</option>
                    <option value="Des">Desasyuno</option>
                    <option value="Onc">Once</option>
                    <option value="Cho">Choca</option>
                    <option value="Cen_Tar">Cena Tarde</option>
                    <option value="Cen_Noc">Cena Noche</option>
                </select>
            </div>
        </div>
            <!--
        <div class="col-md-12">
            <div class="form-group">
                <label for="addService">Agregar Servicio:</label>
                <div class="input-group">
                    <input class="form-control" type="text" name="addService" id="addService">
                    <span class="input-group-btn">
                        <button class="btn btn-default addServiceButton" type="button">Agregar Servicio</button>
                    </span>

                </div>
                <span class="help-block">(Solo llenar este campo si no está el servicio arriba)</span>
            </div>
        </div>
            -->
        <div class="col-md-12">
            <div class="form-group">
                <label for="orientacion">orientación :</label>
                <select name="orientacion[]" id="orientacion"  multiple="" style="width:100%;" required="">
                    <option value="Arr">Arriba</option>
                    <option value="Aba">Abajo</option>
                    <option value="Izq">Izquierda</option>
                    <option value="Der">Derecha</option>
                    <option value="ArrIzq">Arriba Izquierda</option>
                    <option value="ArrDer">Arriba Derecha</option>
                    <option value="IzqAba">Abajo Izquierda</option>
                    <option value="DerAba">Abajo Derecha</option>
                    <option value="Slide">Slide</option>
                </select>
            </div>
        </div>
        <div class="col-md-12">
            
            <div class="form-group">
                <label for="serverMI">Servidor MagicInfo :</label>
                <select name="serverMI" id="serverMI" class="form-control" style="width:100%;" required="">
                    <option value="">Seleccione Servidor</option>
                    <option value="190.151.100.1">SERVIDOR 1 - 10.10.0.246 - 190.151.100.12</option>
                    <option value="190.151.100.10">SERVIDOR 2 - 10.10.0.250 - 190.151.100.10</option>
                    <option value="190.151.100.13">SERVIDOR 3 - 10.10.0.130 - 190.151.100.13</option>
                    <option value="190.151.100.14">SERVIDOR 4 - 10.10.0.6 - 190.151.100.14</option>
                </select>
            </div>
            <div class="form-group">
                <div class="form-group">
                    <label for="portMI">Puerto MI:</label>
                    <input class="form-control" type="text" name="portMI" id="portMI" required="">
                </div>
            </div>
            <div class="form-group">
                <div class="form-group">
                    <label for="ipFtp">IP FTP:</label>
                    <input class="form-control" type="text" name="ipFtp" id="ipFtp" required="">
                </div>
            </div>
            
            <div class="form-group">
                <div class="form-group">
                    <label for="port">Puerto:</label>
                    <input class="form-control" type="text" name="port" id="port" required="">
                </div>
            </div>
            
            <div class="form-group">
                <label for="refresh">Resfresco (Min):</label>
                <select name="refresh" id="refresh" class="form-control" style="width:100%;" required="">
                    <option value="10">10</option>
                    <option value="15">15</option>
                </select>
            </div>
        </div>
            
        <div class="clearfix"></div>
        <div class="col-md-12">
            <div class="form-group">
                <button class="btn btn-primary generateFolder" type="button">Generar Carpetas!</button>
            </div>  
        </div>
            
        </form>
    </div>
        
</div>

 


<script>
    
  $("#services").select2();
  $("#orientacion").select2();
  
  $('.addServiceButton').click(function(){
      if($('#addService').val() != ''){
            service = $('#addService').val();
            
            service = service.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
            
            valService =  service.substr(0, 3);
            
            
            $('#services').append('<option value="' + valService + '" selected="">' + service + '</option>');
            $('#services').trigger('change');
      }
    
  });
  
  $('.generateFolder').click(function(){
        $('.mensaje').fadeOut('slow');
        $('.generateFolder').attr('disabled','disabled');
      
        $.ajax({
            url: '/publications/generateFolder',
            type: 'post',
            data: $('#generateFolder').serialize(),
             success: function(data){
                if(data == 'OK'){
                    $('.generateFolder').removeAttr('disabled');
                    $('.success-folder').fadeIn('slow');
                }else if(data == 'ERROR FTP'){
                    $('.generateFolder').removeAttr('disabled');
                    $('.danger-folder-ftp').fadeIn('slow');
                }else if(data == 'NO_CONTENT'){
                    $('.generateFolder').removeAttr('disabled');
                    $('.danger-folder-content').fadeIn('slow');
                }else{
                     $('.generateFolder').removeAttr('disabled');
                    $('.danger-folder').fadeIn('slow');
                }
           }
        });
  });
  
  
  
</script>