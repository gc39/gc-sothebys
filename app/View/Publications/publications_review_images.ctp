
<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3>Revisión Publicaciones<small></small></h3>
    </div>
</div>
<!-- /page header -->
<div class="panel panel-default">
       
    <div class="panel-body">

        <?php 
       // echo '<pre>';
       // print_r($modulesMenu);
       // echo '</pre>';
        
        ?>
        
        
        <div class="form-group form-group-servicio">
           <label class="col-sm-2 control-label text-right" style="margin-top: 8px; text-align: left;">Selecciona el Modulo</label>
            <div class="">
                <select class="col-sm-12 logical-select-behavior valid" name="modulos" id="modulos">
                    
                    <option value="">Seleccione modulo</option>
                    <?php foreach ($modules as $moduleKey => $module){?>
                        <option value="<?=$moduleKey?>" ><?=$module?></option>
                    <?php } ?>
                </select>
            </div>
        </div>  
        <div class="clearfix"></div>
        <div class="col-md-12 text-center hidden" id="loading-div-icon" style="margin: 15px;">
                <i class="icon-spinner2 spin block-inner" style="font-size: 35px;"></i>
                <div class="col-md-12 text-center">
                    <span >Cargando...</span>
                </div>
                
            </div>
        <div id="table-content">
            
            
        </div>
        
        
    </div>
        
</div>

<script>

    $(document).ready(function() {
        $("#modulos").select2();

        $('#modulos').change(function(){
            
            $('#table-content-tab').remove();
            
            module = $('#modulos').val();
            console.log(module);
            if(module != ''){
                $('#loading-div-icon').removeClass('hidden');
            }else{
                 $('#loading-div-icon').addClass('hidden');
            }
            
            
           $.ajax({
               url: '/publications/publicationsReviewImages/tablePublicationsReviewAjax/' + module, 
                success: function(data){
                    $("#table-content").html(data);
              }
            });
        });
        
        $('#modulos').trigger('change');
        
        
    });
    
    
</script>
<style>

</style>