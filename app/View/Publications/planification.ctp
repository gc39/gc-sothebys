
<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3>Revisión Publicaciones<small></small></h3>
    </div>
</div>

<div class="clearfix"></div>

<br><br>

<ul id="publication-status-filter" class="nav nav-tabs nav-jusdtified">
    <li class=""><a href="/publications/no-publication/<?=$selectedModule?>/<?=  str_replace('/', '-', $dateFrom)?>/<?=  str_replace('/', '-', $dateTo)?>"><i class="icon-paragraph-justify2"></i> Items Cargados Sin Publicar</a></li>    
    <li class="active"><a href="/publications/planification/<?=$selectedModule?>/<?=  str_replace('/', '-', $dateFrom)?>/<?=  str_replace('/', '-', $dateTo)?>"><i class="icon-paragraph-justify2"></i> Planificación</a></li>    
    <li class=""><a href="/publications/changes/<?=$selectedModule?>/<?=  str_replace('/', '-', $dateFrom)?>/<?=  str_replace('/', '-', $dateTo)?>"><i class="icon-paragraph-justify2"></i> Cambios </a></li>       
</ul>

<div class="row">
    <br><br>
    
    <div class="col-md-6">
        <select style="width:100%" name="modulos" id="modulos">                    
            <option value="">Seleccione una Sucursal</option>
            <?php foreach ($modules as $moduleKey => $module){
                
                if(array_key_exists(str_replace('_menu_diario', '' , $moduleKey), $hh)) {
                ?>
                <option value="<?=$moduleKey?>" <?php if($selectedModule == $moduleKey) { ?> selected="selected"<?php }?>><?=$module?></option>
            <?php }            
                } ?>
        </select>
    </div>


     <div class="col-md-2 mt5">
        <input class="form-control" name="date-from" id="date-from" placeholder="Fecha Desde..." value="<?=$dateFrom?>" type="text" />
    </div>

    <div class="col-md-2 mt5">
        <input class="form-control" name="date-to" id="date-to" placeholder="Fecha Hasta..." value="<?=$dateTo?>"  type="text" />
    </div>                 

    <div class="col-md-2 mt5">
        <button class="btn btn-primary" style="width: 100%" id="generate-report">Generar Reporte</button>
    </div>
    
    <div class="col-md-12">        
        <br><br>
        
        <div class="table-responsive">
            <table id="planification" class="table table-success table-bordered table-striped table-striped-col table-condensed">
                
                <thead>
                    <tr>
                        <th>Servicio</th>
                        
                        <?php foreach($planification as $date => $data) { ?>
                        <th class="text-center"><?=$date?></th>
                        <?php } ?>
                    </tr>  
                </thead>

                <tbody>
                    <?php 
                        foreach($services as $service) {
                        ?>
                    <tr>
                        <td><?=$service?></td>
                        <?php 
                        foreach($planification as $date => $data) {
                            if($data[$service]!=false) {
                           
                            ?>
                            <td class="text-center"><?php echo $data[$service][0]['diffHuman']?> <br></td>
                            <?php   }else { ?>
                            <td class="text-center">-</td>
                            <?php } 
                        } ?>
                    </tr>
                    <?php                            
                    } ?>
                </tbody>
            </table>
        </div>
        <br>
        <div class="clearfix"></div>
    </div>
    
    <br>
</div>

<script>

    $(document).ready(function() {
        
        
        $('.sidebar-toggle').trigger('click');
        
        $("#modulos").select2();
        $('#date-from').datepicker();
        $('#date-to').datepicker();

        
        $('#modulos').trigger('change');
        
        $('#generate-report').click(function() {
            
            dateFrom = $('#date-from').val().split('/');
            dateTo   = $('#date-to').val().split('/');
            
            dateFrom = dateFrom[2] + '-' + dateFrom[1] + '-' + dateFrom[0];
            dateTo   = dateTo[2]   + '-' + dateTo[1]   + '-' + dateTo[0];
            
            location.href = '/publications/planification/' + $('#modulos').val().split('|')[0] + '/' + dateFrom + '/' + dateTo;                   
            
        })
    });
    
    
</script>