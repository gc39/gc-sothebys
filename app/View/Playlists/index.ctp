
<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3><i class="icon-playlist" style="font-size: 24px;margin-top: -4px;"></i> Playlists</h3>
    </div>
</div>
<!-- /page header -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="/">Home</a></li> 
        <li class="active">Playlists</li> 
    </ul>

</div>


<ul class="nav nav-tabs">
    <li><a href="/program"><i class="icon-play"></i> Programación</a></li>    
    <li class="active"><a href="/playlists"><i class="icon-playlist"></i> Playlists</a></li>    
</ul>

<br>

<div id="playlists" class="block">

    <div class="action-bar row">
        
        <div class="col-xs-6">
            
            <select id="select-playlist" class="form-control" style="font-weight: bold">
                <option value="">Seleccione un Playlist...</option>
                <?php
                foreach($playlistAvailables as $playlist) {
                    
                    $selectedClass = $currentPlaylist['Playlist']['id'] == $playlist['Playlist']['id'] ? 'selected="selected"' : '';
                    
                    echo '<option value="' . $playlist['Playlist']['id'] . '" '.$selectedClass.'>' . $playlist['Playlist']['name'] . '</option>';
                }
                ?>
            </select>
      
        </div>
        
        <div class="col-xs-6">
            <a type="button" class="btn btn-success pull-right" href="/program/new"><i class="icon-plus"></i>Nueva Playlist</a>             
        </div>
    
    </div>

    
    <div id="main-playlist-content" class="col-xs-12">   
        
<!--        <h4><?=$currentPlaylist['Playlist']['name']?></h4>-->
        <?php 
        if($currentPlaylist === false) {
        ?>
            <p class="text-center"><br /><br />
                <strong>Por favor seleccione una Playlist o Cree una nueva.</strong><br />
            </p>
        <?php 
        } else {     

        ?>
            <ul id="playlistItems">
                <?php                                
                foreach($currentPlaylistItems as $playlistItem) {
                ?>
                <li class="ui-state-default itemx"><i class="icon-menu2"></i> <?=$playlistItem['PlaylistItem']['name']?></li>     
                <?php }?>
            </ul>
               
            <div class="text-center">
            <a type="button" class="btn btn-icon btn-default btn-sm" href="#" style="margin-top: 10px;"><i class="icon-plus"></i></a>  
            </div>
        <?php          
        }
        ?>
            
    </div>   
            
    <?php 
    if($currentPlaylist != false) {
    ?>
    
    <a type="button" class="btn btn-primary" href="#"><i class="icon-save"></i>Guardar Playlist</a> 
    <a type="button" class="btn btn-warning pull-right btn-icon" href="#"><i class="icon-remove"></i></a> 
    <?php } ?> 
    
    <div class="clearfix"></div>
    
</div> 

<style>
    
    
    
    #main-playlist-content {
        margin-top: 9px;
        margin-bottom: 9px;
        border-top: 1px solid #DDD;
        border-bottom: 1px solid #eaeaea;
        padding: 19px 0px 10px 0px;
    }
    
    #playlistItems {
        margin: 0px;
        padding: 0px;
    }
    
    #playlistItems li.itemx {
        list-style: none;
        margin: 0px 0px 1px 0px;
        padding: 8px 5px;
        background: rgba(115, 134, 177, 0.16);
        border: 1px solid #CCC;
    }
    
</style>

<script>

    $(document).ready(function() {

        $('#select-playlist').change(function() {
            if($('#select-playlist').val() != '') {
                location.href = '/playlists/' + $('#select-playlist').val();
            } else {
                location.href = '/playlists';
            }
        })
        
        $( "#playlistItems" ).sortable({ 
            placeholder: "ui-sortable-placeholder" 
        });
    })
     
</script>