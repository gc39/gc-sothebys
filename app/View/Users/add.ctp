
<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3>Usuarios y Permisos <small></small></h3>
    </div>
</div>
<!-- /page header -->


<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="/">Home</a></li> 
        <li><a href="/users">Usuarios y Permisos</a></li> 
        <li class='active'>Nuevo Usuario</li> 
    </ul>

</div>
<!-- /breadcrumbs line -->

<form action="" role="form" method="post">
    <div class="panel panel-default">
        <div class="panel-heading"><h6 class="panel-title"><i class="icon-profile"></i> Nuevo Usuario</h6></div>
        <div class="panel-body">
  
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Nombre Completo:</label>
                        <input name="fullname" minlength="2" maxlength="70" type="text" required class="form-control">
                    </div>

                    <div class="col-md-6">
                        <label>Email:</label>
                        <input name="email" maxlength="70" type="email" required class="form-control">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Contraseña:</label>
                        <input name="password" id="password" type="password" required class="form-control">
                    </div>

                    <div class="col-md-6">
                        <label>Repetir Contraseña:</label>
                        <input name="password_retype" id="password_retype"  type="password" required class="form-control">
                    </div>
                </div>
            </div>                        
            
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        
                        <label>Rol</label>
                        
                        <select required name="rol" id="rol" data-placeholder="Roles.." class="select-full select2-offscreen" tabindex="-1" title="">                             
                            <option value="PLATFORM_ADMIN">PLATFORM_ADMIN</option> 
                            <option value="CLIENT">CLIENT</option>                     
                        </select>
                    </div>                   
                </div>
            </div>           

			<div class="form-group module-group hide">
				<div class="row">
					<div class="col-md-12">
						<label>Modulos</label>
                         <label class="pull-right"><input type="checkbox" id="selectedAll" /> Seleccionar Todo</label>
						<div class="list-group">
						<?php
							foreach($modulesMenu as $module) {
						?>
							<div class="list-group-item">
								
                                <span class=""><?php echo $module['name']; ?></span>
                                
								<label class="pull-right">
									<input type="checkbox" class="publish" name="publish[]" value="<?php echo $module['url']?>" /> Permitir Publicar
								</label>
							
								<label class="pull-right">
									<input type="checkbox" class="edit" name="edit[]" value="<?= $module['url']?>"/> Permitir Editar
									&nbsp;&nbsp;&nbsp;
								</label>
                                
                             	<label class="pull-right">
									<input type="checkbox" class="access" name="access[]" value="<?= $module['url']?>"/> Permitir Ver
									&nbsp;&nbsp;&nbsp;
								</label>
                                
								<div class="clearfix"></div>
							</div>
						<?php
							}
						?>
						</div>
					</div>
				</div>
			</div>

            <div class="form-actions text-right">                
                <input type="submit" value="Guardar" class="btn btn-primary">
            </div>

        </div>
    </div>
</form>
<script>        
    
    $(document).ready(function() {
                        
        $('form').validate({rules: {            
            password_retype: {
              equalTo: "#password"
            }
        }});
         $('#rol').select2();
        $('#rol').change(function() {            
            if($(this).val() == 'PLATFORM_ADMIN') {                
                $('.module-group').addClass('hide');
            } else {
                $('.module-group').removeClass('hide');
            }
        });
        
        $( '#selectedAll' ).change(function(){
            if($("#selectedAll").is(':checked')) {  
                $( '.publish' ).prop( 'checked', true );
                $( '.edit' ).prop( 'checked', true );
                $( '.access' ).prop( 'checked', true );
            }else{
                $( '.publish' ).prop( 'checked', false );
                $( '.edit' ).prop( 'checked', false );
                $( '.access' ).prop( 'checked', false );
            }
        });
    });    
    
</script>