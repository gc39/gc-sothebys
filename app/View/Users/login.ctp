<div class="login-wrapper">
    <form action="" role="form" method="post">
        <div class="popup-header">            
            <span class="text-semibold">Ingresar</span>            
        </div>
        <div class="well">
            
			<div class="form-group has-feedback">
                <label>Usuario</label>
                <input name="email" required type="email" class="form-control" placeholder="Email">
                <i class="icon-mail form-control-feedback"></i>
            </div>

            <div class="form-group has-feedback">
                <label>Contraseña</label>
                <input name="password" required type="password" class="form-control" placeholder="Contraseña">
                <i class="icon-lock form-control-feedback"></i>
            </div>

            <div class="row form-actions">
			
                <div class="col-xs-6">
                    <div class="checkbox checkbox-success">
                        <a href="/users/forgot">¿Olvidó su contraseña?</a>
                    </label>
                    </div>
                </div>

                <div class="col-xs-6">
                    <button type="submit" class="btn btn-warning pull-right"><i class="icon-key"></i> Ingresar</button>
                </div>
            </div>
			
        </div>
    </form>
</div>