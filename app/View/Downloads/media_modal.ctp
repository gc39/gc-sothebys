<?php

?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><i class="icon-images"></i>Galeria</h4>
</div>

<div class="modal-body with-padding">
    <div class="tabbable page-tabs">       
        <?php                 
        
        //pr($imagesList);
        
        echo $this->element('media-manager', array('userLogged' => $userLogged, 'imagesList' => $imagesList, 'clientFolder' => $clientFolder, 'pages' => $pages, 'page' => $page)); 
        ?>
    </div>    
</div>

<div class="modal-footer">
    <button class="btn btn-warning" data-dismiss="modal">Cerrar</button>
    <button id="assign-media" class="btn btn-primary" data-dismiss="modal" onclick="assingMedia()">Guardar</button>
</div>

<script>

    $("#visual-loader").hide();

    function assingMedia() {
        
        imageID  = $('#remote_modal .thumb.selected').data('image-id'); 
        imageURL = $('#remote_modal .thumb.selected').data('src');                    

        $('#edit-form #thumb-<?=$inputField?>').attr('src', imageURL);   
        $('#edit-form #thumb-link-<?=$inputField?>').attr('href', '/gallery/mediaModal/' + imageID + '/<?=$inputField?>/<?=$moduleKey?>');   
        $('#edit-form #button-link-<?=$inputField?>').attr('href', '/gallery/mediaModal/' + imageID + '/<?=$inputField?>/<?=$moduleKey?>');                           
        $('#edit-form #crop-link-<?=$inputField?>').attr('href', '/gallery/cropModal/' + imageID + '/<?=$inputField?>/' + $('#edit-form #crop-link-<?=$inputField?>').data('expected-width') + ':'  + $('#edit-form #crop-link-<?=$inputField?>').data('expected-height') + '/<?=$moduleKey?>'); 
        $('#edit-form input#<?=$inputField?>').val(imageID);
        
        $('#edit-form #crop-link-<?=$inputField?>').show();
        $('#edit-form #clear-button-<?=$inputField?>').show();
        $('#edit-form #crop-link-<?=$inputField?>').removeClass('hide');
        $('#edit-form #clear-button-<?=$inputField?>').removeClass('hide');
           
}
    
</script>