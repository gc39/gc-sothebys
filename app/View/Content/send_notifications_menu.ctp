
<div class="modal-body with-padding"> 
    
   
    
            <div class="alert alert-block alert-info fade in block-inner">            
                <h6><i class="icon-screen"></i> Notificaciones Enviadas</h6>
                <hr>
                <p>Se han enviado las notificaciones a los dispositivos con éxito.</p>
                <div class="text-left">                                
                    
                    <a class="btn btn-default" href="#" data-dismiss="modal">Cerrar</a> 
                </div>
            </div>
    
</div>

<script>
    
    $(document).ready(function() {                        
        $('#visual-loader').hide();        
    });        
    
</script>