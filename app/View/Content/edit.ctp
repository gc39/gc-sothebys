
<div class="page-header">
    <div class="page-title">
        <h3><?=$viewTitle?></h3>
    </div>
</div>
<!-- /page header -->

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="/">Home</a></li> 
        <li class='active'><?=$viewTitle?></li> 
    </ul>
</div>
<!-- /breadcrumbs line -->

<?php
echo $this->Session->flash();
?>

<form id="edit-form" autocomplete="off" class="form-horizontal" role="form" method="POST" onsubmit="beforeSubmit()">
    <div class="panel panel-default">

        <div class="panel-body">
            <input type="hidden" name="s" value="<?=$_GET['s']?>" >
            <?php
            
            $system_fields = array('id', 'created', 'modified');
            $validationsRules['rules'] = array();

            foreach($customFields as $field) {
                
                if(!in_array($field['@key'], $system_fields) ) {  
                    
                    $field['@type'] = $field['@type'] == 'richtext' ? 'richtext_summer' : $field['@type'];
                                        
                    if($field['@type'] == 'text-related') {
                        
                        $relatedParts = explode('.', $field['@related-to-id']);
                        $relatedTable = $relatedParts[0];
                        $relatedID    = $relatedParts[1];
                        
                        $relatedParts = explode('.', $field['@related-to-show']);
                        $relatedShow  = $relatedParts[1];
                        
                    } else {
                        
                        $relatedTable = '';
                        $relatedID    = '';
                        $relatedShow  = '';
                        
                    }
                    
                    if($field['@type'] != 'select'){
                        // Se encarga de colocar el id del cronjob por defecto al hacer un nuevo registro
                        if ($field['@key'] == 'id_cronjob_modulo' && strpos($_SERVER['REQUEST_URI'], '/new') !== false){
                            echo $this->element('custom_types/'.$field['@type'], array('relatedTable' => $relatedTable, 'relatedID' => $relatedID, 'relatedShow' => $relatedShow, 'field' => $field, 'moduleKey' => $moduleKey, 'content' => $_GET['cronjobid']));
                        }
                        else{
                            echo $this->element('custom_types/'.$field['@type'], array('relatedTable' => $relatedTable, 'relatedID' => $relatedID, 'relatedShow' => $relatedShow, 'field' => $field, 'moduleKey' => $moduleKey, 'content' => $content['Content'][$field['@key']])); 
                        }
                    } else {
                        if($field['@rendermodules'] != 'true')
                            echo $this->element('custom_types/'.$field['@type'], array('relatedTable' => $relatedTable, 'relatedID' => $relatedID, 'relatedShow' => $relatedShow, 'field' => $field, 'moduleKey' => $moduleKey, 'content' => $content['Content'][$field['@key']])); 
                        else
                            {
                                $modulemask = $field['@modulemask'];

                                $tienda_names = Array();
                                $tienda_names[] = explode("|#|", $content['Content'][$field['@key']]);
                            ?>

                            
                            <div class="form-group form-group-modules">
                                <label class="col-sm-2 control-label text-right">Casinos</label>
                                <div class="col-sm-10">
                                
                                    <div class="form-group module-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label></label> 
                                                <label class="pull-right"><input type="checkbox" id="selectedAll" /> Seleccionar Todo</label>
                                                <div class="clearfix"></div>
                                                <div class="list-group">
                                                <?php
                                                    $current_url = $_SERVER["REQUEST_URI"];
                                                    $current_url_array = explode('/', $current_url);
                                                    $current_url = $current_url_array[1];


                                                    foreach($modules_publicacionmultiple as $tienda) {
                                                ?>
                                                    <div class="list-group-item" style="padding: 8px 12px">
                                                        <span class=""><?php echo $tienda[0]; ?></span>
                                                        <label class="pull-right">
                                                            <input type="checkbox" class="publish" name="publish[]" value="<?echo $tienda[0];?>" <?php if(in_array($tienda[0], $tienda_names[0])){?> checked="checked" <?php } ?> /> Publicar
                                                        </label>
                                                    </div>
                                                <?php
                                                    }
                                                ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <?php
                        }
                    }
                }
                
                if(isset($field['@validations'])) {
                    
                    $validations = explode('|', $field['@validations']);
                    
                    foreach ($validations as $validation) {                        
                        $validationValue = $validation == 'remote' ? '/' . $moduleKey . '/validateUnique?id='.$contentID.'&field=' . $field['@key'] : true;
                        $validationsRules['rules'][$field['@key']][$validation] = $validationValue;
                    }
                }
            }     

         
            ?>                      

            <div class="form-actions text-right">
                <input type="hidden" value="<?=$contentID?>" name="edit-content-id" id="edit-content-id">
                <?php if($_GET['s']){ ?>
                    <a type="button" href="/<?=$moduleKey?>/?s=<?=$_GET['s']?>" class="btn btn-default">Cancelar</a>
                <?php }else{?>
                    <a type="button" href="/<?=$moduleKey?>" class="btn btn-default">Cancelar</a>
                <?php } ?>
                <input id="submitEditForm" type="submit" value="Guardar" class="btn btn-primary">                
            </div>
            
        </div>
        
    </div>
</form>

<!-- Modal with remote path -->
<div id="remote_modal" class="modal fade" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<!-- Modal with remote path -->
<div id="remote_modal_video" class="modal fade" tabindex="-1" role="dialog" >
    <div class="modal-dialog" style="width: 500px">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {             

        generateDatepickers();
        generateVideoUpload();
        $('#video_temp').val($('#video').val());

        $( '#selectedAll' ).change(function(){
            if($("#selectedAll").is(':checked')) {  
                $( '.publish' ).prop( 'checked', true );
            }else{
                $( '.publish' ).prop( 'checked', false );
            }
        });

        if($('#edit-form .logical-select-behavior').size() > 0) {
            
            $('#edit-form .logical-select-behavior').each(function() {
                
                show_fields = $(this).find('option:selected').data('show-fields');
                hide_fields = $(this).find('option:selected').data('hide-fields');
                show_fields = show_fields.split('|');
                hide_fields = hide_fields.split('|');                                

                for(i in show_fields) {
                    $('.form-group-' + show_fields[i]).show();
                }

                for(i in hide_fields) {
                    $('.form-group-' + hide_fields[i]).hide();
                }
                
            });
        }
        
        if($('.editor').size() > 0) {
            $( '.editor' ).summernote({
                height: 180,
                tabsize: 4, 
                lang: 'es-ES',
                callbacks: {
                    onPaste: function (e) {
                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        document.execCommand('insertText', false, bufferText);
                    }
                },
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'clear']],
                    ['para', ['ul', 'ol','paragraph']]
                ]
            });
        }
        
        $('#edit-form .clear-image').click(function() {
        
            fieldKey = $(this).data('field-key');
            
            $('#edit-form #thumb-' + fieldKey).attr('src', '/files/no_image.gif');   
            $('#edit-form #thumb-link-' + fieldKey).attr('href', '/gallery/mediaModal/0/' + fieldKey + '/<?=$moduleKey?>'); 
            $('#edit-form #button-link-' + fieldKey).attr('href', '/gallery/mediaModal/0/' + fieldKey + '/<?=$moduleKey?>');                           
            $('#edit-form input#' + fieldKey).val('');
            $('#edit-form #crop-link-' + fieldKey).hide();
            $('#edit-form #clear-button-' + fieldKey).hide();
        
        })
        
        
        $('#edit-form .clear-video').click(function() {
        
            fieldKey = $(this).data('field-key');
            
            $('#edit-form input#' + fieldKey).val('');
            $('#edit-form #view-video').hide();
        
        })        
        
        $('#edit-form .linked-behavior').change(function() {
            
            group_main      = $(this).data('linked-to');
            group_visible   = $(this).find('option:selected').data('visible-group');                        
            
            if(typeof group_visible === "undefined") {                
                return false;
            }
            
            expected_width  = $('#'+ group_visible).data('expected-width');
            expected_height = $('#'+ group_visible).data('expected-height');
            expected_aspect = $('#'+ group_visible).data('expected-aspect');

            $('#crop-link-' + group_main).data('expected-width', expected_width);
            $('#crop-link-' + group_main).data('expected-height', expected_height);
            
            crop_link = $('#button-link-' + group_main).attr('href') + '/' + expected_width + ':' + expected_height;
            crop_link = crop_link.replace('mediaModal', 'cropModal');
            
            $('#crop-link-' + group_main).attr('href', crop_link);
            
            $('#edit-form .group-' + group_main).hide();
            $('#edit-form .group-' + group_visible).show();
        
        })
        
        $('#edit-form .logical-select-behavior').change(function() {
            
            show_fields = $(this).find('option:selected').data('show-fields');
            hide_fields = $(this).find('option:selected').data('hide-fields');
            show_fields = show_fields.split('|');
            hide_fields = hide_fields.split('|');                                
            
            for(i in show_fields) {
                $('.form-group-' + show_fields[i]).show();
            }
            
            for(i in hide_fields) {
                $('.form-group-' + hide_fields[i]).hide();
            }

        })


        $('#edit-form .linked-behavior').trigger('change');        
        $("#edit-form").validate(<?=json_encode($validationsRules)?>);

    });
    
    function generateVideoUpload() {
    
        var uploader = new plupload.Uploader({
            runtimes : 'html5',
            browse_button : 'button-browser-video', // you can pass an id...
            container: document.getElementById('video-upload-controls'), // ... or DOM Element itself
            url : '/<?=$moduleKey?>/uploadLocal',

            filters : {
                max_file_size : '150mb',
                mime_types: [
                    {title : "MP4 Video", extensions : "mp4"},
                ]
            },
            init: {
                PostInit: function() {
                    document.getElementById('video-filelist').innerHTML = '';
                    document.getElementById('start-video-upload').onclick = function() {
                        uploader.start();
                        return false;
                    };
                },
                FilesAdded: function(up, files) {
                    
                    $('#start-video-upload').show();
                    
                    plupload.each(files, function(file) {
                        $('#start-video-upload').html('Iniciar carga de ' + file.name + ' (' + plupload.formatSize(file.size) + ')');
                    });
                },
                BeforeUpload: function(up, files) {
                    $('#submitEditForm').attr('disabled', 'disabled');
                    plupload.each(files, function(file) {
                    $('#start-video-upload').html('Subiendo Archivo <b></b>');
                    });
                },
                
                UploadProgress: function(up, file) {                   
                    $('#start-video-upload b').html(file.percent + "%");
                    
                    if(file.percent >= 100) {
                        $('#start-video-upload').html("Sincronizando, por favor espere...");
                    }
                },
                Error: function(up, err) {
                    document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
                },
                FileUploaded: function(uploader,file,result) {
                    
                    $('#submitEditForm').removeAttr('disabled');
                    $('#start-video-upload').hide();
                    
                    $('#view-video').attr('href', '/gallery/viewVideo?video=' + result.response);
                    $('#view-video').show();
                    
                    $('.video-upload-url').val(result.response);
                },
            }
        });
        
        uploader.init();

    }
    function beforeSubmit() {
        
        $('#edit-form .form-group:hidden').remove();       
        
        $('.editor').each(function() {            
            id = $(this).data('id');
            $('#' + id).val(cleanHTML($(this).summernote('code')));           
        })
        
    }
    
    function cleanHTML(txt){
        var sS=/(\r| class=(")?Mso[a-zA-Z]+(")?)/g;
        var out=txt.replace(sS,' ');
        var nL=/(\n)+/g;
        out=out.replace(nL,'<br>');
        var cS=new RegExp('<!--(.*?)-->','gi');
        out=out.replace(cS,'');
        var tS=new RegExp('<(/)*(meta|link|span|\\?xml:|st1:|o:|font)(.*?)>','gi');
        out=out.replace(tS,'');
        var bT=['style','script','applet','embed','noframes','noscript'];
        for(var i=0;i<bT.length;i++){
            tS=new RegExp('<'+bT[i]+'.*?'+bT[i]+'(.*?)>','gi');
            out=out.replace(tS,'');
        }
        var bA=['style','start'];
        for(var ii=0;ii<bA.length;ii++){
            var aS=new RegExp(' '+bA[i]+'="(.*?)"','gi');
            out=out.replace(aS,'');
        }
        return out;
    };
</script>