
<div class="modal-body with-padding">

    <?php
    if($status['success']) { ?>
        <?php

        $caption = 'Publicacion en Proceso';

        if($clientFolder != 'sodexo'){
            $text = 'Su publicación sera procesada en unos minutos, para revisar el estado de su publicación ingrese al <a href="/publications">Centro de Publicacion</a>.';
        } else {
            $text = 'Su publicación será procesada y descargada en pantallas entre 20 minutos y 60 minutos aproximadamente.';

            if($clientFolder == 'sodexo') {
                  $casino_name = split(':',$modulesMenu[$moduleKey]['name'])[0];

                  $prefijo = substr($casino_name, 0, 3); 
                  if($prefijo != 'APP'){
                    // examina los datos de json para insertar en la tabla monitoreo_publicacion
                    //if($moduleKey == 'app_demo_menu_diario'){
                      $uri = "http://api.digitalboard.cl/sodexo/publication/{$moduleKey}/monitoreo";
                      ini_set('default_socket_timeout', 3);
                      file_get_contents($uri);
                    //}

                    //if($moduleKey == 'cafeteria_unidad1_menu_diario'){
                    $uri = "http://api.digitalboard.cl/sodexo/display/{$moduleKey}/macaddress";
                    ini_set('default_socket_timeout', 3);
                    $macaddresses = file_get_contents($uri);
                    $connectivity_ok = 1;
                    $connectivity_fail_array = array();

                    try{
                      $macaddresses = json_decode($macaddresses);

                      if($macaddresses[0]->mac_pantalla != NULL){
                        foreach ($macaddresses as $key => $value) {
                          $macaddress = $value->mac_pantalla;

                          $uri = "http://190.151.100.11:9502/devices/{$macaddress}?now=true";
                          $display_connectivity = file_get_contents($uri);
                          $display_connectivity = json_decode($display_connectivity);

                          $status_conectivity = $display_connectivity->status;

                          switch ($status_conectivity) {
                              case 'powerOn':
                                  break;
                              case 'powerOff':
                                  $connectivity_ok = 0;
                                  $connectivity_fail_array[] = $macaddress;
                                  break;
                              case 'unknown':
                                  $connectivity_ok = 0;
                                  break;
                          }
                        }
                      }
                    }
                    catch(Exception $e){
                      echo ".";
                    }

                    $num_pantallas_sin_conexion = count($connectivity_fail_array);

                    if ($num_pantallas_sin_conexion > 1){
                      switch($connectivity_ok){
                        case 0:
                          $caption = "Las pantallas del casino {$casino_name} se encuentran fuera de línea o apagadas. Por ahora no es posible actualizar su contenido, lo harán una vez recupere la conectividad.";
                          $text = 'Para recuperar la conectividad ejecute el siguiente protocolo:<br><br>Si las pantallas se encuetran apagada, enciéndalas. <br><br>Si las pantallas se encuentran encendida desenchufelas, espere 3 minutos y vuelva a enchufarlas. Estas encenderán automáticamente.<br>Si luego de 25 minutos las pantallas siguen sin actualizar el contenido, escríbanos a soporte@grupoclan.cl para poder asistirlo.';
                          // mandar email
                          break;
                        case 1:
                          $caption = "La pantalla del casino {$casino_name} se encuentra en línea y actualizará el contenido.";
                          $text = 'Si luego de 25 minutos la pantalla sigue sin actualizar el contenido, escríbanos a soporte@grupoclan.cl para poder asistirlo.';
                          break;
                      }
                    } else {
                      switch($connectivity_ok){
                        case 0:
                          if($num_pantallas_sin_conexion != 0) {
                            $caption = "Una de sus pantallas del casino {$casino_name} se encuentra fuera de línea o apagada. Por ahora no es posible actualizar su contenido, lo hará una vez recupere la conectividad.";
                            $text = 'Para recuperar su conectividad ejecute el siguiente protocolo:<br><br>Si las pantallas se encuetran apagada, enciéndalas. <br><br>Si la pantalla se encuentra encendida desenchufela, espere 3 minutos y vuelva a enchufarla. Esta encenderá automáticamente.<br>Si luego de 25 minutos las pantalla siguen sin actualizar el contenido, escríbanos a soporte@grupoclan.cl para poder asistirlo.';
                            // mandar email
                          }else {
                            $caption = "La pantalla del casino {$casino_name} se encuentra fuera de línea o apagada. Por ahora no es posible actualizar su contenido, lo hará una vez recupere la conectividad.";
                            $text = 'Para recuperar su conectividad ejecute el siguiente protocolo:<br><br>Si las pantallas se encuetran apagada, enciéndalas. <br><br>Si la pantalla se encuentra encendida desenchufela, espere 3 minutos y vuelva a enchufarla. Esta encenderá automáticamente.<br>Si luego de 25 minutos las pantalla siguen sin actualizar el contenido, escríbanos a soporte@grupoclan.cl para poder asistirlo.';
                            // mandar email
                          }
                          break;
                        case 1:
                          $caption = "La pantalla del casino {$casino_name} se encuentra en línea y actualizará el contenido.";
                          $text = 'Si luego de 25 minutos la pantalla sigue sin actualizar el contenido, escríbanos a soporte@grupoclan.cl para poder asistirlo.';
                          break;
                      }
                    }
                  }else {
                    $caption = "Publicacion correcta.";
                    $text = '';
                  }
            }

        } ?>
      <?php if($client != 'johnson' && $client != 'paris' && $client != 'cruzverde'){ ?>

            <div class="alert alert-block alert-info fade in block-inner">
                <h6><i class="icon-screen"></i> <?=$caption?></h6>
                <hr>
                <p><?=$text?></p>
                <div class="text-left">
                    <a class="btn btn-info" href="/publications">Ir al Centro de Publicación</a>
                    <?php if($showSendNotificationsButton) {?>

                    <button type="button" data-dismiss="modal" class="btn btn-default" style="margin-right:4px"  data-toggle="modal"  href="/<?=$moduleKey?>/sendNotifications" data-target="#send_notifications_modal" onclick="javascript: $('#visual-loader').show();">Enviar Notificaciones</button>
                    <?php } ?>
                    <a class="btn btn-default  pull-right" href="#" data-dismiss="modal">Cerrar</a>

                </div>
            </div>
     <?php } elseif($client == 'cruzverde') { ?>
            <div class="alert alert-block alert-info fade in block-inner">
                <h6><i class="icon-screen"></i>Campaña publicada</h6>
                <hr>
                <p>La publicación sera verá reflejada en unos minutos, recibirá un mail de confirmación con la campaña publicada correctamente.</p>
                <div class="text-left">

                    <a class="btn btn-default" href="#" data-dismiss="modal">Cerrar</a>

                </div>
            </div>
      <?php } else { ?>
             <div class="alert alert-block alert-info fade in block-inner">
                 <h6><i class="icon-screen"></i> Publicacion en Proceso</h6>
                 <hr>
                 <p>Su publicación sera procesada en unos minutos.</p>
                 <div class="text-left">

                     <a class="btn btn-default" href="#" data-dismiss="modal">Cerrar</a>

                 </div>
             </div>
         <?php } ?>

    <?php } else { ?>
        <div class="alert alert-block alert-danger fade in block-inner">
            <h6><i class="icon-eye7"></i> Fallo al Publicar!</h6>
            <hr>
            <p><?=$status['message']?></p>
            <div class="text-left">
                <a class="btn btn-info" href="#" data-dismiss="modal">Cerrar</a>
            </div>
        </div>
    <?php } ?>

</div>

<script>

    $(document).ready(function() {

        $('#visual-loader').hide();

        $('.alert-<?=$clientFolder.'-'.$moduleKey?>').remove();

    });

</script>
