
<div class="page-header">
    <div class="page-title">
        <h3><?=$viewTitle?> <small></small></h3>
    </div>
</div>
<!-- /page header -->

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="/">Home</a></li> 
        <li class='active'><?=$viewTitle?></li> 
    </ul>
</div>
<!-- /breadcrumbs line -->

<?php
echo $this->Session->flash();
?>

<form id="edit-form" class="form-horizontal" role="form" method="POST" onsubmit="beforeSubmit()">
    <div class="panel panel-default">
       
        <div class="panel-body">
            <input type="hidden" name="s" value="<?=$_GET['s']?>" >
            <?php
            
            
            $system_fields = array('id', 'created', 'modified');
            $validationsRules['rules'] = array();
            
            foreach($customFields as $field) {
                
                if(!in_array($field['@key'], $system_fields) ) {  
                    $field['@type'] = $field['@type'] == 'richtext'? 'richtext_summer':$field['@type'];
                    echo $this->element('custom_types/'.$field['@type'], array('field' => $field, 'content' => $content['Content'][$field['@key']])); 
                }
                
                if(isset($field['@validations'])) {
                    
                    $validations = explode('|', $field['@validations']);
                    
                    foreach ($validations as $validation) {
                        $validationsRules['rules'][$field['@key']][$validation] = true;
                    }
                }
            }          
            ?>                      

            <div class="form-actions text-right">
                <input type="hidden" value="<?=$contentID?>" name="edit-content-id" id="edit-content-id">
                <?php if($_GET['s']){ ?>
                    <a type="button" href="/<?=$moduleKey?>/?s=<?=$_GET['s']?>" class="btn btn-default">Cancelar</a>
                <?php }else{?>
                <a type="button" href="/<?=$moduleKey?>" class="btn btn-default">Cancelar</a>
                <?php } ?>
                <input type="submit" value="Guardar" class="btn btn-primary">                
            </div>
        </div>
        
    </div>
</form>

<!-- Modal with remote path -->
<div id="remote_modal" class="modal fade" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<script>
    
    $(document).ready(function() {
             
        
        generateDatepickers();

        if($('#edit-form .logical-select-behavior').size() > 0) {
            
            $('#edit-form .logical-select-behavior').each(function() {
                
                show_fields = $(this).find('option:selected').data('show-fields');
                hide_fields = $(this).find('option:selected').data('hide-fields');
                show_fields = show_fields.split('|');
                hide_fields = hide_fields.split('|');                                

                for(i in show_fields) {
                    $('.form-group-' + show_fields[i]).show();
                }

                for(i in hide_fields) {
                    $('.form-group-' + hide_fields[i]).hide();
                }
                
            });
        }
        
        if($('.editor').size() > 0) {
            $( '.editor' ).summernote({
                height: 180,
                tabsize: 4, 
                lang: 'es-ES',
                callbacks: {
                    onPaste: function (e) {
                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        document.execCommand('insertText', false, bufferText);
                    }
                },
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'clear']],
                    ['para', ['ul', 'ol','paragraph']]
                ]
        });
        
        
            /*
            new EasyEditor('.editor', {
                buttons: ['bold', 'italic', 'list', 'alignleft', 'aligncenter', 'alignright', 'x'] ,
                buttonsHtml: {
                    'bold': '<i class="icon-bold"></i>',
                    'italic': '<i class="icon-italic"></i>',
                    'link': '<i class="icon-link5"></i>',
                    'list' : '<i class="icon-list2"></i>',
                    'align-left': '<i class="icon-paragraph-left2"></i>',
                    'align-center': '<i class="icon-paragraph-center2"></i>',
                    'align-right': '<i class="icon-paragraph-right2"></i>'
                }
            });*/
        }
             
        $('#edit-form .clear-image').click(function() {
           
            fieldKey = $(this).data('field-key');
            
            $('#edit-form #thumb-' + fieldKey).attr('src', '/files/no_image.gif');   
            $('#edit-form #thumb-link-' + fieldKey).attr('href', '/gallery/mediaModal/0/' + fieldKey); 
            $('#edit-form #button-link-' + fieldKey).attr('href', '/gallery/mediaModal/0/' + fieldKey);                           
            $('#edit-form input#' + fieldKey).val('');
            $('#edit-form #crop-link-' + fieldKey).hide();
            $('#edit-form #clear-button-' + fieldKey).hide();
        
        })
        
        $('#edit-form .linked-behavior').change(function() {
            
            group_main      = $(this).data('linked-to');
            group_visible   = $(this).find('option:selected').data('visible-group');                        
            
            if(typeof group_visible === "undefined") {                
                return false;
            }
                       
            expected_width  = $('#'+ group_visible).data('expected-width');
            expected_height = $('#'+ group_visible).data('expected-height');

            $('#crop-link-' + group_main).data('expected-width', expected_width);
            $('#crop-link-' + group_main).data('expected-height', expected_height);
             
            crop_link = $('#button-link-' + group_main).attr('href') + '/' + expected_width + ':' + expected_height;
            crop_link = crop_link.replace('mediaModal', 'cropModal');
            
            $('#crop-link-' + group_main).attr('href', crop_link);
            
            $('#edit-form .group-' + group_main).hide();
            $('#edit-form .group-' + group_visible).show();
        
        })
        
        $('#edit-form .logical-select-behavior').change(function() {
            
            show_fields = $(this).find('option:selected').data('show-fields');
            hide_fields = $(this).find('option:selected').data('hide-fields');
            show_fields = show_fields.split('|');
            hide_fields = hide_fields.split('|');                                
            
            for(i in show_fields) {
                $('.form-group-' + show_fields[i]).show();
            }
            
            for(i in hide_fields) {
                $('.form-group-' + hide_fields[i]).hide();
            }

        })
        
        $('#edit-form .linked-behavior').trigger('change');
        $("#edit-form").validate(<?=json_encode($validationsRules)?>);

    });
       
    
    function beforeSubmit() {
        
        $('#edit-form .form-group:hidden').remove();       
        
        $('.editor').each(function() {            
            id = $(this).data('id');
            $('#' + id).val(cleanHTML($(this).summernote('code')));
            
        })
               
        
    }
    
    function cleanHTML(txt){
        var sS=/(\r| class=(")?Mso[a-zA-Z]+(")?)/g;
        var out=txt.replace(sS,' ');
        var nL=/(\n)+/g;
        out=out.replace(nL,'<br>');
        var cS=new RegExp('<!--(.*?)-->','gi');
        out=out.replace(cS,'');
        var tS=new RegExp('<(/)*(meta|link|span|\\?xml:|st1:|o:|font)(.*?)>','gi');
        out=out.replace(tS,'');
        var bT=['style','script','applet','embed','noframes','noscript'];
        for(var i=0;i<bT.length;i++){
            tS=new RegExp('<'+bT[i]+'.*?'+bT[i]+'(.*?)>','gi');
            out=out.replace(tS,'');
        }
        var bA=['style','start'];
        for(var ii=0;ii<bA.length;ii++){
            var aS=new RegExp(' '+bA[i]+'="(.*?)"','gi');
            out=out.replace(aS,'');
        }
        return out;
    };
</script>