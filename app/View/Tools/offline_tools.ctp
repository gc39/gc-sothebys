<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3>Offline Tools</h3>
    </div>
</div>
<!-- /page header -->


<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="/">Home</a></li> 
        <li class='active'>Offlines Tools</li> 
    </ul>

</div>
<!-- /breadcrumbs line -->

    <!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <?php 
    
    $first = true;
    
    foreach($players as $player) {?>
        <li role="presentation" class="<?php if($first) {?>active<?php }?>"><a href="#player_<?=$player?>" aria-controls="home" role="tab" data-toggle="tab"><?=$player?></a></li>
    <?php     
        $first = false;
    }?>                    
    <li role="presentation" class="pull-right"><a href="#redirects" aria-controls="home" role="tab" data-toggle="tab">Redirects</a></li>
</ul>
    
<div class="tab-content">
    <br>
    <?php 
    
    $first = true;
    
    foreach($players as $player) {?>
    
        <div role="tabpanel" class="tab-pane  no-border well <?php if($first) {?>active<?php }?>" id="player_<?=$player?>">
            
            <div class="list-group">                           
                <div class="list-group-item">                    
                    <i class="icon-folder2"></i> /data/data/ad_hoc_media/<b>files</b>                
                    <form method="post" action="http://grupoclan:grupoclandsm@190.151.100.12/player/<?=$player?>.remote_2/files?media=ad_hoc_media/files/<?=$clientFolder?>/templates&tab=files" accept-charset="utf-8" target="_blank">
                       <input type="hidden" name="command" value="fm_new_folder">
                       <input type="hidden"class="form-controlx" name="parent_folder" size="50" value="/data/data/ad_hoc_media/">                
                       <input type="hidden" class="form-controlx" name="new_folder" value="files" size="80">
                       <input type="submit" value="+">                
                    </form>
                </div>
                <div class="list-group-item">   
                    <i class="icon-folder2"></i> /data/data/ad_hoc_media/files/<b><?=$clientFolder?></b>
                    <form method="post" action="http://grupoclan:grupoclandsm@190.151.100.12/player/<?=$player?>.remote_2/files?media=ad_hoc_media/files/<?=$clientFolder?>/templates&tab=files" accept-charset="utf-8" target="_blank">
                       <input type="hidden" name="command" value="fm_new_folder">
                       <input type="hidden" class="form-controlx" name="parent_folder" size="50" value="/data/data/ad_hoc_media/files/">                
                       <input type="hidden" class="form-controlx" name="new_folder" value="<?=$clientFolder?>" size="80">
                       <input type="submit" value="+">                
                    </form>
                </div>
                <div class="list-group-item">
                    <i class="icon-folder2"></i> /data/data/ad_hoc_media/files/<?=$clientFolder?>/<b>templates</b>
                    <form method="post" action="http://grupoclan:grupoclandsm@190.151.100.12/player/<?=$player?>.remote_2/files?media=ad_hoc_media/files/<?=$clientFolder?>/templates&tab=files" accept-charset="utf-8" target="_blank">
                       <input type="hidden" name="command" value="fm_new_folder">
                       <input type="hidden" class="form-controlx" name="parent_folder" size="50" value="/data/data/ad_hoc_media/files/<?=$clientFolder?>">                
                       <input type="hidden" class="form-controlx" name="new_folder" value="templates" size="80">
                       <input type="submit" value="+">                
                    </form>
                </div>
            <?php 

            foreach($paths as $path) {

                $path_parts = explode('/', $path);
                $path_parts_size = count($path_parts);

                $parent = $path_parts[$path_parts_size - 2];
                $new = $path_parts[$path_parts_size - 1];

                ?>
            
                <div class="list-group-item">
                    <i class="icon-folder2"></i> /data/data/ad_hoc_media/files/<?=$clientFolder?>/templates/<?=$parent?>/<b><?=$new?></b>
                <form class="" method="post" action="http://grupoclan:grupoclandsm@190.151.100.12/player/<?=$player?>.remote_2/files?media=ad_hoc_media/files/<?=$clientFolder?>/templates&tab=files" accept-charset="utf-8" target="_blank">
                    <input type="hidden" name="command" value="fm_new_folder">
                    <input type="hidden" class="form-controlx" name="parent_folder" size="50" value="/data/data/ad_hoc_media/files/<?=$clientFolder?>/templates/<?=$parent?>">                
                    <input type="hidden" class="form-controlx" name="new_folder" value="<?=$new?>" size="80">
                    <input type="submit" value="+">                
                </form>
                </div>
            <?php } ?>
            </div>    
        </div>    
    <?php         
        $first = false;
    }?>
    
    
    <div role="tabpanel" class="tab-pane well no-border" id="redirects">
        <div class="list-group">            
            <?php foreach($redirects as $k => $v) {?>
            <div class="list-group-item"><i class="icon-download4"></i><a href="/tools/offlineTools/?d=<?=$k?>"><?=$v?></a></div>
            <?php }?>
        </div>
    </div>
</div>
    
    <style>
        input[type="submit"] {margin-top: -19px; float: right;}
    </style>
<br />    