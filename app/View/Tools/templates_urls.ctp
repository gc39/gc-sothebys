<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3>Templates URLs</h3>
    </div>
</div>
<!-- /page header -->


<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="/">Home</a></li> 
        <li class='active'>Templates URLs</li> 
    </ul>

</div>
<!-- /breadcrumbs line -->

<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title">URLs Test</h6></div>
    <div class="panel-body">                
        <table>
            <?php foreach($testURLs as $url) {?>
            <tr><?=$url?></tr>
            <?php }?>
        </table>
    </div>
</div>       

<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title">URLs Producción</h6></div>
    <div class="panel-body">                
        <table>
            <?php foreach($prodURLs as $url) {?>
            <tr><?=$url?></tr>
            <?php }?>
        </table>
    </div>
</div>         