
<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3>Home</h3>
    </div>
</div>
<!-- /page header -->


<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">        
        <li><a href="/">Home</a></li> 
        <li class='active'>Bienvenido</li>         
    </ul>
</div>
<!-- /breadcrumbs line -->



<?php

foreach($modulesMenu as $keyModule => $module) { 
        
    $nameParts = explode(':', $module['name']);

    if(isset($nameParts[0]) && isset($nameParts[1])) {
        if($userLogged['User']['rol'] == 'PLATFORM_ADMIN' || in_array($keyModule, $userLogged['User']['modules']['view'])){
            $groupModulesMenu[$nameParts[0]][$keyModule] = $module;
        }
        
    } else {
        if($userLogged['User']['rol'] == 'PLATFORM_ADMIN' || in_array($keyModule, $userLogged['User']['modules']['view'])){
            $groupModulesMenu['DB_UNGROUP'][$keyModule] = $module;
        }
    }
    
}

?>

<div class="panel panel-default direct-access">
    <div class="panel-heading">
        <h6 class="panel-title"><i class="icon-cube2"></i> Modulos</h6>
    </div>
    
    <div class="panel-body">
   
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <?php 
        
        $firstElement = true;
        $groupIndex   = 0;
        foreach($groupModulesMenu as $keyGroup => $modulesMenu) { 
            $groupIndex++; 
            
           
        ?>
            
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#panel_<?=$groupIndex?>" aria-expanded="true" aria-controls="collapseOne">
                    <?php 
                    if($keyGroup == 'DB_UNGROUP') {
                        echo 'General';                        
                    } else {
                        echo $keyGroup;
                    }
                    ?>
                  </a>
                </h4>
              </div>
              <div id="panel_<?=$groupIndex?>" class="panel-collapse collapse <?php if($firstElement) { echo 'in'; } else { echo 'out'; }?>" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  <?php foreach($modulesMenu as $key => $module) {  ?>
                    
                    
                    <div class="col-lg-2 module-box text-center">

                        <br />
                        <a href="/<?=$module['url']?>"><i class="<?=$module['icon']?>"></i></a>        

                        <div class="top-info">
                            <a href="/<?=$module['url']?>">
                            <?php
                            if($keyGroup == 'DB_UNGROUP') {
                                echo $module['name'];
                            } else {
                                
                                $nameParts  = explode(':', $module['name']);                
                                $moduleName = $nameParts[1];
                                echo $moduleName;
                            }
                            ?>
                            </a>    
                        </div>        
                    </div>
                    
                  <?php }?>
                </div>
              </div>
            </div>
        <?php         
            $firstElement = false;
        } ?>
        </div>

    </div>
    
</div>