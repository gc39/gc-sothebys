
<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3>Home</h3>
    </div>
</div>
<!-- /page header -->


<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">        
        <li><a href="/">Home</a></li> 
        <li class='active'>Bienvenido</li>         
    </ul>
</div>
<!-- /breadcrumbs line -->

 
<div class="panel panel-default direct-access">
    <div class="panel-heading">
        <h6 class="panel-title"><i class="icon-link"></i> Accesos Directos</h6>
    </div>
    <div class="panel-body">

        

        <div class="col-md-3 direct-access-box text-center">

            <br />
            <a href="/gallery"><i class="icon-images"></i></a>        

            <div class="top-info">
                <a href="/gallery">Ver Imagenes</a>
                <small>en la Galeria</small>
            </div>        
        </div>    

        <?php if($userLogged['User']['rol'] != 'BASIC_USER') { ?>    
        <div class="col-md-3 direct-access-box text-center">

            <br />
            <a href="/gallery#file-upload"><i class="icon-upload"></i></a>        

            <div class="top-info">
                <a href="/gallery#file-upload">Subir Imagen</a>
                <small>a la Galeria</small>
            </div>        
        </div>   
        <?php } ?>    
        
      
        <?php if($userLogged['User']['rol'] == 'PLATFORM_ADMIN') { ?>    
        <div class="col-md-3 direct-access-box text-center">

            <br />
            <a href="/users"><i class="icon-users2"></i></a>        

            <div class="top-info">
                <a href="/users">Ver Usuarios</a>
                <small>Usuarios y Permisos</small>
            </div>        
        </div>   
          
        
        <div class="col-md-3 direct-access-box text-center">

            <br />
            <a href="/users/add"><i class="icon-user-plus"></i></a>        

            <div class="top-info">
                <a href="/users/add">Agregar Usuario</a>
                <small>Usuarios y Permisos</small>
            </div>        
        </div>  
        <?php } ?>  
    </div>
</div>

<?php

foreach($modulesMenu as $keyModule => $module) { 
        
    $nameParts = explode(':', $module['name']);

    if(isset($nameParts[0]) && isset($nameParts[1])) {
        if($userLogged['User']['rol'] == 'PLATFORM_ADMIN' || in_array($keyModule, $userLogged['User']['modules']['view'])){
            $groupModulesMenu[$nameParts[0]][$keyModule] = $module;
        }
        
    } else {
        if($userLogged['User']['rol'] == 'PLATFORM_ADMIN' || in_array($keyModule, $userLogged['User']['modules']['view'])){
            $groupModulesMenu['DB_UNGROUP'][$keyModule] = $module;
        }
    }
    
}

?>

<div class="panel panel-default direct-access">
    <div class="panel-heading">
        <h6 class="panel-title"><i class="icon-cube2"></i> Modulos</h6>
    </div>
    
    <div class="panel-body">
   
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <?php 
        
        $firstElement = true;
        $groupIndex   = 0;
        foreach($groupModulesMenu as $keyGroup => $modulesMenu) { 
            $groupIndex++; 
            
           
        ?>
            
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#panel_<?=$groupIndex?>" aria-expanded="true" aria-controls="collapseOne">
                    <?php 
                    if($keyGroup == 'DB_UNGROUP') {
                        echo 'General';                        
                    } else {
                        echo $keyGroup;
                    }
                    ?>
                  </a>
                </h4>
              </div>
              <div id="panel_<?=$groupIndex?>" class="panel-collapse collapse <?php if($firstElement) { echo 'in'; } else { echo 'out'; }?>" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  <?php foreach($modulesMenu as $key => $module) {  ?>
                    
                    
                    <div class="col-lg-2 module-box text-center">

                        <br />
                        <a href="/<?=$module['url']?>"><i class="<?=$module['icon']?>"></i></a>        

                        <div class="top-info">
                            <a href="/<?=$module['url']?>">
                            <?php
                            if($keyGroup == 'DB_UNGROUP') {
                                echo $module['name'];
                            } else {
                                
                                $nameParts  = explode(':', $module['name']);                
                                $moduleName = $nameParts[1];
                                echo $moduleName;
                            }
                            ?>
                            </a>    
                        </div>        
                    </div>
                    
                  <?php }?>
                </div>
              </div>
            </div>
        <?php         
            $firstElement = false;
        } ?>
        </div>

    </div>
    
</div>