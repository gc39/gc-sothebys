<?php

App::uses('AppHelper', 'View/Helper');

class RelatedHelper extends AppHelper {
    
    public function getRelated($table, $value, $field) {
        
        App::import('Model', 'Content');
                
        $db = ConnectionManager::getDataSource('default');
        $returnValue = $db->query('SELECT * FROM '.$table.' WHERE '.$field.' = "'.$value.'"');         
        
        return $returnValue;        
                
    }
    
    public function getAllRelated($table, $field) {
        
        App::import('Model', 'Content');
                
        $db = ConnectionManager::getDataSource('default');
        $returnValue = $db->query('SELECT * FROM '.$table.' WHERE 1');         
        
        return $returnValue;        
                
    }
    
}

?>