<?php

App::uses('AppHelper', 'View/Helper');

class ImageCropHelper extends AppHelper {
    
    public function getFilename($id) {
        
        App::import('Model', 'ImageCrop');
        
        $ImageCropModel = new ImageCrop();
    
        return $ImageCropModel->getImageFilename($id);
                
    }
}

?>