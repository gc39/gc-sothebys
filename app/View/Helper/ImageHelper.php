<?php

App::uses('AppHelper', 'View/Helper');

class ImageHelper extends AppHelper {
    
    public function getFilename($id) {
        
        App::import('Model', 'Image');
        App::import('Model', 'GCDS');
        
        $ImageModel = new Image();
        $GcdsModel  = new GCDS();
    
        if(STANDALONE_MODE === false) {
            
            $clientFolder = $GcdsModel->getClientSubdomain();
            
            if($clientFolder === false) {
                die('No se pudo determinar la carpeta de imagenes del cliente');
            } 
            
        } else {
            $clientFolder = '';
        }
        
        return $clientFolder  . $ImageModel->getImageFilename($id);
                
    }
    
    public function getStaticFilename($filename) {
        
        App::import('Model', 'Image');
        App::import('Model', 'GCDS');
        
        $ImageModel = new Image();
        $GcdsModel  = new GCDS();
    
        if(STANDALONE_MODE === false) {
            
            $clientFolder = $GcdsModel->getClientSubdomain();
            
            if($clientFolder === false) {
                die('No se pudo determinar la carpeta de imagenes del cliente');
            } 
            
        } else {
            $clientFolder = '';
        }
        
        return $clientFolder  . '/' . $filename;
                
    }
}

?>