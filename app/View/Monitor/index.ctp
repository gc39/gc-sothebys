
<!-- Page header -->
<div class="page-header">
    <div class="page-title">
        <h3>Monitor</h3>
    </div>
</div>
<!-- /page header -->


<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="/">Home</a></li> 
        <li class='active'>Monitor</li> 
    </ul>

</div>
<!-- /breadcrumbs line -->

<div class="tabbable page-tabs">        
    <?php 
    if($players && $existPlayer != false ) {
    ?>
    <div class="block">
        
        <h6><i class="icon-play"></i> Reproductores Registrados <a href="/monitor" class="btn btn-xs btn-default pull-right" type="button"><i class="icon-rotate2"></i> Actualizar</a></h6>
        <br>
        <ul class="message-list">
            <?php
            //pr($players);

           // die('as');
		   
            foreach ($players as $k => $player) { 
            
               //    pr($k);
            
            if(isset($player->serial_number) && !empty($player->serial_number) && $player->serial_number != '31066') {
                $online      = isset($player->system->network->vpn->is_connected) && $player->system->network->vpn->is_connected == 1 ? true : false;
                $statusClass = $online ? 'status-success' : 'status-warning';
                                
                $disk_temperature = !isset($player->system) || $player->system->hardware->disk_temperature == '' ? 'N / A' : $player->system->hardware->disk_temperature. ' º';
                $cpu_temperature = !isset($player->system) || $player->system->hardware->cpu_temperature == '' ? 'N / A' : $player->system->hardware->cpu_temperature. ' º';
                $system_temperature = !isset($player->system) || $player->system->hardware->system_temperature == '' ? 'N / A' : $player->system->hardware->system_temperature . ' º';
                
                $data_disk_total = !isset($player->system) || $player->system->hardware->data_disk->total == '' ? 'N / A' : number_format($player->system->hardware->data_disk->total / 1024, 0, '', '.'). ' MB';
                $data_disk_free  = !isset($player->system) || $player->system->hardware->data_disk->free == '' ? 'N / A' : number_format($player->system->hardware->data_disk->free  / 1024, 0, '', '.'). ' MB';
                
                $system_disk_total = !isset($player->system) || $player->system->hardware->system_disk->total == '' ? 'N / A' : number_format($player->system->hardware->system_disk->total / 1024, 0, '', '.') . ' MB';
                $system_disk_free = !isset($player->system) || $player->system->hardware->system_disk->free == '' ? 'N / A' : number_format($player->system->hardware->system_disk->free / 1024, 0, '', '.') . ' MB';
                
                $channel_name  = !isset($player->all_channels[0]->name) ? 'N / A' : $player->all_channels[0]->name;
                
                $online_since  = !isset($player->system->up_since) ? 'N / A' : $player->system->up_since;
                $last_online_at  = !isset($player->system->network->vpn->last_connect_event_at) ? 'N/A' : $player->system->network->vpn->last_connect_event_at;
            ?>
            <li>
                <div class="clearfix">
                    <div class="chat-member">
                        
                        <?php 
                        if($online && $player->imageData) {
                        ?>
                        <a href="#"><img src="<?=$player->imageData?>" alt="" onclick="openScreencap(this)"></a>                         
                        <?php } else {?>
                        <img src="" alt="">
                        <?php } ?>
                        <h6><?=$player->player_name?> <span class="status <?=$statusClass?>"></span> <small>SERIAL: <?=$player->serial_number?></small></h6>
                    </div>
                    <div class="chat-actions">
                        <a class="btn btn-link btn-icon btn-xs" data-toggle="collapse" href="#player_<?=$player->serial_number?>"><i class="icon-info"></i></a>                        
                    </div>
                </div>

                <div class="panel-collapse collapse" id="player_<?=$player->serial_number?>">
                    <br>
                    <div class="callout callout-info fade in">
                        
                        <h5>Información Avanzada</h5>
                        <br>
                        <div class="block">
                            
                            <ul class="nav nav-pills nav-justified">
                                <li class=""><a href="#"><i class="icon-database"></i> Temperatura Disco <span class="label label-success"><?=$disk_temperature;?></span></a></a></li>
                                <li class=""><a href="#"><i class="icon-cog3"></i> Temperatura CPU <span class="label label-success"><?=$cpu_temperature;?></span></a></a></li>
                                <li class=""><a href="#"><i class="icon-laptop"></i> Temperatura Sistema <span class="label label-success"><?=$system_temperature;?></span></a></li>		                    
                            </ul>
                            <br>
                            <ul class="nav nav-pills nav-justified">
                                <li class=""><a href="#"><i class="icon-pie4"></i> Espacio Total en Disco <span class="label label-success"><?=$data_disk_total;?></span></a></a></li>
                                <li class=""><a href="#"><i class="icon-pie3"></i> Espacio Libre en Disco <span class="label label-success"><?=$data_disk_free;?></span></a></a></li>                                
                            </ul>
                            <br>
                            <ul class="nav nav-pills nav-justified">
                                <li class=""><a href="#"><i class="icon-pie4"></i> Espacio Total en Sistema <span class="label label-success"><?=$system_disk_total;?></span></a></a></li>
                                <li class=""><a href="#"><i class="icon-pie3"></i> Espacio Libre en Sistema <span class="label label-success"><?=$system_disk_free;?></span></a></a></li>                                
                            </ul>
                            <br>
                            <ul class="list-group">
                                <li class="list-group-item"><b>Serial: </b><?=$player->serial_number?></li>
                                <li class="list-group-item"><b>Modelo: </b><?=$player->system->hardware->model?></li>
                                <li class="list-group-item"><b>OEM Modelo: </b><?=$player->system->hardware->oem_model?></li>
                                <li class="list-group-item"><b>Canal: </b><?=$channel_name?></li>                                
                                <li class="list-group-item"><b>Online Desde: </b><?=$online_since?></li>                                
                                <li class="list-group-item"><b>Última Ves Online: </b><?=$last_online_at?></li>                                
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
                <?php } }?>
        </ul>
    </div>
    <?php } else { ?>       
    <div class="row">        
        <div class="col-md-6">
            <div class="alert alert-block alert-danger fade in block-inner">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h6><i class="icon-command"></i> Sin informacion disponible!</h6>
                <hr>
                <p>No se ha podido obtener informacion de los reproductores.</p>                
            </div>           
        </div>
    </div>    
    <?php } ?>
</div>

<!-- Modal with remote path -->
<div id="screencap_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">            
            <div class="modal-body">
              <img id="screencap-image" width="100%"/>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
            </div>
        </div>
    </div>
</div>
<!-- modal with remote path -->
            
<script>
    
    function openScreencap(image) {
        
        $('#screencap_modal img#screencap-image').attr('src', $(image).attr('src'));
        $('#screencap_modal').modal('show'); 
    }
</script>