
<div class="modal-body with-padding"> 
    <?php 
    if($status['success']) { ?>
    

        <?php if($clientFolder != 'sodexo'){
            $text = 'Su publicación sera procesada en unos minutos, para revisar el estado de su publicación ingrese al <a href="/publications">Centro de Publicacion</a>.';
        } else {
            $text = 'Su publicación será procesada y descargada en pantallas entre 20 minutos y 60 minutos aproximadamente.';
        } ?>
    
            <div class="alert alert-block alert-info fade in block-inner">            
                <h6><i class="icon-screen"></i> Publicacion en Proceso</h6>
                <hr>
                <p><?=$text?></p>
                <div class="text-left">                                
                    <a class="btn btn-info" href="/publications">Ir al Centro de Publicación</a> 
                    <?php if($showSendNotificationsButton) {?>
                    
                    <button type="button" data-dismiss="modal" class="btn btn-default" style="margin-right:4px"  data-toggle="modal"  href="/<?=$moduleKey?>/sendNotifications" data-target="#send_notifications_modal" onclick="javascript: $('#visual-loader').show();">Enviar Notificaciones</button> 
                    <?php } ?>
                    <a class="btn btn-default  pull-right" href="#" data-dismiss="modal">Cerrar</a> 
                    
                </div>
            </div>
    
            
    <?php } else { ?>
        <div class="alert alert-block alert-danger fade in block-inner">            
            <h6><i class="icon-eye7"></i> Fallo al Publicar!</h6>
            <hr>
            <p><?=$status['message']?></p>
            <div class="text-left">                                
                <a class="btn btn-info" href="#" data-dismiss="modal">Cerrar</a> 
            </div>
        </div>
    <?php } ?>
    
</div>

<script>
    
    $(document).ready(function() {                
        generateDatepickers();
         
        $('.btn-generate-report').click(function(){

             $('.btn-generate-report').attr( 'href', "/<?=$moduleKey?>/downloadReportsApp/" + $('.date-report').val());
             
        });
         
         
        $('#visual-loader').hide();
        
        $('.alert-<?=$clientFolder.'-'.$moduleKey?>').remove();
        
    });        
    
</script>