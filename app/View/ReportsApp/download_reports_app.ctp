<div class="modal-body with-padding"> 
    <div class="alert alert-block fade in block-inner">            
        <h6><i class="icon-screen"></i> Seleccione fecha para el reporte </h6>
        <div class="alert alert-danger fade in block-inner error-download hidden">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <i class="icon-cancel-circle"></i> Ocurrió un error al generar el archivo!
        </div>
        <div class="alert alert-success fade in block-inner success-download hidden">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <i class="icon-checkmark-circle"></i> El archivo se ha generado correctamente!
        </div>
        <p>fecha inicio:</p>
        <input class="form-control datepicker date-report" name="fecha" id="fecha" value="">

        <div class="text-left">                                
           <a type="button"  class="btn btn-info btn-generate-report disabled" style="margin-right:4px">Generar Reporte</a>

            <a class="btn btn-default  pull-right" href="#" data-dismiss="modal">Cerrar</a> 
        </div>
    </div>
</div>

<script>
    
    $(document).ready(function() {                
        generateDatepickers();
        $('#visual-loader').hide();
        
        var dateToday = new Date();
        var month = ((dateToday.getMonth() + 1) < 10)? '0' + (dateToday.getMonth() + 1) : dateToday.getMonth() + 1;
        var dateFormat = dateToday.getDate() + '/' + month + '/' +dateToday.getFullYear();

        $('.date-report').val(dateFormat);
        $('.btn-generate-report').removeClass('disabled');
        
        var date = dateFormat;
        date = $('.date-report').val().replace("/", "-");
        date = date.replace("/", "-");
        
        $('.date-report').change(function(){
            date = $('.date-report').val().replace("/", "-");
            date = date.replace("/", "-");
            $('.btn-generate-report').removeClass('disabled');
        });
         
        $('.btn-generate-report').click(function(){
            
            if($('.date-report').val() == ''){
                $('.btn-generate-report').addClass('disabled');
            }
            console.log('<?=$type?>');
            <?php if($type == 'count'){ ?>
                $.ajax({
                    url: "/<?=$moduleKey?>/downloadReportsCount/" + date,
                    type: 'POST',
                    beforeSend:function(){
                        $('.success-download').addClass('hidden');
                        $('.error-download').addClass('hidden'); 
                        $('.btn-generate-report').addClass('disabled');
                    },
                    success: function(data){
                        window.location = "/<?=$moduleKey?>/downloadReportsCount/" + date;
                        $('.success-download').removeClass('hidden'); 
                        $('.btn-generate-report').removeClass('disabled');
                    },
                    error: function(){
                        $('.error-download').removeClass('hidden'); 
                    }
                });
            <?php }else{ ?>
                $.ajax({
                    url: "/<?=$moduleKey?>/downloadReports/" + date,
                    type: 'POST',
                    beforeSend:function(){
                        $('.success-download').addClass('hidden');
                        $('.error-download').addClass('hidden'); 
                        $('.btn-generate-report').addClass('disabled');
                    },
                    success: function(data){
                        window.location = "/<?=$moduleKey?>/downloadReports/" + date;
                        $('.success-download').removeClass('hidden'); 
                        $('.btn-generate-report').removeClass('disabled');
                    },
                    error: function(){
                        $('.error-download').removeClass('hidden'); 
                    }
                });
            <?php } ?>
        });

        $('.alert-<?=$clientFolder.'-'.$moduleKey?>').remove();
        
    });        
    
</script>