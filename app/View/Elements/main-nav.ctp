<!-- Navbar -->
<div class="navbar navbar-inverse" role="navigation">
    <div class="navbar-header">
        <a class="navbar-brand" href="/">DIGITAL BOARD</a>
        <a class="sidebar-toggle"><i class="icon-paragraph-justify2"></i></a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
            <span class="sr-only">Toggle navbar</span>
            <i class="icon-grid3"></i>
        </button>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar">
            <span class="sr-only">Toggle navigation</span>
            <i class="icon-paragraph-justify2"></i>
        </button>
    </div>

    <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
        <li class="user dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown">
                <img src="/images/profile.png" alt="">
                <span><?=$userLogged['User']['fullname']?></span>
                <i class="caret"></i>
            </a>
            <ul class="user-menu dropdown-menu dropdown-menu-right icons-right">             
                <li><a href="#" data-toggle="modal" data-target="#change-password-modal"><i class="icon-key2"></i> Modificar Contraseña</a></li>
                <li><a href="/users/login"><i class="icon-exit"></i> Salir</a></li>
            </ul>
        </li>
    </ul>
</div>
<!-- /navbar -->