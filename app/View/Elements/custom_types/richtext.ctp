<div class="form-group form-group-<?=$field['@key']?>">
    <label class="col-sm-2 control-label text-right"><?=$field['@label']?></label>
    <div class="col-sm-10">       
        <div class="editor form-control" data-id="<?=$field['@key']?>" style="height: 300px" id="<?=$field['@key']?>_editor"><?=$content?></div> 
         <input class="hide form-control" name="<?=$field['@key']?>" id="<?=$field['@key']?>">
    </div>
</div>