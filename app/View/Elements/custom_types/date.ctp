<?php 

$dateParts = explode('-', $content);

if(isset($field['@no-transform-to-mysql']) && $field['@no-transform-to-mysql'] == true) {
    $content = count($dateParts) == 3 ? $dateParts[0].'/'.$dateParts[1].'/'.$dateParts[2] : '';
} else {
    $content = count($dateParts) == 3 ? $dateParts[2].'/'.$dateParts[1].'/'.$dateParts[0] : '';
}
?>
<div class="form-group form-group-<?=$field['@key']?>">
    <label class="col-sm-2 control-label text-right"><?=$field['@label']?></label>
    <div class="col-sm-10">
        <input class="form-control datepicker" autocomplete="off" name="<?=$field['@key']?>" id="<?=$field['@key']?>" value="<?=$content?>"/>
    </div>
</div>