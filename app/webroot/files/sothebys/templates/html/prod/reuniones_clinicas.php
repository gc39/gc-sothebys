<?php
$slides = array();

$directorio = 'files/clinicaalemana/templates_images_test/reuniones_clinicas/';
$folderImageTest = scandir($directorio);

foreach ($folderImageTest as $filesTest){
    
    if(!in_array($filesTest,array(".",".."))  ){
        $nameFile = explode('_', $filesTest);
        $fileStart = $nameFile[1];
        $fileFinish = $nameFile[2];
        $today = date('d-m-Y');
        
        if(strtotime($today) >= strtotime($fileStart)){
            $slides[] = $filesTest.'?var='.rand(50, 200);
        }
        
        if(empty($slides)){
            $slides[] = 'logo.jpg?var='.rand(50, 200);
        }
    }   
}

?>
<html><head>
        <meta charset="utf-8">        
        <title>Reuniones Clinicas</title>

        <script>

            var show = 0;
            var hide = null;
            var seconds = 0;

            function _(id) {
                return document.getElementById(id);
            }

            function slideTurnoInterval() {

                if (hide != null) {
                    _('slide-' + hide).style.display = 'none';
                }

                _('slide-' + show).style.display = 'block';
                seconds = _('slide-' + show).getAttribute('data-duration');

                hide = show;

                if (_('slide-' + (show + 1)) != null) {
                    show++;
                } else {
                    show = 0;
                }

                var a = setInterval(function () {

                    clearInterval(a);
                    slideTurnoInterval();

                }, seconds * 1000);
            }

        </script>
        <style>
            body { padding: 0px; margin: 0px;}

            #template { width: 100%; height: 100%;}

            .slide  { display: none; height: 955px; width: 1920px; background-color: #EDEDED; }
            .slide .container { width: 100%; height: 100%; background-color: #FFF; }

        </style>	
    </head>

    <body>

        <div id="template">	
    <?php
            foreach ($slides as $index => $slide) {
            ?>
                <div id="slide-<?= $index ?>" class="slide" data-duration="15">            
                    <div class="container">
                        <img class="image" src="/files/clinicaalemana/templates_images_test/reuniones_clinicas/<?= $slide ?>" height="1080" width="1920" <?php if ($index == 0) { ?>onload="slideTurnoInterval();" <?php } ?>>                  
                    </div>
                </div>
            <?php } ?>
        </div>                

    </body>    
</html>