<?php
$nombre_modulo   = $_GET['nombre_modulo'];
$directorio = __DIR__ . '/../../../templates_images_test/' . $nombre_modulo;
$folderImageTest = scandir($directorio);


foreach ($folderImageTest as $filesTest) {
	if (!in_array($filesTest, array(".", ".."))) {
		
		
		if ($indice == 'MC'){
			$Search =['_MC_','_indicadores'];
			foreach ($Search as $i => $n_arch) {
					if (strpos($filesTest, $n_arch) !== false) {
						//echo '/templates_images_test/' . $nombre_modulo . '/' . $filesTest . '?var=' . rand(50, 200);echo '<br>';
						$slides[] = '/templates_images_test/' . $nombre_modulo . '/' . $filesTest . '?var=' . rand(50, 200);
						//Extrae el nombre sin extension 00_nombre_20200101_20200101

					}
			}
		}else{
			// echo substr($filesTest, 0, 1) . ' - ' . $indice .  '<br>';

			//var_dump($filesTest);
			$slides[] = '/templates_images_test/' . $nombre_modulo . '/' . $filesTest . '?var=' . rand(50, 200);
			
		}
	}
}

if (empty($slides)) {
	// $slides[] = 'templates/img/logo.jpg?var=' . rand(50, 200);
}

?>

<html>

<head>
	<meta charset="utf-8">
	<title>Previsualización</title>
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script>
		$(document).ready(function() {
			attachImages();
			var num_visible_panels = $("div>div.panel-block:visible").length;
			if (num_visible_panels == 0) {
				$('.panel-floating-empty').css('display', 'block');
			}
		});

		function attachImages() {
			$(".panel-img img").click(function(obj) {
				var img = this.src;
				$('.panel-floating img')[0].src = this.src;
				$('.panel-floating').css('display', 'block');
			});

			$(".panel-img-vert img").click(function(obj) {
				var img = this.src;
				$('.panel-floating img')[0].src = this.src;
				$('.panel-floating').css('display', 'block');
			});

			$(".panel-floating").click(function(obj) {
				$('.panel-floating').css('display', 'none');
			});
		}

	</script>
</head>

<body>
	<div>
		<div class="panel-floating">
			<div class="panel-floating-img">
				<img />
			</div>
		</div> 

		<?php
		$counter = 0;
		foreach ($slides as $i => $slide) {
			if (($counter % 5) == 0) {
				if ($counter > 0) {
					echo "</div>";
				}
				echo "<div class='panel-block horizontal'>";
			}
			if (substr('horizontal', 0, 3) == 'hor')
				echo "<div class='panel-img'>";
			else
				echo "<div class='panel-img-vert'>";

			if (substr((end(explode(".", $slide))), 0, 3) == 'mp4'){
				?>
				<video src='/files/sothebys<?=$slide?>' poster='https://sothebys.digitalboard.app/files/sothebys/templates/img/img_video.jpg' class='style="z-index:-1"' height="200" controls="">
				</video>
				<?

			}else{
			?>
			<img src='/files/sothebys/<?=$slide?>' class='style="z-index:10"' />
			
			<?
			}
			echo '</div>';
			$counter++;
		?>
		<?php }  ?>
</body>

</html>