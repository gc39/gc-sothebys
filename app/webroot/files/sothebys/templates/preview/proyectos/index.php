<?php

define('CLIENTE', 'sothebys');
define('BASE_TEMPLATE', '/home/digitalboard/public_html/sub_dominios/'.CLIENTE.'/app/webroot/files/'.CLIENTE);
define('URL_BASE', 'https://'.CLIENTE.'.dev.digitalboard.app/files/'.CLIENTE);


$directorio = '/templates_images_test/proyectos';


$folderImageTest = scandir(BASE_TEMPLATE.$directorio);
var_dump($cliente);

foreach ($folderImageTest as $filesTest) {
	//var_dump($filesTest);
	if (!in_array($filesTest, array(".", ".."))) {
		$slides[] = URL_BASE.$directorio . '/' . $filesTest . '?var=' . rand(50, 200);
	}
}


?>

<html>

<head>
	<meta charset="utf-8">
	<title>Previsualización</title>
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script>
		$(document).ready(function() {
			attachImages();
			var num_visible_panels = $("div>div.panel-block:visible").length;
			if (num_visible_panels == 0) {
				$('.panel-floating-empty').css('display', 'block');
			}
		});

		function attachImages() {
			$(".panel-img img").click(function(obj) {
				var img = this.src;
				$('.panel-floating img')[0].src = this.src;
				$('.panel-floating').css('display', 'block');
			});

			$(".panel-img-vert img").click(function(obj) {
				var img = this.src;
				$('.panel-floating img')[0].src = this.src;
				$('.panel-floating').css('display', 'block');
			});

			$(".panel-floating").click(function(obj) {
				$('.panel-floating').css('display', 'none');
			});
		}

	</script>
</head>

<body>
	<div>
		<div class="panel-floating">
			<div class="panel-floating-img">
				<img />
			</div>
		</div> 

		<?php
		$counter = 0;
		foreach ($slides as $i => $slide) {
			if (($counter % 5) == 0) {
				if ($counter > 0) {
					echo "</div>";
				}
				echo "<div class='panel-block horizontal'>";
			}
			if (substr('horizontal', 0, 3) == 'hor')
				echo "<div class='panel-img'>";
			else
				echo "<div class='panel-img-vert'>";

			
			?>
			<img src='<?=$slide?>' class='style="z-index:10"' />
			
			<?
			
			echo '</div>';
			$counter++;
		?>
		<?php }  ?>
</body>

</html>