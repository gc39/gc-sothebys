<?php

define('BASE_TEMPLATE', WWW_ROOT . 'files' . '/' . $clientFolder);

$nombre_modulo = $this->moduleData['gcdb']['nombre_modulo'];  // Es el nombre del modulo

//tamaño de imagen
$max_width = 1920;
$max_height = 1080;

// sucursal por pagina
$imagen_por_pagina= 1;
// Número para ordenar el módulo en la playlist
$bloque = 0;

// Array con elementos para borrar contenido
$borrar_archivos = [$nombre_modulo, 'logo'];

//Logo default si no hay imagenes en carpeta
$defaultLogo = BASE_TEMPLATE . '/' . 'templates/img/bg_sin_imagen.jpg';

// Tipografia a utilizar
$BentonSansBook = BASE_TEMPLATE . '/templates/fonts/otf/BentonSansBook.otf';
$BentonSansMedium = BASE_TEMPLATE . '/templates/fonts/otf/BentonSansMedium.otf';




// patrones
$patterns[0] = 'á';
$patterns[1] = 'é';
$patterns[2] = 'í';
$patterns[3] = 'ó';
$patterns[4] = 'ú';

$replacements[0] = 'a';
$replacements[1] = 'e';
$replacements[2] = 'i';
$replacements[3] = 'o';
$replacements[4] = 'u';

// Horas servidor
$fecha = new DateTime("now", new DateTimeZone('America/Santiago'));
$fecha_hora = $fecha->format('ymd-His');
$current_time = $fecha->format('Y-m-d');
$today_ts     = strtotime($current_time);
$mesActual = $fecha->format('m');
$anioActual = $fecha->format('Y');
$fecha_hora = $fecha->format('Ymd-His'); 
$today =  $fecha->format("Y-m-d");
$numDias = cal_days_in_month(CAL_GREGORIAN, $mesActual, $anioActual);
$control = false;

// Carga de archivos json
$jsonFile     = file_get_contents(BASE_TEMPLATE . '/templates/json/' . AMBIENTE . '/' . $nombre_modulo . '-data.json');
$jsonData     = json_decode($jsonFile);

// Variables que se usan en test y prod
$slides = false;
// Preview
if (AMBIENTE == 'test') {
    // Variables
    $numPag = 1;
    $espacioX = 1;
    $spacing = 0;
    $imagenHorizontal_1=[];
    $imagenHorizontal_2=[];
    $imagenVertical_1=[];
    $imagenVertical_2=[];
    // recorre el json y genere un objeto con la data
    foreach ($jsonData as &$slide) {
        $date_ini = strtotime($slide->fecha_inicio);
        $date_ter = strtotime($slide->fecha_termino);
        $imgHorizontal_1   = $slide->imagen_Horizontal_1;
        $imgHorizontal_2   = $slide->imagen_Horizontal_2;
        $imgVertical_1     = $slide->imagen_vertical_1;
        $imgVertical_2     = $slide->imagen_vertical_2;
        if ($date_ini <= $date_ter) {
            for ($date = $slide->fecha_inicio; strtotime($date) <= strtotime($slide->fecha_termino); $date = date("d-m-Y", strtotime($date . "+ 1 days"))) {
                if (strtotime($date) == $today_ts) {
                    if(!empty($imgHorizontal_1) ){
                        array_push($imagenHorizontal_1, $slide); 
                        
                    }
                    if(!empty($imgHorizontal_2) ){
                        array_push($imagenHorizontal_2, $slide);   
                    }
                    if(!empty($imgVertical_1) ){
                        array_push($imagenVertical_1, $slide);   
                    }
                    if(!empty($imgVertical_2) ){
                        array_push($imagenVertical_2, $slide);   
                    }
       
                    $slides[$a] = $slide;
                    
                    $a++;
                }
            }
        }
    }

    // Obtiene el número de imagenes en el db
    $imgH_1=ceil(sizeof($imagenHorizontal_1));
    $imgH_2=ceil(sizeof($imagenHorizontal_2));
    $imagenesHorizontal= $imgH_1 + $imgH_2;

    $imgV_1=ceil(sizeof($imagenVertical_1));
    $imgV_2=ceil(sizeof($imagenVertical_2));

    $imagenesVertical= $imgV_1 + $imgV_2;

    // Obtiene el número de páginas
    $paginasHorizontal = ceil($imagenesHorizontal / $imagen_por_pagina);
    $paginasVertical = ceil($imagenesVertical / $imagen_por_pagina);

    //$pagina = ceil((sizeof($slides)*2)/$imagen_por_pagina);

    // Borrado del archivo en directorio preview
    $directorio = BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $nombre_modulo;
    $files = glob($directorio . '/*');
    foreach ($files as $file) {
        foreach ($borrar_archivos as $n_arch) {
            if (is_file(($file))) {
                if (strpos($file, $n_arch) !== false) {
                    unlink($file);
                }
            }
        }
    }


    $num=1; 
    $numImage=1;   
    foreach($slides as $slide){
        //Numero de paginas
        for($i=1 ; $i <= 2; $i++){
            // Crea la imagen de fondo
            $imageHandler     =  imagecreatetruecolor($max_width, $max_height);    // Ojo con las dimensiones
            $colors['white'] = imagecolorallocate($imageHandler, 255, 250, 250);
            $colors['yellow'] = imagecolorallocate($imageHandler, 193, 154, 61);
            $colors['blue'] = imagecolorallocate($imageHandler, 65, 67, 66);
            $colors['green'] = imagecolorallocate($imageHandler, 92,157, 51);
            //RUTA DEL FONDO a usar 
            $bgImage = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/bg_'.strtolower($slide->estilo).'.png'); 
           // $bgImage = imagecreatefromjpeg(BASE_TEMPLATE . '/templates/images/bg_'.strtolower($slide->estilo).'1.jpg'); 
                  
            // Crea fondo que se usa en la imagen 
            imagecopy($imageHandler, $bgImage, 0, 0, 0, 0, 1920, 1080);    // Ojo con las dimensiones 
            
            if($i == 1 ){ //Primera pagina

                if(strtolower($slide->estilo)== 'horizontal'){
                    // Imagen desde DB
                    $bgImageThumb = imagecreatefromjpeg(BASE_TEMPLATE . '/' . $slide->imagen_Horizontal_1);
                    // Propiedades de Alto y Ancho de la imagen
                    $arrayImage = getimagesize(BASE_TEMPLATE . '/' . $slide->imagen_Horizontal_1);
                    // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                    imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 810, $arrayImage[0], $arrayImage[1]);

                    if(strtolower($slide->huincha) == 'vendido' ){
                        // huincha
                        $bgImageThumb = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Huincha_VENDIDO.png');
                        // Propiedades de Alto y Ancho de la imagen
                        $arrayImage = getimagesize(BASE_TEMPLATE . '/templates/images/Huincha_VENDIDO.png');
                        // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                        imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 1080, $arrayImage[0], $arrayImage[1]);

                    }elseif(strtolower($slide->huincha) == 'nuevo precio' ){
                        
                       // huincha
                       $bgImageThumb = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Huincha_NUEVO-PRECIO.png');
                       // Propiedades de Alto y Ancho de la imagen
                       $arrayImage = getimagesize(BASE_TEMPLATE . '/templates/images/Huincha_NUEVO-PRECIO.png');
                       // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                       imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 1080, $arrayImage[0], $arrayImage[1]);

                    }
                    //Operacion /VENTA/ARRIENDO/VENTA Y ARRIENDO
                    $subcadena = substr($slide->operacion, 0, 40);//Obtener una parte específica de una cadena de caracteres
                   // list($x) = centerText($subcadena, $BentonSansMedium, 20, 100); //Centrar texto
                    imagettftext($imageHandler, 20, 0,  90, 873, $colors['yellow'], $BentonSansMedium,mb_strtoupper($subcadena));//Pinta Texto en Imagen

                    //Descripcion                
                    $subcadena = substr($slide->descripcion, 0, 30);//Obtener una parte específica de una cadena de caracteres
                    $lineas= wordwrap($subcadena, 12, "\n",true);//Ajusta un string hasta un número dado de caracteres
                    //list($x) = centerText($lineas, $BentonSansBook, 45, 100); //Centrar texto
                    imagettftext($imageHandler, 45, 0,  88, 943, $colors['white'], $BentonSansBook,$lineas);//Pinta Texto en Imagen
                    if($slide->valor_uf == 0){

                        //Valor en UF
                        $subcadena = 'Consultar  Precio';//Obtener una parte específica de una cadena de caracteres
                        // list($x) = centerText($subcadena, $BentonSansBook, 66, 400); //Centrar texto
                        imagettftext($imageHandler, 35, 0,  528, 970, $colors['yellow'], $BentonSansBook,mb_strtoupper($subcadena));//Pinta Texto en Imagen

                    }else{
                        //Valor en UF
                        $subcadena = substr($slide->valor_uf, 0, 10);//Obtener una parte específica de una cadena de caracteres
                        // list($x) = centerText($subcadena, $BentonSansBook, 66, 400); //Centrar texto
                        imagettftext($imageHandler, 66, 0,  570, 977, $colors['yellow'], $BentonSansBook,'UF '.mb_strtoupper($subcadena));//Pinta Texto en Imagen
 
                    }
 
                    if($slide->superficie_terreno == 0){ //si no posee dato en terreno muestra terraza en la imagen 
                        
                        if($slide->superficie_construida != 0){
                            //superficie_construida                
                            $subcadena = substr($slide->superficie_construida, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            list($x) = centerText($subcadena, $BentonSansBook, 31, 245); //Centrar texto
                            imagettftext($imageHandler, 31, 0,  1064+$x, 896, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //unidad_medida fija           
                            $subcadena = 'M2';//Obtener una parte específica de una cadena de caracteres
                            //list($x) = centerText($subcadena, $BentonSansBook, 17, 200); //Centrar texto
                            imagettftext($imageHandler, 17, 0,  1233, 864, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //texto superficie construida
                            $subcadena='SUP CONST';
                            // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                            imagettftext($imageHandler, 19, 0,  1125, 934, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
                        }else{
                            //no mostrar en vista
                        }
                        if($slide->superficie_terraza != 0){
                           //superficie_terraza                
                           $subcadena = substr($slide->superficie_terraza, 0, 6);//Obtener una parte específica de una cadena de caracteres
                           list($x) = centerText($subcadena, $BentonSansBook, 31, 245); //Centrar texto
                           imagettftext($imageHandler, 31, 0,  1280+$x, 896, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
   
                           //unidad_medidA FIJA          
                            $subcadena ='M2';//Obtener una parte específica de una cadena de caracteres
                           // list($x) = centerText($subcadena, $BentonSansBook, 17, 200); //Centrar texto
                           imagettftext($imageHandler, 17, 0,  1457, 864, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
       
                           //texto superficie terreno
                           $subcadena='SUP TERRAZA';
                           //list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                           imagettftext($imageHandler, 19, 0,  1324, 934, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
                        }else{
                            //no mostrar en vista
                        }
                    }else{

                        if($slide->superficie_construida != 0){
                            //superficie_construida                
                            $subcadena = substr($slide->superficie_construida, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            list($x) = centerText($subcadena, $BentonSansBook, 31, 245); //Centrar texto
                            imagettftext($imageHandler, 31, 0,  1064+$x, 896, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //unidad_medida fija           
                            $subcadena = 'M2';//Obtener una parte específica de una cadena de caracteres
                            //list($x) = centerText($subcadena, $BentonSansBook, 17, 200); //Centrar texto
                            imagettftext($imageHandler, 17, 0,  1233, 864, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //texto superficie construida
                            $subcadena='SUP CONST';
                            // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                            imagettftext($imageHandler, 19, 0,  1125, 934, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
                        }else{
                            //no mostrar en vista
                        }
                        if($slide->superficie_terreno != 0){
                            //unidad_medida pasada por el DB  TERRENO          
                            $subcadena = substr($slide->unidad_medida, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            // list($x) = centerText($subcadena, $BentonSansBook, 17, 233); //Centrar texto
                            imagettftext($imageHandler, 17, 0,  1457, 864, $colors['white'], $BentonSansBook,mb_strtoupper($subcadena));//Pinta Texto en Imagen
                            
                            //superficie_terreno                
                            $subcadena = substr($slide->superficie_terreno, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            list($x) = centerText($subcadena, $BentonSansBook, 31, 245); //Centrar texto
                            imagettftext($imageHandler, 31, 0,  1280+$x, 896, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
        
                            //texto superficie terreno
                            $subcadena='SUP TERRENO';
                            // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                            imagettftext($imageHandler, 19, 0,  1324, 934, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
                        }else{
                            //no mostrar en vista
                        }
                    }
                    if($slide->dormitorios !=0 ){
                        //Dormitorios               
                        $subcadena = substr($slide->dormitorios, 0, 6);//Obtener una parte específica de una cadena de caracteres
                        list($x) = centerText($subcadena, $BentonSansBook, 31, 245); //Centrar texto
                        imagettftext($imageHandler, 31, 0,  1037+$x, 997, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                        //texto Dormitorio
                        $subcadena='DORM';
                        // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                        imagettftext($imageHandler, 17, 0,  1125, 1033, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen


                    }else{
                        //no motrar
                    }
                    if( $slide->banos != 0){

                        //Baños             
                        $subcadena = substr($slide->banos, 0, 6);//Obtener una parte específica de una cadena de caracteres
                        list($x) = centerText($subcadena, $BentonSansBook, 31, 245); //Centrar texto
                        imagettftext($imageHandler, 31, 0,  1236+$x, 997, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                        //texto superficie terreno
                        $subcadena='BAÑOS';
                        // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                        imagettftext($imageHandler, 17, 0,  1320, 1033, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
                    }else{
                        //no motrar
                    }
  
                }elseif(strtolower($slide->estilo)== 'vertical'){

                    // Imagen desde DB
                    $bgImageThumb = imagecreatefromjpeg(BASE_TEMPLATE . '/' . $slide->imagen_vertical_1);
                    // Propiedades de Alto y Ancho de la imagen
                    $arrayImage = getimagesize(BASE_TEMPLATE . '/' . $slide->imagen_vertical_1);
                    // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                    imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1395, 1080, $arrayImage[0], $arrayImage[1]);

                    if(strtolower($slide->huincha) == 'vendido' ){
                        // huincha
                        $bgImageThumb = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Huincha_VENDIDO.png');
                        // Propiedades de Alto y Ancho de la imagen
                        $arrayImage = getimagesize(BASE_TEMPLATE . '/templates/images/Huincha_VENDIDO.png');
                        // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                        imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 1080, $arrayImage[0], $arrayImage[1]);

                    }elseif(strtolower($slide->huincha) == 'nuevo precio' ){
                        
                        // huincha
                        $bgImageThumb = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Huincha_NUEVO-PRECIO.png');
                        // Propiedades de Alto y Ancho de la imagen
                        $arrayImage = getimagesize(BASE_TEMPLATE . '/templates/images/Huincha_NUEVO-PRECIO.png');
                        // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                        imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 1080, $arrayImage[0], $arrayImage[1]);

                    }

                    //Operacion /VENTA/ARRIENDO/VENTA Y ARRIENDO
                    $subcadena = substr($slide->operacion, 0, 40);//Obtener una parte específica de una cadena de caracteres
                    // list($x) = centerText($subcadena, $BentonSansMedium, 20, 100); //Centrar texto
                    imagettftext($imageHandler, 20, 0,  1461, 305, $colors['yellow'], $BentonSansMedium,mb_strtoupper($subcadena));//Pinta Texto en Imagen

                    //Descripcion                
                    $subcadena = substr($slide->descripcion, 0, 30);//Obtener una parte específica de una cadena de caracteres
                    $lineas= wordwrap($subcadena, 12, "\n",true);//Ajusta un string hasta un número dado de caracteres
                    //list($x) = centerText($lineas, $BentonSansBook, 45, 100); //Centrar texto
                    imagettftext($imageHandler, 45, 0,  1455, 370, $colors['white'], $BentonSansBook,$lineas);//Pinta Texto en Imagen

                    if($slide->valor_uf == 0){

                        //Valor en UF
                        $subcadena = 'Consultar  Precio';//Obtener una parte específica de una cadena de caracteres
                        // list($x) = centerText($subcadena, $BentonSansBook, 66, 400); //Centrar texto
                        imagettftext($imageHandler, 30, 0,  1450, 575, $colors['yellow'], $BentonSansBook,mb_strtoupper($subcadena));//Pinta Texto en Imagen

                    }else{
                        //Valor en UF
                        $subcadena = substr($slide->valor_uf, 0, 10);//Obtener una parte específica de una cadena de caracteres
                        // list($x) = centerText($subcadena, $BentonSansBook, 66, 400); //Centrar texto
                        imagettftext($imageHandler, 64, 0,  1455, 580, $colors['yellow'], $BentonSansBook,'UF '.mb_strtoupper($subcadena));//Pinta Texto en Imagen
                    }



                    if($slide->superficie_terreno == 0){ //si no posee dato en terreno muestra terraza en la imagen
                        if($slide->superficie_construida != 0){
                            //superficie_construida                
                            $subcadena = substr($slide->superficie_construida, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            list($x) = centerText($subcadena, $BentonSansBook, 31, 247); //Centrar texto
                            imagettftext($imageHandler, 31, 0,  1393+$x, 687, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //unidad_medida fija           
                            $subcadena = 'M2';//Obtener una parte específica de una cadena de caracteres
                            //list($x) = centerText($subcadena, $BentonSansBook, 17, 200); //Centrar texto
                            imagettftext($imageHandler, 17, 0,  1555, 655, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //texto superficie construida
                            $subcadena='SUP CONST';
                            // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                            imagettftext($imageHandler, 19, 0,  1466, 725, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                        }else{
                        //no mostrar campo
                        } 
                        if($slide->superficie_terraza != 0){
                            //superficie_terraza                
                            $subcadena = substr($slide->superficie_terraza, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            list($x) = centerText($subcadena, $BentonSansBook, 31, 247); //Centrar texto
                            imagettftext($imageHandler, 31, 0,  1575+$x, 687, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //unidad_medidA FIJA          
                            $subcadena ='M2';//Obtener una parte específica de una cadena de caracteres
                            // list($x) = centerText($subcadena, $BentonSansBook, 17, 200); //Centrar texto
                            imagettftext($imageHandler, 17, 0,  1750, 655, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
        
                            //texto superficie terreno
                            $subcadena='SUP TERRAZA';
                            //list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                            imagettftext($imageHandler, 19, 0,  1650, 725, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                        }else{
                            //no mostrar campo
                        } 
                    }else{
                        if($slide->superficie_construida != 0){
                            //superficie_construida                
                            $subcadena = substr($slide->superficie_construida, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            list($x) = centerText($subcadena, $BentonSansBook, 31, 247); //Centrar texto
                            imagettftext($imageHandler, 31, 0,  1393+$x, 687, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //unidad_medida fija           
                            $subcadena = 'M2';//Obtener una parte específica de una cadena de caracteres
                            //list($x) = centerText($subcadena, $BentonSansBook, 17, 200); //Centrar texto
                            imagettftext($imageHandler, 17, 0,  1555, 655, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //texto superficie construida
                            $subcadena='SUP CONST';
                            // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                            imagettftext($imageHandler, 19, 0,  1466, 725, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                        }else{
                        //no mostrar campo
                        } 
                        if($slide->superficie_terreno != 0){
                            //unidad_medida pasada por el DB  TERRENO          
                            $subcadena = substr($slide->unidad_medida, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            // list($x) = centerText($subcadena, $BentonSansBook, 17, 233); //Centrar texto
                            imagettftext($imageHandler, 17, 0,  1750, 655, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
                                
                            //superficie_terreno                
                            $subcadena = substr($slide->superficie_terreno, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            list($x) = centerText($subcadena, $BentonSansBook, 31, 247); //Centrar texto
                            imagettftext($imageHandler, 31, 0,  1575+$x, 687, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
        
                            //texto superficie terreno
                            $subcadena='SUP TERRENO';
                            // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                            imagettftext($imageHandler, 19, 0,  1650, 725, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                        }else{
                            //no mostrar campo
                        }
                    }
                    if($slide->dormitorios != 0){
                        //Dormitorios               
                        $subcadena = substr($slide->dormitorios, 0, 6);//Obtener una parte específica de una cadena de caracteres
                        list($x) = centerText($subcadena, $BentonSansBook, 31, 247); //Centrar texto
                        imagettftext($imageHandler, 31, 0,  1365+$x, 809, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                        //texto Dormitorio
                        $subcadena='DORM';
                        // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                        imagettftext($imageHandler, 17, 0,  1467, 846, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
                    }else{
                        //no mostrar campo
                    }
                    if($slide->banos != 0){
                         //Baños             
                         $subcadena = substr($slide->banos, 0, 6);//Obtener una parte específica de una cadena de caracteres
                         list($x) = centerText($subcadena, $BentonSansBook, 31, 247); //Centrar texto
                         imagettftext($imageHandler, 31, 0,  1488+$x, 809, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
 
                         //texto superficie terreno
                         $subcadena='BAÑOS';
                         // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                         imagettftext($imageHandler, 17, 0,  1593, 846, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen    
                    }else{
                        //no mostrar campo
                    }
                }
                
            }else{  //Segunda Pagina 
              
                if(strtolower($slide->estilo)== 'horizontal'){

                    // Imagen desde DB
                    $bgImageThumb = imagecreatefromjpeg(BASE_TEMPLATE . '/' . $slide->imagen_Horizontal_2);
                    $bgImageThumb1 = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Mobile.png');
                    $bgImageThumb2 = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Mail.png');
     
                    // Propiedades de Alto y Ancho de la imagen
                    $arrayImage = getimagesize(BASE_TEMPLATE . '/' . $slide->imagen_Horizontal_2);
                    $arrayImage1 = getimagesize(BASE_TEMPLATE . '/templates/images/Mobile.png');
                    $arrayImage2 = getimagesize(BASE_TEMPLATE . '/templates/images/Mail.png');
                    // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                    imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 810, $arrayImage[0], $arrayImage[1]);
                    imagecopyresampled($imageHandler, $bgImageThumb1, 1140, 917, 0, 0, 21, 29, $arrayImage1[0], $arrayImage1[1]);
                    imagecopyresampled($imageHandler, $bgImageThumb2, 1138, 960, 0, 0, 28, 29, $arrayImage2[0], $arrayImage2[1]);

                    
                    if(strtolower($slide->huincha) == 'vendido' ){
                        // huincha
                        $bgImageThumb = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Huincha_VENDIDO.png');
                        // Propiedades de Alto y Ancho de la imagen
                        $arrayImage = getimagesize(BASE_TEMPLATE . '/templates/images/Huincha_VENDIDO.png');
                        // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                        imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 1080, $arrayImage[0], $arrayImage[1]);

                    }elseif(strtolower($slide->huincha) == 'nuevo precio' ){
                        
                       // huincha
                       $bgImageThumb = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Huincha_NUEVO-PRECIO.png');
                       // Propiedades de Alto y Ancho de la imagen
                       $arrayImage = getimagesize(BASE_TEMPLATE . '/templates/images/Huincha_NUEVO-PRECIO.png');
                       // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                       imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 1080, $arrayImage[0], $arrayImage[1]);
                    }
                    //titulo de propiedad
                    $subcadena = 'Código de propiedad';//Obtener una parte específica de una cadena de caracteres
                    //list($x) = centerText($subcadena, $BentonSansMedium, 20, 103); //Centrar texto
                    imagettftext($imageHandler, 27, 0,  90, 948, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                    //Codigo de Propiedad
                    $subcadena = substr($slide->codigo, 0, 10);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansMedium, 20, 103); //Centrar texto
                    imagettftext($imageHandler, 30, 0,  177+$x, 1007, $colors['white'], $BentonSansMedium,mb_strtoupper($subcadena));//Pinta Texto en Imagen

                    //titulo de propiedad
                    $subcadena = 'Contacta a nuestro ejecutivo';//Obtener una parte específica de una cadena de caracteres
                    //list($x) = centerText($subcadena, $BentonSansMedium, 20, 103); //Centrar texto
                    imagettftext($imageHandler, 27, 0,  547, 948, $colors['yellow'], $BentonSansMedium,$subcadena);//Pinta Texto en Imagen

                    //Nombre Ejecutiva
                    $subcadena = substr($slide->ejecutiva, 0, 25);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansBook, 28, 175); //Centrar texto
                    imagettftext($imageHandler, 28, 0,  1150+$x, 894, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                    //telefono
                    $subcadena = substr($slide->telefono, 0, 14);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansBook, 28, 175); //Centrar texto
                    imagettftext($imageHandler, 20, 0,  1212+$x, 943, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                    //email
                    $subcadena = substr($slide->email, 0, 30);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansBook, 28, 140); //Centrar texto
                    imagettftext($imageHandler, 20, 0,  1300+$x, 983, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                    //cargo
                    $subcadena = substr($slide->cargo, 0, 35);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansBook, 28, 200); //Centrar texto
                    imagettftext($imageHandler, 20, 0,  1317+$x, 1026, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                }elseif(strtolower($slide->estilo)== 'vertical'){
                    
                    // Imagen desde DB
                    $bgImageThumb = imagecreatefromjpeg(BASE_TEMPLATE . '/' . $slide->imagen_vertical_2);
                    $bgImageThumb1 = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Mobile.png');
                    $bgImageThumb2 = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Mail.png');
     
                    // Propiedades de Alto y Ancho de la imagen
                    $arrayImage = getimagesize(BASE_TEMPLATE . '/' . $slide->imagen_vertical_2);
                    $arrayImage1 = getimagesize(BASE_TEMPLATE . '/templates/images/Mobile.png');
                    $arrayImage2 = getimagesize(BASE_TEMPLATE . '/templates/images/Mail.png');
                    // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                    imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1395, 1080, $arrayImage[0], $arrayImage[1]);
                    imagecopyresampled($imageHandler, $bgImageThumb1, 1555, 691, 0, 0, 21, 29, $arrayImage1[0], $arrayImage1[1]);
                    imagecopyresampled($imageHandler, $bgImageThumb2, 1504, 734, 0, 0, 28, 29, $arrayImage2[0], $arrayImage2[1]);

                    
                    if(strtolower($slide->huincha) == 'vendido' ){
                        // huincha
                        $bgImageThumb = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Huincha_VENDIDO.png');
                        // Propiedades de Alto y Ancho de la imagen
                        $arrayImage = getimagesize(BASE_TEMPLATE . '/templates/images/Huincha_VENDIDO.png');
                        // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                        imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 1080, $arrayImage[0], $arrayImage[1]);

                    }elseif(strtolower($slide->huincha) == 'nuevo precio' ){
                        
                       // huincha
                       $bgImageThumb = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Huincha_NUEVO-PRECIO.png');
                       // Propiedades de Alto y Ancho de la imagen
                       $arrayImage = getimagesize(BASE_TEMPLATE . '/templates/images/Huincha_NUEVO-PRECIO.png');
                       // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                       imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 1080, $arrayImage[0], $arrayImage[1]);


                    }

                    //titulo de propiedad
                    $subcadena = 'Código de propiedad';//Obtener una parte específica de una cadena de caracteres
                    //list($x) = centerText($subcadena, $BentonSansMedium, 20, 103); //Centrar texto
                    imagettftext($imageHandler, 31, 0,  1466, 355, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                    //Codigo de Propiedad
                    $subcadena = substr($slide->codigo, 0, 10);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansMedium, 38, 205); //Centrar texto
                    imagettftext($imageHandler, 38, 0,  1559+$x, 423, $colors['white'], $BentonSansMedium,mb_strtoupper($subcadena));//Pinta Texto en Imagen

                    //titulo de propiedad
                    $subcadena = 'Contacta a nuestro';//Obtener una parte específica de una cadena de caracteres
                   // list($x) = centerText($subcadena, $BentonSansMedium, 27, 103); //Centrar texto
                    imagettftext($imageHandler, 27, 0,  1498, 556, $colors['yellow'], $BentonSansMedium,$subcadena);//Pinta Texto en Imagen

                    //titulo de propiedad
                    $subcadena = 'ejecutivo';//Obtener una parte específica de una cadena de caracteres
                    //list($x) = centerText($subcadena, $BentonSansMedium, 27, 103); //Centrar texto
                    imagettftext($imageHandler, 27, 0,  1589, 598, $colors['yellow'], $BentonSansMedium,$subcadena);//Pinta Texto en Imagen

                    //Nombre Ejecutiva
                    $subcadena = substr($slide->ejecutiva, 0, 25);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansBook, 28, 175); //Centrar texto
                    imagettftext($imageHandler, 28, 0,  1580+$x, 670, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                    //telefono
                    $subcadena = substr($slide->telefono, 0, 14);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansBook, 20, 165); //Centrar texto
                    imagettftext($imageHandler, 20, 0,  1597+$x, 714, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                    //email
                    $subcadena = substr($slide->email, 0, 30);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansBook, 20, 143); //Centrar texto
                    imagettftext($imageHandler, 20, 0,  1602+$x, 755, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                    //cargo
                    $subcadena = substr($slide->cargo, 0, 35);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansBook, 18.5, 200); //Centrar texto
                    imagettftext($imageHandler, 18.5, 0,1570+$x,813, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                }              
            }
        
            $paginas_cero = $num < 10 ? sprintf("%02d", $num) : $num; //agrega un cero a numero menores a 10              
            // Crea la imagen y la guarda en la carpeta correspondiente
            mkdir(BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $nombre_modulo);
            imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $nombre_modulo . '/' . $bloque . $paginas_cero . '_' . $nombre_modulo  .'_'.strtolower($slide->estilo).'_'.$numImage.'.jpg', 75);
            $num++;   
        }
        $numImage++;
    }
    if (empty($slides)) {
        copy($defaultLogo, BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $nombre_modulo . '/001_sin_sucursal.jpg');
    }
}

// Publicacion, todo lo de publicacion va aca
if (AMBIENTE == 'prod') {
    
    // Variables
    $numPag = 1;
    $espacioX = 1;
    $spacing = 0;
    $imagenHorizontal_1=[];
    $imagenHorizontal_2=[];
    $imagenVertical_1=[];
    $imagenVertical_2=[];
    // recorre el json y genere un objeto con la data
    foreach ($jsonData as &$slide) {
        $date_ini = strtotime($slide->fecha_inicio);
        $date_ter = strtotime($slide->fecha_termino);
        $imgHorizontal_1   = $slide->imagen_Horizontal_1;
        $imgHorizontal_2   = $slide->imagen_Horizontal_2;
        $imgVertical_1     = $slide->imagen_vertical_1;
        $imgVertical_2     = $slide->imagen_vertical_2;
        if ($date_ini <= $date_ter) {
            for ($date = $slide->fecha_inicio; strtotime($date) <= strtotime($slide->fecha_termino); $date = date("d-m-Y", strtotime($date . "+ 1 days"))) {
                if (strtotime($date) == $today_ts) {
                    if(!empty($imgHorizontal_1) ){
                        array_push($imagenHorizontal_1, $slide); 
                        
                    }
                    if(!empty($imgHorizontal_2) ){
                        array_push($imagenHorizontal_2, $slide);   
                    }
                    if(!empty($imgVertical_1) ){
                        array_push($imagenVertical_1, $slide);   
                    }
                    if(!empty($imgVertical_2) ){
                        array_push($imagenVertical_2, $slide);   
                    }
       
                    $slides[$a] = $slide;
                    
                    $a++;
                }
            }
        }
    }

    // Obtiene el número de imagenes en el db
    $imgH_1=ceil(sizeof($imagenHorizontal_1));
    $imgH_2=ceil(sizeof($imagenHorizontal_2));
    $imagenesHorizontal= $imgH_1 + $imgH_2;

    $imgV_1=ceil(sizeof($imagenVertical_1));
    $imgV_2=ceil(sizeof($imagenVertical_2));

    $imagenesVertical= $imgV_1 + $imgV_2;

    // Obtiene el número de páginas
    $paginasHorizontal = ceil($imagenesHorizontal / $imagen_por_pagina);
    $paginasVertical = ceil($imagenesVertical / $imagen_por_pagina);

    //$pagina = ceil((sizeof($slides)*2)/$imagen_por_pagina);

    // Borrado del archivo en directorio preview
    $directorio = BASE_TEMPLATE . '/' . 'templates_images' . '/' . $nombre_modulo;
    $files = glob($directorio . '/*');
    foreach ($files as $file) {
        foreach ($borrar_archivos as $n_arch) {
            if (is_file(($file))) {
                if (strpos($file, $n_arch) !== false) {
                    unlink($file);
                }
            }
        }
    }
    $num=1; 
    $numImage=1;   
    foreach($slides as $slide){
        // Les da el formato a las fechas de inicio y termino
        $dateSplitStart =  split('-', $slide->fecha_inicio);
        // Variable fecha para usar en el nombre del archivo
        $fechaIni = $dateSplitStart[0] . $dateSplitStart[1] . $dateSplitStart[2];

        $dateSplitEnd =  split('-', $slide->fecha_termino);
        // Variable fecha para usar en el nombre del archivo
        $fechaTer = $dateSplitEnd[0] . $dateSplitEnd[1] . $dateSplitEnd[2];
        // Obtiene los segundos que tendra la imagen
        $segundos = $slide->segundos;
        //Numero de paginas
        for($i=1 ; $i <= 2; $i++){
            // Crea la imagen de fondo
            $imageHandler     =  imagecreatetruecolor($max_width, $max_height);    // Ojo con las dimensiones
            $colors['white'] = imagecolorallocate($imageHandler, 255, 250, 250);
            $colors['yellow'] = imagecolorallocate($imageHandler, 193, 154, 61);
            $colors['blue'] = imagecolorallocate($imageHandler, 65, 67, 66);
            $colors['green'] = imagecolorallocate($imageHandler, 92,157, 51);
            //RUTA DEL FONDO a usar 
            $bgImage = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/bg_'.strtolower($slide->estilo).'.png'); 
           // $bgImage = imagecreatefromjpeg(BASE_TEMPLATE . '/templates/images/bg_'.strtolower($slide->estilo).'1.jpg'); 
                  
            // Crea fondo que se usa en la imagen 
            imagecopy($imageHandler, $bgImage, 0, 0, 0, 0, 1920, 1080);    // Ojo con las dimensiones 
            
            if($i == 1 ){ //Primera pagina
                if(strtolower($slide->estilo)== 'horizontal'){
                    // Imagen desde DB
                    $bgImageThumb = imagecreatefromjpeg(BASE_TEMPLATE . '/' . $slide->imagen_Horizontal_1);
                    // Propiedades de Alto y Ancho de la imagen
                    $arrayImage = getimagesize(BASE_TEMPLATE . '/' . $slide->imagen_Horizontal_1);
                    // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                    imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 810, $arrayImage[0], $arrayImage[1]);

                    if(strtolower($slide->huincha) == 'vendido' ){
                        // huincha
                        $bgImageThumb = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Huincha_VENDIDO.png');
                        // Propiedades de Alto y Ancho de la imagen
                        $arrayImage = getimagesize(BASE_TEMPLATE . '/templates/images/Huincha_VENDIDO.png');
                        // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                        imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 1080, $arrayImage[0], $arrayImage[1]);

                    }elseif(strtolower($slide->huincha) == 'nuevo precio' ){
                        
                       // huincha
                       $bgImageThumb = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Huincha_NUEVO-PRECIO.png');
                       // Propiedades de Alto y Ancho de la imagen
                       $arrayImage = getimagesize(BASE_TEMPLATE . '/templates/images/Huincha_NUEVO-PRECIO.png');
                       // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                       imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 1080, $arrayImage[0], $arrayImage[1]);

                    }
                    //Operacion /VENTA/ARRIENDO/VENTA Y ARRIENDO
                    $subcadena = substr($slide->operacion, 0, 40);//Obtener una parte específica de una cadena de caracteres
                   // list($x) = centerText($subcadena, $BentonSansMedium, 20, 100); //Centrar texto
                    imagettftext($imageHandler, 20, 0,  90, 873, $colors['yellow'], $BentonSansMedium,mb_strtoupper($subcadena));//Pinta Texto en Imagen

                    //Descripcion                
                    $subcadena = substr($slide->descripcion, 0, 30);//Obtener una parte específica de una cadena de caracteres
                    $lineas= wordwrap($subcadena, 12, "\n",true);//Ajusta un string hasta un número dado de caracteres
                    //list($x) = centerText($lineas, $BentonSansBook, 45, 100); //Centrar texto
                    imagettftext($imageHandler, 45, 0,  88, 943, $colors['white'], $BentonSansBook,$lineas);//Pinta Texto en Imagen
                    if($slide->valor_uf == 0){

                        //Valor en UF
                        $subcadena = 'Consultar  Precio';//Obtener una parte específica de una cadena de caracteres
                        // list($x) = centerText($subcadena, $BentonSansBook, 66, 400); //Centrar texto
                        imagettftext($imageHandler, 35, 0,  528, 970, $colors['yellow'], $BentonSansBook,mb_strtoupper($subcadena));//Pinta Texto en Imagen

                    }else{
                        //Valor en UF
                        $subcadena = substr($slide->valor_uf, 0, 10);//Obtener una parte específica de una cadena de caracteres
                        // list($x) = centerText($subcadena, $BentonSansBook, 66, 400); //Centrar texto
                        imagettftext($imageHandler, 66, 0,  570, 977, $colors['yellow'], $BentonSansBook,'UF '.mb_strtoupper($subcadena));//Pinta Texto en Imagen
 
                    }
 
                    if($slide->superficie_terreno == 0){ //si no posee dato en terreno muestra terraza en la imagen 
                        
                        if($slide->superficie_construida != 0){
                            //superficie_construida                
                            $subcadena = substr($slide->superficie_construida, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            list($x) = centerText($subcadena, $BentonSansBook, 31, 245); //Centrar texto
                            imagettftext($imageHandler, 31, 0,  1064+$x, 896, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //unidad_medida fija           
                            $subcadena = 'M2';//Obtener una parte específica de una cadena de caracteres
                            //list($x) = centerText($subcadena, $BentonSansBook, 17, 200); //Centrar texto
                            imagettftext($imageHandler, 17, 0,  1233, 864, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //texto superficie construida
                            $subcadena='SUP CONST';
                            // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                            imagettftext($imageHandler, 19, 0,  1125, 934, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
                        }else{
                            //no mostrar en vista
                        }
                        if($slide->superficie_terraza != 0){
                           //superficie_terraza                
                           $subcadena = substr($slide->superficie_terraza, 0, 6);//Obtener una parte específica de una cadena de caracteres
                           list($x) = centerText($subcadena, $BentonSansBook, 31, 245); //Centrar texto
                           imagettftext($imageHandler, 31, 0,  1280+$x, 896, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
   
                           //unidad_medidA FIJA          
                            $subcadena ='M2';//Obtener una parte específica de una cadena de caracteres
                           // list($x) = centerText($subcadena, $BentonSansBook, 17, 200); //Centrar texto
                           imagettftext($imageHandler, 17, 0,  1457, 864, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
       
                           //texto superficie terreno
                           $subcadena='SUP TERRAZA';
                           //list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                           imagettftext($imageHandler, 19, 0,  1324, 934, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
                        }else{
                            //no mostrar en vista
                        }
                    }else{

                        if($slide->superficie_construida != 0){
                            //superficie_construida                
                            $subcadena = substr($slide->superficie_construida, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            list($x) = centerText($subcadena, $BentonSansBook, 31, 245); //Centrar texto
                            imagettftext($imageHandler, 31, 0,  1064+$x, 896, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //unidad_medida fija           
                            $subcadena = 'M2';//Obtener una parte específica de una cadena de caracteres
                            //list($x) = centerText($subcadena, $BentonSansBook, 17, 200); //Centrar texto
                            imagettftext($imageHandler, 17, 0,  1233, 864, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //texto superficie construida
                            $subcadena='SUP CONST';
                            // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                            imagettftext($imageHandler, 19, 0,  1125, 934, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
                        }else{
                            //no mostrar en vista
                        }
                        if($slide->superficie_terreno != 0){
                            //unidad_medida pasada por el DB  TERRENO          
                            $subcadena = substr($slide->unidad_medida, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            // list($x) = centerText($subcadena, $BentonSansBook, 17, 233); //Centrar texto
                            imagettftext($imageHandler, 17, 0,  1457, 864, $colors['white'], $BentonSansBook,mb_strtoupper($subcadena));//Pinta Texto en Imagen
                            
                            //superficie_terreno                
                            $subcadena = substr($slide->superficie_terreno, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            list($x) = centerText($subcadena, $BentonSansBook, 31, 245); //Centrar texto
                            imagettftext($imageHandler, 31, 0,  1280+$x, 896, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
        
                            //texto superficie terreno
                            $subcadena='SUP TERRENO';
                            // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                            imagettftext($imageHandler, 19, 0,  1324, 934, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
                        }else{
                            //no mostrar en vista
                        }
                    }
                    if($slide->dormitorios !=0 ){
                        //Dormitorios               
                        $subcadena = substr($slide->dormitorios, 0, 6);//Obtener una parte específica de una cadena de caracteres
                        list($x) = centerText($subcadena, $BentonSansBook, 31, 245); //Centrar texto
                        imagettftext($imageHandler, 31, 0,  1037+$x, 997, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                        //texto Dormitorio
                        $subcadena='DORM';
                        // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                        imagettftext($imageHandler, 17, 0,  1125, 1033, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen


                    }else{
                        //no motrar
                    }
                    if( $slide->banos != 0){

                        //Baños             
                        $subcadena = substr($slide->banos, 0, 6);//Obtener una parte específica de una cadena de caracteres
                        list($x) = centerText($subcadena, $BentonSansBook, 31, 245); //Centrar texto
                        imagettftext($imageHandler, 31, 0,  1236+$x, 997, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                        //texto superficie terreno
                        $subcadena='BAÑOS';
                        // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                        imagettftext($imageHandler, 17, 0,  1320, 1033, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
                    }else{
                        //no motrar
                    }
  
                }elseif(strtolower($slide->estilo)== 'vertical'){

                    // Imagen desde DB
                    $bgImageThumb = imagecreatefromjpeg(BASE_TEMPLATE . '/' . $slide->imagen_vertical_1);
                    // Propiedades de Alto y Ancho de la imagen
                    $arrayImage = getimagesize(BASE_TEMPLATE . '/' . $slide->imagen_vertical_1);
                    // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                    imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1395, 1080, $arrayImage[0], $arrayImage[1]);

                    if(strtolower($slide->huincha) == 'vendido' ){
                        // huincha
                        $bgImageThumb = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Huincha_VENDIDO.png');
                        // Propiedades de Alto y Ancho de la imagen
                        $arrayImage = getimagesize(BASE_TEMPLATE . '/templates/images/Huincha_VENDIDO.png');
                        // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                        imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 1080, $arrayImage[0], $arrayImage[1]);

                    }elseif(strtolower($slide->huincha) == 'nuevo precio' ){
                        
                        // huincha
                        $bgImageThumb = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Huincha_NUEVO-PRECIO.png');
                        // Propiedades de Alto y Ancho de la imagen
                        $arrayImage = getimagesize(BASE_TEMPLATE . '/templates/images/Huincha_NUEVO-PRECIO.png');
                        // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                        imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 1080, $arrayImage[0], $arrayImage[1]);

                    }

                    //Operacion /VENTA/ARRIENDO/VENTA Y ARRIENDO
                    $subcadena = substr($slide->operacion, 0, 40);//Obtener una parte específica de una cadena de caracteres
                    // list($x) = centerText($subcadena, $BentonSansMedium, 20, 100); //Centrar texto
                    imagettftext($imageHandler, 20, 0,  1461, 305, $colors['yellow'], $BentonSansMedium,mb_strtoupper($subcadena));//Pinta Texto en Imagen

                    //Descripcion                
                    $subcadena = substr($slide->descripcion, 0, 30);//Obtener una parte específica de una cadena de caracteres
                    $lineas= wordwrap($subcadena, 12, "\n",true);//Ajusta un string hasta un número dado de caracteres
                    //list($x) = centerText($lineas, $BentonSansBook, 45, 100); //Centrar texto
                    imagettftext($imageHandler, 45, 0,  1455, 370, $colors['white'], $BentonSansBook,$lineas);//Pinta Texto en Imagen

                    if($slide->valor_uf == 0){

                        //Valor en UF
                        $subcadena = 'Consultar  Precio';//Obtener una parte específica de una cadena de caracteres
                        // list($x) = centerText($subcadena, $BentonSansBook, 66, 400); //Centrar texto
                        imagettftext($imageHandler, 30, 0,  1450, 575, $colors['yellow'], $BentonSansBook,mb_strtoupper($subcadena));//Pinta Texto en Imagen

                    }else{
                        //Valor en UF
                        $subcadena = substr($slide->valor_uf, 0, 10);//Obtener una parte específica de una cadena de caracteres
                        // list($x) = centerText($subcadena, $BentonSansBook, 66, 400); //Centrar texto
                        imagettftext($imageHandler, 64, 0,  1455, 580, $colors['yellow'], $BentonSansBook,'UF '.mb_strtoupper($subcadena));//Pinta Texto en Imagen
                    }



                    if($slide->superficie_terreno == 0){ //si no posee dato en terreno muestra terraza en la imagen
                        if($slide->superficie_construida != 0){
                            //superficie_construida                
                            $subcadena = substr($slide->superficie_construida, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            list($x) = centerText($subcadena, $BentonSansBook, 31, 247); //Centrar texto
                            imagettftext($imageHandler, 31, 0,  1393+$x, 687, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //unidad_medida fija           
                            $subcadena = 'M2';//Obtener una parte específica de una cadena de caracteres
                            //list($x) = centerText($subcadena, $BentonSansBook, 17, 200); //Centrar texto
                            imagettftext($imageHandler, 17, 0,  1555, 655, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //texto superficie construida
                            $subcadena='SUP CONST';
                            // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                            imagettftext($imageHandler, 19, 0,  1466, 725, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                        }else{
                        //no mostrar campo
                        } 
                        if($slide->superficie_terraza != 0){
                            //superficie_terraza                
                            $subcadena = substr($slide->superficie_terraza, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            list($x) = centerText($subcadena, $BentonSansBook, 31, 247); //Centrar texto
                            imagettftext($imageHandler, 31, 0,  1575+$x, 687, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //unidad_medidA FIJA          
                            $subcadena ='M2';//Obtener una parte específica de una cadena de caracteres
                            // list($x) = centerText($subcadena, $BentonSansBook, 17, 200); //Centrar texto
                            imagettftext($imageHandler, 17, 0,  1750, 655, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
        
                            //texto superficie terreno
                            $subcadena='SUP TERRAZA';
                            //list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                            imagettftext($imageHandler, 19, 0,  1650, 725, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                        }else{
                            //no mostrar campo
                        } 
                    }else{
                        if($slide->superficie_construida != 0){
                            //superficie_construida                
                            $subcadena = substr($slide->superficie_construida, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            list($x) = centerText($subcadena, $BentonSansBook, 31, 247); //Centrar texto
                            imagettftext($imageHandler, 31, 0,  1393+$x, 687, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //unidad_medida fija           
                            $subcadena = 'M2';//Obtener una parte específica de una cadena de caracteres
                            //list($x) = centerText($subcadena, $BentonSansBook, 17, 200); //Centrar texto
                            imagettftext($imageHandler, 17, 0,  1555, 655, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                            //texto superficie construida
                            $subcadena='SUP CONST';
                            // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                            imagettftext($imageHandler, 19, 0,  1466, 725, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                        }else{
                        //no mostrar campo
                        } 
                        if($slide->superficie_terreno != 0){
                            //unidad_medida pasada por el DB  TERRENO          
                            $subcadena = substr($slide->unidad_medida, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            // list($x) = centerText($subcadena, $BentonSansBook, 17, 233); //Centrar texto
                            imagettftext($imageHandler, 17, 0,  1750, 655, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
                                
                            //superficie_terreno                
                            $subcadena = substr($slide->superficie_terreno, 0, 6);//Obtener una parte específica de una cadena de caracteres
                            list($x) = centerText($subcadena, $BentonSansBook, 31, 247); //Centrar texto
                            imagettftext($imageHandler, 31, 0,  1575+$x, 687, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
        
                            //texto superficie terreno
                            $subcadena='SUP TERRENO';
                            // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                            imagettftext($imageHandler, 19, 0,  1650, 725, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                        }else{
                            //no mostrar campo
                        }
                    }
                    if($slide->dormitorios != 0){
                        //Dormitorios               
                        $subcadena = substr($slide->dormitorios, 0, 6);//Obtener una parte específica de una cadena de caracteres
                        list($x) = centerText($subcadena, $BentonSansBook, 31, 247); //Centrar texto
                        imagettftext($imageHandler, 31, 0,  1365+$x, 809, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                        //texto Dormitorio
                        $subcadena='DORM';
                        // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                        imagettftext($imageHandler, 17, 0,  1467, 846, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
                    }else{
                        //no mostrar campo
                    }
                    if($slide->banos != 0){
                         //Baños             
                         $subcadena = substr($slide->banos, 0, 6);//Obtener una parte específica de una cadena de caracteres
                         list($x) = centerText($subcadena, $BentonSansBook, 31, 247); //Centrar texto
                         imagettftext($imageHandler, 31, 0,  1488+$x, 809, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen
 
                         //texto superficie terreno
                         $subcadena='BAÑOS';
                         // list($x) = centerText($subcadena, $BentonSansBook, 18, 200); //Centrar texto
                         imagettftext($imageHandler, 17, 0,  1593, 846, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen    
                    }else{
                        //no mostrar campo
                    }
                }
                
            }else{  //Segunda Pagina 
              
                if(strtolower($slide->estilo)== 'horizontal'){

                    // Imagen desde DB
                    $bgImageThumb = imagecreatefromjpeg(BASE_TEMPLATE . '/' . $slide->imagen_Horizontal_2);
                    $bgImageThumb1 = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Mobile.png');
                    $bgImageThumb2 = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Mail.png');
     
                    // Propiedades de Alto y Ancho de la imagen
                    $arrayImage = getimagesize(BASE_TEMPLATE . '/' . $slide->imagen_Horizontal_2);
                    $arrayImage1 = getimagesize(BASE_TEMPLATE . '/templates/images/Mobile.png');
                    $arrayImage2 = getimagesize(BASE_TEMPLATE . '/templates/images/Mail.png');
                    // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                    imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 810, $arrayImage[0], $arrayImage[1]);
                    imagecopyresampled($imageHandler, $bgImageThumb1, 1140, 917, 0, 0, 21, 29, $arrayImage1[0], $arrayImage1[1]);
                    imagecopyresampled($imageHandler, $bgImageThumb2, 1138, 960, 0, 0, 28, 29, $arrayImage2[0], $arrayImage2[1]);

                    
                    if(strtolower($slide->huincha) == 'vendido' ){
                        // huincha
                        $bgImageThumb = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Huincha_VENDIDO.png');
                        // Propiedades de Alto y Ancho de la imagen
                        $arrayImage = getimagesize(BASE_TEMPLATE . '/templates/images/Huincha_VENDIDO.png');
                        // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                        imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 1080, $arrayImage[0], $arrayImage[1]);

                    }elseif(strtolower($slide->huincha) == 'nuevo precio' ){
                        
                       // huincha
                       $bgImageThumb = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Huincha_NUEVO-PRECIO.png');
                       // Propiedades de Alto y Ancho de la imagen
                       $arrayImage = getimagesize(BASE_TEMPLATE . '/templates/images/Huincha_NUEVO-PRECIO.png');
                       // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                       imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 1080, $arrayImage[0], $arrayImage[1]);
                    }
                    //titulo de propiedad
                    $subcadena = 'Código de propiedad';//Obtener una parte específica de una cadena de caracteres
                    //list($x) = centerText($subcadena, $BentonSansMedium, 20, 103); //Centrar texto
                    imagettftext($imageHandler, 27, 0,  90, 948, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                    //Codigo de Propiedad
                    $subcadena = substr($slide->codigo, 0, 10);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansMedium, 20, 103); //Centrar texto
                    imagettftext($imageHandler, 30, 0,  177+$x, 1007, $colors['white'], $BentonSansMedium,mb_strtoupper($subcadena));//Pinta Texto en Imagen

                    //titulo de propiedad
                    $subcadena = 'Contacta a nuestro ejecutivo';//Obtener una parte específica de una cadena de caracteres
                    //list($x) = centerText($subcadena, $BentonSansMedium, 20, 103); //Centrar texto
                    imagettftext($imageHandler, 27, 0,  547, 948, $colors['yellow'], $BentonSansMedium,$subcadena);//Pinta Texto en Imagen

                    //Nombre Ejecutiva
                    $subcadena = substr($slide->ejecutiva, 0, 25);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansBook, 28, 175); //Centrar texto
                    imagettftext($imageHandler, 28, 0,  1150+$x, 894, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                    //telefono
                    $subcadena = substr($slide->telefono, 0, 14);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansBook, 28, 175); //Centrar texto
                    imagettftext($imageHandler, 20, 0,  1212+$x, 943, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                    //email
                    $subcadena = substr($slide->email, 0, 30);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansBook, 28, 140); //Centrar texto
                    imagettftext($imageHandler, 20, 0,  1300+$x, 983, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                    //cargo
                    $subcadena = substr($slide->cargo, 0, 35);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansBook, 28, 200); //Centrar texto
                    imagettftext($imageHandler, 20, 0,  1317+$x, 1026, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                }elseif(strtolower($slide->estilo)== 'vertical'){
                    
                    // Imagen desde DB
                    $bgImageThumb = imagecreatefromjpeg(BASE_TEMPLATE . '/' . $slide->imagen_vertical_2);
                    $bgImageThumb1 = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Mobile.png');
                    $bgImageThumb2 = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Mail.png');
     
                    // Propiedades de Alto y Ancho de la imagen
                    $arrayImage = getimagesize(BASE_TEMPLATE . '/' . $slide->imagen_vertical_2);
                    $arrayImage1 = getimagesize(BASE_TEMPLATE . '/templates/images/Mobile.png');
                    $arrayImage2 = getimagesize(BASE_TEMPLATE . '/templates/images/Mail.png');
                    // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                    imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1395, 1080, $arrayImage[0], $arrayImage[1]);
                    imagecopyresampled($imageHandler, $bgImageThumb1, 1555, 691, 0, 0, 21, 29, $arrayImage1[0], $arrayImage1[1]);
                    imagecopyresampled($imageHandler, $bgImageThumb2, 1504, 734, 0, 0, 28, 29, $arrayImage2[0], $arrayImage2[1]);

                    
                    if(strtolower($slide->huincha) == 'vendido' ){
                        // huincha
                        $bgImageThumb = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Huincha_VENDIDO.png');
                        // Propiedades de Alto y Ancho de la imagen
                        $arrayImage = getimagesize(BASE_TEMPLATE . '/templates/images/Huincha_VENDIDO.png');
                        // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                        imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 1080, $arrayImage[0], $arrayImage[1]);

                    }elseif(strtolower($slide->huincha) == 'nuevo precio' ){
                        
                       // huincha
                       $bgImageThumb = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/Huincha_NUEVO-PRECIO.png');
                       // Propiedades de Alto y Ancho de la imagen
                       $arrayImage = getimagesize(BASE_TEMPLATE . '/templates/images/Huincha_NUEVO-PRECIO.png');
                       // imagecopyresampled($imageHandler (destino donde pegar), $bgImageThumb (imagen que pegar), $x (mov en fondo), $y (mov en el fondo), $y (punto de origen), $x (punto de origen), $ancho (que tendrá en el fondo), $alto (que tendrá en el fondo), $ancho (original), $alto (original) )
                       imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 1080, $arrayImage[0], $arrayImage[1]);


                    }

                    //titulo de propiedad
                    $subcadena = 'Código de propiedad';//Obtener una parte específica de una cadena de caracteres
                    //list($x) = centerText($subcadena, $BentonSansMedium, 20, 103); //Centrar texto
                    imagettftext($imageHandler, 31, 0,  1466, 355, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                    //Codigo de Propiedad
                    $subcadena = substr($slide->codigo, 0, 10);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansMedium, 38, 205); //Centrar texto
                    imagettftext($imageHandler, 38, 0,  1559+$x, 423, $colors['white'], $BentonSansMedium,mb_strtoupper($subcadena));//Pinta Texto en Imagen

                    //titulo de propiedad
                    $subcadena = 'Contacta a nuestro';//Obtener una parte específica de una cadena de caracteres
                   // list($x) = centerText($subcadena, $BentonSansMedium, 27, 103); //Centrar texto
                    imagettftext($imageHandler, 27, 0,  1498, 556, $colors['yellow'], $BentonSansMedium,$subcadena);//Pinta Texto en Imagen

                    //titulo de propiedad
                    $subcadena = 'ejecutivo';//Obtener una parte específica de una cadena de caracteres
                    //list($x) = centerText($subcadena, $BentonSansMedium, 27, 103); //Centrar texto
                    imagettftext($imageHandler, 27, 0,  1589, 598, $colors['yellow'], $BentonSansMedium,$subcadena);//Pinta Texto en Imagen

                    //Nombre Ejecutiva
                    $subcadena = substr($slide->ejecutiva, 0, 25);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansBook, 28, 175); //Centrar texto
                    imagettftext($imageHandler, 28, 0,  1580+$x, 670, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                    //telefono
                    $subcadena = substr($slide->telefono, 0, 14);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansBook, 20, 165); //Centrar texto
                    imagettftext($imageHandler, 20, 0,  1597+$x, 714, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                    //email
                    $subcadena = substr($slide->email, 0, 30);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansBook, 20, 143); //Centrar texto
                    imagettftext($imageHandler, 20, 0,  1602+$x, 755, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                    //cargo
                    $subcadena = substr($slide->cargo, 0, 35);//Obtener una parte específica de una cadena de caracteres
                    list($x) = centerText($subcadena, $BentonSansBook, 18.5, 200); //Centrar texto
                    imagettftext($imageHandler, 18.5, 0,1570+$x,813, $colors['white'], $BentonSansBook,$subcadena);//Pinta Texto en Imagen

                }              
            }
            
          for ($recorre_fecha = $slide->fecha_inicio; strtotime($recorre_fecha) <= strtotime($slide->fecha_termino); $recorre_fecha = date("Y-m-d", strtotime($recorre_fecha . "+ 1 days"))) {
                $date_parts = split('-', $recorre_fecha);
                $date_parts[2] = ($date_parts[2] < 10) ? str_replace("0", "", $date_parts[2]) : $date_parts[2];
                $slide_ts = strtotime($recorre_fecha);

                // Agrega un 0 si el orden es menor a 10 para agregar al nombre del archivo jpg a crear
            // $paginas_cero = $orden < 10 ? sprintf("%02d", $orden) : $orden;
                $paginas_cero = $num < 10 ? sprintf("%02d", $num) : $num;
                // Crea la carpeta para guardar las imagenes publicadas
                mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $nombre_modulo);
                $xSeg=$segundos/5;
            // echo $xSeg.'</br>';
                for ($seg = 1; $seg <= $xSeg; $seg++) {
                    // Genera y guarda la imagen en la carpeta respectiva
                    imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $nombre_modulo . '/' .$paginas_cero.'_' . $nombre_modulo . '-' . $paginas_cero . '_' . $seg . '_' .$fecha_hora . '_' .$fechaIni . '_' . $fechaTer.'_'.strtolower($slide->estilo).'_'.$numImage . '.jpg');
                    //imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $nombre_modulo . '/'  . $paginas_cero . '_' . $nombre_modulo . '_' . $seg . '_' .$fecha_hora . '_' .$fechaIni . '_' . $fechaTer . '.jpg');

                }
            }
            $num++;   
        }
        $numImage++;
    }
    if (empty($slides)) {
        copy($defaultLogo, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $nombre_modulo . '/001_sin_cargos_' . $fecha_hora . '_' . $anioActual . $mesActual . '01' . '_' . $anioActual . $mesActual . $numDias . '.jpg');
        copy($defaultLogo, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $nombre_modulo . '/001_sin_cargos_' . $fecha_hora . '_' . $anioActual . $mesActual . '01' . '_' . $anioActual . $mesActual . $numDias . '.jpg');
        copy($defaultLogo, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $nombre_modulo . '/001_sin_cargos_' . $fecha_hora . '_' . $anioActual . $mesActual . '01' . '_' . $anioActual . $mesActual . $numDias . '.jpg');
    }
    // Si no hay imagenes en la carpeta copia logo default
    // $contador_archivos = 0;
    // $directorio = BASE_TEMPLATE . '/' . 'templates_images' . '/' . $nombre_modulo;
    // $files = glob($directorio . '/*');
    // foreach ($files as $file) {
    //     $contador_archivos++;
    // }
    // if ($contador_archivos == 0) {
    //     copy($defaultLogo, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $nombre_modulo . '/bg_sin_cumple_' . $fecha_hora . '_' . $anioActual . $mesActual . '01' . '_' . $anioActual . $mesActual . $numDias . '.jpg');
    // }

    // Copia (crea, reemplaza) la carpeta de prod al FTP
    shell_exec(PHP_RUTA . ' ' . CARPETA_SHELL_EXEC . 'copiaArchFTP.php ' . $nombre_modulo . ' > /dev/null 2>&1 &');

   //echo(shell_exec(PHP_RUTA . ' ' . CARPETA_SHELL_EXEC . 'copiaArchFTP.php ' . $nombre_modulo . ' > /dev/null 2>&1 &')) ;
}

// libera memoria de sistema
imagedestroy($bgImage);
imagedestroy($imageHandler);

// funcion para centrar texto
function centerText($text, $font, $size, $xi)
{
    $box = ImageTTFBBox($size, 0, $font, $text);
    $xr = abs(max($box[2], $box[4]));
    $x = intval(($xi - $xr) / 2);

    return array($x);
}

// funcion para crear bloque de texto
function makeTextBlock($text, $fontfile, $fontsize, $width)
{
    $words = explode(' ', $text);
    $lines = array($words[0]);
    $currentLine = 0;
    for ($i = 1; $i < count($words); $i++) {
        $lineSize = imagettfbbox($fontsize, 0, $fontfile, $lines[$currentLine] . ' ' . $words[$i]);
        if ($lineSize[2] - $lineSize[0] < $width) {
            $lines[$currentLine] .= ' ' . $words[$i];
        } else {
            $currentLine++;
            $lines[$currentLine] = $words[$i];
        }
    }

    return $lines;
}

// funcion para pasar texto a la imagen
function imagettftextSp($image, $size, $angle, $x, $y, $color, $font, $text, $spacing)
{
    if ($spacing == 0) {
        imagettftext($image, $size, $angle, $x, $y, $color, $font, $text);
    } else {
        $temp_x = $x;
        for ($i = 0; $i < strlen($text); $i++) {
            $bbox = imagettftext($image, $size, $angle, $temp_x, $y, $color, $font, $text[$i]);
            $temp_x += $spacing + ($bbox[2] - $bbox[0]);
        }
    }
}

?> 
