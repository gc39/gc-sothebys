<?php

$patterns[0] = 'á';
$patterns[1] = 'é';
$patterns[2] = 'í';
$patterns[3] = 'ó';
$patterns[4] = 'ú';
$patterns[5] = 'ñ';
$patterns[6] = 'Ñ';
//$patterns[6] = 'Ú';

$replacements[0] = 'Ì';
$replacements[1] = 'Ó';
$replacements[2] = '×';
$replacements[3] = 'Þ';
$replacements[4] = 'å';
$replacements[5] = 'Ü';
$replacements[6] = 'ÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãä - ';
$replacements[5] = 'Æ';


define('BASE_TEMPLATE', WWW_ROOT .'files' . DS . $clientFolder); 

$jsonFile = file_get_contents(BASE_TEMPLATE . '/templates/json/' . AMBIENTE . '/agenda-data.json');
$jsonData = json_decode($jsonFile);

$slides = false;
$slidesOrde = false;
$datesSlideIni = false;
$datesSlideTer = false;
$current_time = date('d-m-Y');
$today_ts     = strtotime($current_time);
$days = array('DOMINGO', 'LUNES', 'MARTES', 'MIÉRCOLES', 'JUEVES', 'VIERNES', 'SÁBADO');
$monthsArray = array('', 'ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE');
$a = 1;
$aa = 0;

foreach ($jsonData as &$slide) { 
    $slides[] = $slide;
}

//usort($slides, 'sortByOrderDate');
usort($slides, 'sortByOrder');

foreach ($slides as &$slide) { 
    
    $dateIni = strtotime($slide->fecha_inicio);
    $dateTer = strtotime($slide->fecha_termino);
   // $slide->descripcion = str_replace($patterns, $replacements, $slide->descripcion);
    
    if( $today_ts <= $dateTer){
        
        if($aa >= 3){
           $a++;
           $aa = 1;
           $slide->slide = $a;
        }else{
           $a = $a;
           $aa++;
            $slide->slide = $a;
        }
        
        if(isset($datesSlideIni[$a])){
            if(strtotime($slide->fecha_inicio) < strtotime($datesSlideIni[$a])){
                $datesSlideIni[$a] = $slide->fecha_inicio;
            }else{
                $datesSlideIni[$a] = $datesSlideIni[$a];
            }
            
        }else{
            $datesSlideIni[$a] = $slide->fecha_inicio;
        }
        
        if(isset($datesSlideTer[$a])){
            if(strtotime($slide->fecha_termino) > strtotime($datesSlideTer[$a])){
                $datesSlideTer[$a] = $slide->fecha_termino;
            }else{
                $datesSlideTer[$a] = $datesSlideTer[$a];
            }
            
        }else{
            $datesSlideTer[$a] = $slide->fecha_termino;
        }

        $slidesOrde[$a][$aa] = $slide;
    }
    
}
//echo '<pre>';print_r($slidesOrde); echo '</pre>';
$ambiente = AMBIENTE == 'test' ? 'templates_images_test' : 'templates_images';

$directorio = BASE_TEMPLATE . DS . $ambiente . DS . 'agenda';
$folderImageTest = scandir($directorio);

foreach ($folderImageTest as $filesTest){
    if(!in_array($filesTest,array(".",".."))  ){
        @unlink( $directorio . DS . $filesTest);
    }   
}

$InterstateBlack   = BASE_TEMPLATE . '/templates/fonts/Interstate Black.ttf';
$InterstateLight   =BASE_TEMPLATE . '/templates/fonts/Interstate Regular.ttf';
$InterstateBold   = BASE_TEMPLATE . '/templates/fonts/Interstate Bold.ttf';

   
$bgImage        = imagecreatefromjpeg(BASE_TEMPLATE . '/templates/images/bg_agenda_v2.jpg');
$bgImage2        = imagecreatefromjpeg(BASE_TEMPLATE . '/templates/images/ClinicaAlemana.jpg');
if(!empty($slidesOrde) && isset($slidesOrde)){

    foreach($slidesOrde as $indexDates => $dates) {

        $imageHandler    = @imagecreatetruecolor(1920, 1080);
        $colors['white'] = imagecolorallocate($imageHandler, 255, 255, 255);
        $colors['gray'] = imagecolorallocate($imageHandler, 103, 104, 104);  
        $colors['lightBlue'] = imagecolorallocate($imageHandler, 45, 149, 150);
        $colors['orange'] = imagecolorallocate($imageHandler, 254, 128, 52);

        imagecopy($imageHandler, $bgImage, 0, 0, 0, 0, 1920, 1080);

        $dateSplitStart =  split( '-', $datesSlideIni[$indexDates] );
        $dateSplitStart = $dateSplitStart[2].'-'.$dateSplitStart[1].'-'.$dateSplitStart[0];
        $dateSplitEnd =  split( '-', $datesSlideTer[$indexDates] );
        $dateSplitEnd = $dateSplitEnd[2].'-'.$dateSplitEnd[1].'-'.$dateSplitEnd[0];

        $y = 239;

        $x = 128;
        $yDate = (count($dates) == 3)? 250 : 355;

        foreach ($dates as $viewIndex => $view){
             $ytext = 239 + 380;
            $bgImageThumb = imagecreatefromjpeg(BASE_TEMPLATE . '/' . $view->imagen);
            $arrayImage = getimagesize(BASE_TEMPLATE . '/' . $view->imagen);

            imagecopyresampled($imageHandler, $bgImageThumb, $x, $y, 0, 0, 500, 300, $arrayImage[0],$arrayImage[1]);
            imagefttext($imageHandler, 43, 0, $x, $ytext, $colors['lightBlue'], $InterstateBlack, $view->fecha_curso );

            $titulo = makeTextBlock(html_entity_decode($view->titulo), $InterstateBold, 32, 500);
            $ytext = $ytext + 43;

            foreach ($titulo as $index2 => $valueDet) {
                imagefttext($imageHandler, 32, 0, $x, $ytext, $colors['gray'], $InterstateBold, $valueDet );    
                $ytext = $ytext + 42;
            }

            $descripcion = makeTextBlock(html_entity_decode($view->descripcion), $InterstateLight, 24, 500);
            $ytext = $ytext + 10;

            foreach ($descripcion as $index2 => $valueDet) {
                imagefttext($imageHandler, 24, 0, $x, $ytext, $colors['orange'], $InterstateLight, $valueDet );    
                $ytext = $ytext + 35;
            }

            $x = $x + 584;
            $dates->orden = $dates->orden < 10? '0' . $dates->orden : $dates->orden;

            if(AMBIENTE == 'test'){

                mkdir(BASE_TEMPLATE . DS . 'templates_images_test');
                mkdir(BASE_TEMPLATE . DS . 'templates_images_test' . DS . 'agenda');
                imagejpeg($imageHandler, BASE_TEMPLATE . DS .'templates_images_test' . DS . 'agenda' . DS . $indexDates . '_' . $dateSplitStart . '_' . $dateSplitEnd . '.jpg', 75);

            }else{

                mkdir(BASE_TEMPLATE . DS . 'templates_images');
                mkdir(BASE_TEMPLATE . DS . 'templates_images' . DS . 'agenda');
                imagejpeg($imageHandler, BASE_TEMPLATE . DS .'templates_images' . DS . 'agenda' . DS . $indexDates . '_' . $dateSplitStart . '_' . $dateSplitEnd . '.jpg', 75);

                $fileHandler = fopen(BASE_TEMPLATE . DS . 'templates_images' . DS . 'agenda' . DS .  'up.txt', 'w+' );
                fclose($fileHandler);

            }
        
        }
      
    }
    imagedestroy($imageHandler);
        imagedestroy($bgImageThumb);
}else{
    $fileHandler = fopen(BASE_TEMPLATE . DS . 'templates_images' . DS . 'agenda' . DS . 'up.txt', 'w+' );
    fclose($fileHandler);
}

function sortByOrderDate($a,$b) {
    return strtotime($a->fecha_inicio) - strtotime($b->fecha_inicio);  
} 
function sortByOrder($a, $b) {
    return $a->orden - $b->orden; 
    
} 

function _data_last_month_day($date) { 
    
    $month = date('m', strtotime($date));
    $year = date('Y', strtotime($date));
    $day = date("d", mktime(0,0,0, $month+1, 0, $year));

    return date('Y-m-d', mktime(0,0,0, $month, $day, $year));
}

function _data_first_month_day($date) {
    $month = date('m', strtotime($date));
    $year = date('Y', strtotime($date));
    return date('Y-m-d', mktime(0,0,0, $month, 1, $year));
}

function makeTextBlock($text, $fontfile, $fontsize, $width) {    
    
    $words = explode(' ', $text); 
    $lines = array($words[0]); 
    $currentLine = 0; 
    for($i = 1; $i < count($words); $i++) { 
        $lineSize = imagettfbbox($fontsize, 0, $fontfile, $lines[$currentLine] . ' ' . $words[$i]); 
        if($lineSize[2] - $lineSize[0] < $width) { 
            $lines[$currentLine] .= ' ' . $words[$i]; 
        } else { 
            $currentLine++; 
            $lines[$currentLine] = $words[$i]; 
        } 
    } 
    
    return $lines; 
} 

function centerText($text, $font, $size, $xi) {

    $box = ImageTTFBBox($size, 0, $font, $text);
    $xr = abs(max($box[2], $box[4]));
    $x = intval(($xi - $xr) / 2);

    return array($x);
}

function imagettftextSp($image, $size, $angle, $x, $y, $color, $font, $text, $spacing = 0){        
    if ($spacing == 0)    {
        imagettftext($image, $size, $angle, $x, $y, $color, $font, $text);
    } else {
        $temp_x = $x;
        for ($i = 0; $i < strlen($text); $i++){
            $bbox = imagettftext($image, $size, $angle, $temp_x, $y, $color, $font, $text[$i]);
            $temp_x += $spacing + ($bbox[2] - $bbox[0]);
        }
    }
}

?>