<?php

$patterns[0] = 'á';
$patterns[1] = 'é';
$patterns[2] = 'í';
$patterns[3] = 'ó';
$patterns[4] = 'ú';

$replacements[0] = 'a';
$replacements[1] = 'e';
$replacements[2] = 'i';
$replacements[3] = 'o';
$replacements[4] = 'u';

define('BASE_TEMPLATE', WWW_ROOT .'files' . DS . $clientFolder); 

$jsonFile = file_get_contents(BASE_TEMPLATE . '/templates/json/' . AMBIENTE . '/informacion_medica-data.json');
$jsonData = json_decode($jsonFile);

$slides = false;
$current_time = date('d-m-Y');
$today_ts     = strtotime($current_time);
$a = 1;
foreach ($jsonData as &$slide) {  
   
    
    $date_ini = strtotime($slide->fecha_inicio);
    $date_ter = strtotime($slide->fecha_termino);

    if($date_ini <= $date_ter){
        if(($date_ini >= $today_ts && $date_ter >= $today_ts ) || ($date_ini <= $today_ts && $date_ter >= $today_ts ) ){

           // $slide->titulo = str_replace($patterns, $replacements, $slide->titulo);    
            $slides[$a]= $slide; 
            $a++;
  
        }
    } 
}

usort($slides, 'sortByOrder');

$ambiente = AMBIENTE == 'test' ? 'templates_images_test' : 'templates_images';
$directorio = BASE_TEMPLATE . DS . $ambiente . DS . 'informacion_medica';
$folderImageTest = scandir($directorio);

foreach ($folderImageTest as $filesTest){
    if(!in_array($filesTest,array(".",".."))  ){
        @unlink( $directorio . DS . $filesTest);
    }   
}

$InterstateBlack   = BASE_TEMPLATE . '/templates/fonts/Interstate-Black.ttf';
$InterstateLight   = BASE_TEMPLATE . '/templates/fonts/Interstate Light2.ttf';
$bgImage        = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/bg_informacion_medica.png');
$i = 1;

if(!empty($slides) && isset($slides)){
 
    foreach($slides as $dates) {

        $imageHandler    = @imagecreatetruecolor(1920, 1080);
        $colors['green'] = imagecolorallocate($imageHandler, 0, 130, 122);
        $colors['gray'] = imagecolorallocate($imageHandler, 91, 91, 95);
        $colors['yellow'] = imagecolorallocate($imageHandler, 247, 177, 1);


        $bgImageThumb = imagecreatefromjpeg(BASE_TEMPLATE . '/' . $dates->imagen);
        $arrayImage = getimagesize(BASE_TEMPLATE . '/' . $dates->imagen);

        imagecopyresampled($imageHandler, $bgImageThumb, 1065, 0, 0, 0, 855, 998, $arrayImage[0],$arrayImage[1]);
        imagecopy($imageHandler, $bgImage, 0, 0, 0, 0, 1920, 1080);

        $dateSplitStart =  split( '-', $dates->fecha_inicio );
        $dateSplitStart = $dateSplitStart[2].'-'.$dateSplitStart[1].'-'.$dateSplitStart[0];
        $dateSplitEnd =  split( '-', $dates->fecha_termino );
        $dateSplitEnd = $dateSplitEnd[2].'-'.$dateSplitEnd[1].'-'.$dateSplitEnd[0];

        $titulo = makeTextBlock(mb_strtoupper(html_entity_decode($dates->titulo)), $InterstateBlack, 45, 850) ;
        //$y = 185;
        $y = 160;

        foreach ($titulo as $value) {
            imagettftext($imageHandler, 45, 0, 93, $y, $colors['green'], $InterstateBlack, $value );    
            $y = $y + 59;
        }

        $descripcion = makeTextBlock(html_entity_decode($dates->descripcion), $InterstateLight, 29, 850);
        //$y = $y + 55;
        $y = $y + 25;

        foreach ($descripcion as $index => $valueDet) {
            $newLine = explode("\r\n", $valueDet);
            if(is_array($newLine) && count($newLine) > 1){
                foreach ($newLine as $indexLine => $line){

                    $y = $indexLine == 1? $y+ 10: $y;

                    if($indexLine < 12){
                        imagefttext($imageHandler, 29, 0, 93, $y, $colors['gray'], $InterstateLight, $line );    
                        $y = $y + 35;
                    }
                }
            }else{ 
                if($index < 12){
                    imagefttext($imageHandler, 29, 0, 93, $y, $colors['gray'], $InterstateLight, $valueDet );    
                    $y = $y + 35;
                }
            }
        }
        
        $dates->orden = $dates->orden < 10? '0' . $dates->orden : $dates->orden;
        
        if(AMBIENTE == 'test'){

            mkdir(BASE_TEMPLATE . DS . 'templates_images_test');
            mkdir(BASE_TEMPLATE . DS . 'templates_images_test' . DS . 'informacion_medica');
            imagejpeg($imageHandler, BASE_TEMPLATE . DS .'templates_images_test' . DS . 'informacion_medica' . DS . $dates->orden . '_' . $dateSplitStart . '_' . $dateSplitEnd . '_' . $i . '.jpg', 75);

        }else{

            mkdir(BASE_TEMPLATE . DS . 'templates_images');
            mkdir(BASE_TEMPLATE . DS . 'templates_images' . DS . 'informacion_medica');
            imagejpeg($imageHandler, BASE_TEMPLATE . DS .'templates_images' . DS . 'informacion_medica' . DS . $dates->orden . '_' . $dateSplitStart . '_' . $dateSplitEnd . '_' . $i . '.jpg', 75);

            $fileHandler = fopen(BASE_TEMPLATE . DS . 'templates_images' . DS . 'informacion_medica' . DS .  'up.txt', 'w+' );
            fclose($fileHandler);

        }
        imagedestroy($imageHandler);
        imagedestroy($bgImageThumb);
        $i++;

    }

    if(AMBIENTE == 'prod'){
        // Copia (crea, reemplaza) la carpeta de prod al FTP
        shell_exec(PHP_RUTA . ' ' . CARPETA_SHELL_EXEC . 'copiaEnFTP.php ' . $modulo . ' > /dev/null 2>&1 &');
        //echo PHP_RUTA . ' ' . CARPETA_SHELL_EXEC . 'copiaEnFTP.php ' . $modulo;

        // /usr/local/bin/php /home/dev/public_html/sub_dominios/clinicaalemana/app/Shell_exec/copiaEnFTP.php
    }

}else{
    $fileHandler = fopen(BASE_TEMPLATE . DS . 'templates_images' . DS . 'informacion_medica' . DS . 'up.txt', 'w+' );
    fclose($fileHandler);
}

function sortByOrder($a, $b) {
    return $a->orden - $b->orden; 
    
} 

function makeTextBlock($text, $fontfile, $fontsize, $width) {    
    $words = explode(' ', $text); 
    $lines = array($words[0]); 
    $currentLine = 0; 
    for($i = 1; $i < count($words); $i++){  
        $lineSize = imagettfbbox($fontsize, 0, $fontfile, $lines[$currentLine] . ' ' . $words[$i]); 
        if($lineSize[2] - $lineSize[0] < $width){ 
            $lines[$currentLine] .= ' ' . $words[$i]; 
        } else { 
            $currentLine++; 
            $lines[$currentLine] = $words[$i]; 
        } 
    } 
    
    return $lines; 
} 
  
function centerText($text, $font, $size, $xi) {

    $box = ImageTTFBBox($size, 0, $font, $text);
    $xr = abs(max($box[2], $box[4]));
    $x = intval(($xi - $xr) / 2);

    return array($x);
}

?>