<?php

$patterns[0] = 'á';
$patterns[1] = 'é';
$patterns[2] = 'í';
$patterns[3] = 'ó';
$patterns[4] = 'ú';

$replacements[0] = 'a';
$replacements[1] = 'e';
$replacements[2] = 'i';
$replacements[3] = 'o';
$replacements[4] = 'u';

define('BASE_TEMPLATE', WWW_ROOT .'files' . DS . $clientFolder);

$jsonFile = file_get_contents(BASE_TEMPLATE . '/templates/json/' . AMBIENTE . '/vida_social-data.json');
$jsonData = json_decode($jsonFile);


$slides = false;
$current_time = date('d-m-Y');
$today_ts     = strtotime($current_time);
$a = 1;
foreach ($jsonData as &$slide) {

    $date_ini = strtotime($slide->fecha_inicio);
    $date_ter = strtotime($slide->fecha_termino);

    if($date_ini <= $date_ter){

        //if($date_ini >= $today_ts  && $date_ter >= $today_ts){
        if(($date_ini >= $today_ts && $date_ter >= $today_ts ) || ($date_ini <= $today_ts && $date_ter >= $today_ts ) ){

            $slides[$a]= $slide;
            $a++;

        }
    }
}

usort($slides, 'sortByOrder');

$ambiente = AMBIENTE == 'test' ? 'templates_images_test' : 'templates_images';

$directorio = BASE_TEMPLATE . DS . $ambiente . DS . 'vida_social';
$folderImageTest = scandir($directorio);

foreach ($folderImageTest as $filesTest){
    if(!in_array($filesTest,array(".",".."))  ){
        @unlink( $directorio . DS . $filesTest);
    }
}

$InterstateBlack   = BASE_TEMPLATE . '/templates/fonts/Interstate Black.ttf';
//$InterstateLightItalic   = BASE_TEMPLATE . '/templates/fonts/Interstate Light Italic.ttf';
$InterstateLightItalic   = BASE_TEMPLATE . '/templates/fonts/InterstateLightItalicExtended.otf';
$bgImage        = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/bg_vida_social.png');
$i = 1;


if(!empty($slides) && isset($slides)){

    foreach($slides as $dates) {

        $imageHandler    = @imagecreatetruecolor(1920, 1080);
        $colors['green'] = imagecolorallocate($imageHandler, 44, 146, 147);
        $colors['gray'] = imagecolorallocate($imageHandler, 82, 107, 130);


        $bgImageThumb = imagecreatefromjpeg(BASE_TEMPLATE . '/' . $dates->imagen);
        $arrayImage = getimagesize(BASE_TEMPLATE . '/' . $dates->imagen);

        imagecopyresampled($imageHandler, $bgImageThumb, 758, 69, 0, 0, 1165, 1012, $arrayImage[0],$arrayImage[1]);
        //imagecopyresampled($dst_image, $src_image, $i, $a, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
        imagecopy($imageHandler, $bgImage, 0, 0, 0, 0, 1920, 1080);

        $dateSplitStart =  split( '-', $dates->fecha_inicio );
        $dateSplitStart = $dateSplitStart[2].'-'.$dateSplitStart[1].'-'.$dateSplitStart[0];
        $dateSplitEnd =  split( '-', $dates->fecha_termino );
        $dateSplitEnd = $dateSplitEnd[2].'-'.$dateSplitEnd[1].'-'.$dateSplitEnd[0];


        $titulo = makeTextBlock(html_entity_decode($dates->titulo), $InterstateBlack, 47, 700) ;
        $y = 460;

        foreach ($titulo as $value) {

            imagefttext($imageHandler, 47, 0, 50, $y, $colors['green'], $InterstateBlack, $value );
            $y = $y + 65;
        }

        $descripcion = makeTextBlock(html_entity_decode($dates->descripcion), $InterstateLightItalic, 28, 690);
        $y = $y + 20;

        foreach ($descripcion as $index => $valueDet) {
            $newLine = explode("\r\n", $valueDet);
            if(is_array($newLine) && count($newLine) > 1){
                foreach ($newLine as $indexLine => $line){

                    $y = $indexLine == 1? $y+ 5: $y;
                    if($indexLine < 10){
                        imagefttext($imageHandler, 27, 0, 50, $y, $colors['gray'], $InterstateLightItalic, $line );
                        $y = $y + 35;
                    }
                }
            }else{
                if($index < 10){
                    imagefttext($imageHandler, 27, 0, 50, $y, $colors['gray'], $InterstateLightItalic, $valueDet );
                    $y = $y + 35;
                }
            }
        }
// Correccion del salto de linea
/*
        foreach ($descripcion as $index => $valueDet) {

            imagefttext($imageHandler, 27, 0, 50, $y, $colors['gray'], $InterstateLightItalic, $valueDet );
            $y = $y + 35;
        }
*/
        $dates->orden = $dates->orden < 10? '0' . $dates->orden : $dates->orden;

        if(AMBIENTE == 'test'){

            mkdir(BASE_TEMPLATE . DS . 'templates_images_test');
            mkdir(BASE_TEMPLATE . DS . 'templates_images_test' . DS . 'vida_social');
            imagejpeg($imageHandler, BASE_TEMPLATE . DS .'templates_images_test' . DS . 'vida_social' . DS . $dates->orden . '_' . $dateSplitStart . '_' . $dateSplitEnd . '_' . $i . '.jpg', 75);

        }else{

            mkdir(BASE_TEMPLATE . DS . 'templates_images');
            mkdir(BASE_TEMPLATE . DS . 'templates_images' . DS . 'vida_social');
            imagejpeg($imageHandler, BASE_TEMPLATE . DS .'templates_images' . DS . 'vida_social' . DS . $dates->orden . '_' . $dateSplitStart . '_' . $dateSplitEnd . '_' . $i . '.jpg', 75);

            $fileHandler = fopen(BASE_TEMPLATE . DS . 'templates_images' . DS . 'vida_social' . DS .  'up.txt', 'w+' );
            fclose($fileHandler);

        }
        imagedestroy($imageHandler);
        imagedestroy($bgImageThumb);
        $i++;

    }
 }else{
    $fileHandler = fopen(BASE_TEMPLATE . DS . 'templates_images' . DS . 'vida_social' . DS . 'up.txt', 'w+' );
    fclose($fileHandler);
}

function sortByOrder($a, $b) {
    return $a->orden - $b->orden;

}

function makeTextBlock($text, $fontfile, $fontsize, $width) {
    $words = explode(' ', $text);
    $lines = array($words[0]);
    $currentLine = 0;
    for($i = 1; $i < count($words); $i++) {
        $lineSize = imagettfbbox($fontsize, 0, $fontfile, $lines[$currentLine] . ' ' . $words[$i]);
        if($lineSize[2] - $lineSize[0] < $width) {
            $lines[$currentLine] .= ' ' . $words[$i];
        } else {
            $currentLine++;
            $lines[$currentLine] = $words[$i];
        }
    }
    return $lines;
}

function centerText($text, $font, $size, $xi) {

    $box = ImageTTFBBox($size, 0, $font, $text);
    $xr = abs(max($box[2], $box[4]));
    $x = intval(($xi - $xr) / 2);

    return array($x);
}


function imagettftextjustified(&$image, $size, $angle, $left, $top, $color, $font, $text, $max_width, $minspacing=9,$linespacing=1.1){
    $wordwidth = array();
    $linewidth = array();
    $linewordcount = array();
    $largest_line_height = 0;
    $lineno=0;
    $words=explode(" ",$text);
    $wln=0;
    $linewidth[$lineno]=0;
    $linewordcount[$lineno]=0;
    foreach ($words as $word){
        $dimensions = imagettfbbox($size, $angle, $font, $word);
        $line_width = $dimensions[2] - $dimensions[0];
        $line_height = $dimensions[1] - $dimensions[7];
        if ($line_height>$largest_line_height) $largest_line_height=$line_height;
        if (($linewidth[$lineno]+$line_width+$minspacing)>$max_width){
            $lineno++;
            $linewidth[$lineno]=0;
            $linewordcount[$lineno]=0;
            $wln=0;
        }
        $linewidth[$lineno]+=$line_width+$minspacing;
        $wordwidth[$lineno][$wln]=$line_width;
        $wordtext[$lineno][$wln]=$word;
        $linewordcount[$lineno]++;
        $wln++;
    }

    for ($ln=0;$ln<=$lineno;$ln++){
        $slack=$max_width-$linewidth[$ln];
        if (($linewordcount[$ln]>1)&&($ln!=$lineno)) $spacing=($slack/($linewordcount[$ln]-1));
        else $spacing=$minspacing;
        $x=0;
        for ($w=0;$w<$linewordcount[$ln];$w++){
            imagettftext($image, $size, $angle, $left + intval($x), $top + $largest_line_height + ($largest_line_height * $ln * $linespacing), $color, $font, $wordtext[$ln][$w]);
            $x+=$wordwidth[$ln][$w]+$spacing+$minspacing;
        }
    }
    return true;
}

?>
