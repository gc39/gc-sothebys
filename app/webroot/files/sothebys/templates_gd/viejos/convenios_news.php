<?php

$patterns[0] = 'á';
$patterns[1] = 'é';
$patterns[2] = 'í';
$patterns[3] = 'ó';
$patterns[4] = 'ú';

$replacements[0] = 'a';
$replacements[1] = 'e';
$replacements[2] = 'i';
$replacements[3] = 'o';
$replacements[4] = 'u';

define('BASE_TEMPLATE', WWW_ROOT .'files' . DS . $clientFolder); 

$jsonFile = file_get_contents(BASE_TEMPLATE . '/templates/json/' . AMBIENTE . '/convenios_news-data.json');
$jsonData = json_decode($jsonFile);

$slides = false;
$current_time = date('d-m-Y');
$today_ts     = strtotime($current_time);
$a = 1;
foreach ($jsonData as &$slide) {  

    $date_ini = strtotime($slide->fecha_inicio);
    $date_ter = strtotime($slide->fecha_termino);

    if($date_ini <= $date_ter){
        
       // if($date_ini >= $today_ts  && $date_ter >= $today_ts){
        if(($date_ini >= $today_ts && $date_ter >= $today_ts ) || ($date_ini <= $today_ts && $date_ter >= $today_ts ) ){

            $slides[$a]= $slide; 
            $a++;
  
        }
    } 
}

usort($slides, 'sortByOrder');
$ambiente = AMBIENTE == 'test' ? 'templates_images_test' : 'templates_images';

$directorio = BASE_TEMPLATE . DS . $ambiente . DS . 'convenios_news';
$folderImageTest = scandir($directorio);

foreach ($folderImageTest as $filesTest){
    if(!in_array($filesTest,array(".",".."))  ){
        @unlink( $directorio . DS . $filesTest);
    }   
}

$imageHandler    = @imagecreatetruecolor(1920, 1080);
$InterstateBlack   = BASE_TEMPLATE . '/templates/fonts/Interstate Black.ttf';
$InterstateLight   = BASE_TEMPLATE . '/templates/fonts/Interstate Light.ttf';
$i = 1;

if(!empty($slides) && isset($slides)){

    foreach($slides as $dates) {

        $colors['gray'] = imagecolorallocate($imageHandler, 91, 91, 95);
        $colors['lilac'] = imagecolorallocate($imageHandler, 167, 54, 109);

        $dateSplitStart =  split( '-', $dates->fecha_inicio );
        $dateSplitStart = $dateSplitStart[2].'-'.$dateSplitStart[1].'-'.$dateSplitStart[0];
        $dateSplitEnd =  split( '-', $dates->fecha_termino );
        $dateSplitEnd = $dateSplitEnd[2].'-'.$dateSplitEnd[1].'-'.$dateSplitEnd[0];

        if($dates->estilo == 'image' ){

            $bgImage        = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/bg_convenios_news_con_imagen.png');
            $bgImageThumb = imagecreatefromjpeg(BASE_TEMPLATE . '/' . $dates->imagen);
            $arrayImage = getimagesize(BASE_TEMPLATE . '/' . $dates->imagen);

            imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 1920, 1080, $arrayImage[0],$arrayImage[1]);
            imagecopy($imageHandler, $bgImage, 0, 0, 0, 0, 1920, 1080);

            $titulo = makeTextBlock(html_entity_decode(mb_strtoupper($dates->titulo)), $InterstateBlack, 41, 850) ;
            $y = 350;

            foreach ($titulo as $index => $value) {

                $fontTitle = $index == 0? $InterstateLight : $InterstateBlack;
                list($x) = centerText($value, $fontTitle, 41, 830);
                imagefttext($imageHandler, 41, 0, $x + 70, $y, $colors['lilac'], $fontTitle, $value ); 
                //imagefttext($imageHandler, 41, 0, 95, $y, $colors['lilac'], $fontTitle, mb_strtoupper($value) ); 

                $y = $y + 55;
            }

            $descripcion = makeTextBlock(html_entity_decode($dates->descripcion), $InterstateLight, 25, 800);
            $y = $y + 25;

            foreach ($descripcion as $index => $valueDet) {
                $newLine = explode("\r\n", $valueDet);
                if(is_array($newLine) && count($newLine) > 1){
                    foreach ($newLine as $indexLine => $line){

                        $y = $indexLine == 1? $y+ 10: $y;

                        if($indexLine < 7){
                            imagefttext($imageHandler, 25, 0, 93, $y, $colors['gray'], $InterstateLight, $line );    
                            $y = $y + 37;
                        }
                    }
                }else{ 
                    if($index < 7){
                        imagefttext($imageHandler, 25, 0, 93, $y, $colors['gray'], $InterstateLight, $valueDet );    
                        $y = $y + 37;
                    }
                }
            }

            $piePag = makeTextBlock(html_entity_decode($dates->pie_pag), $InterstateLight, 29, 500);
            $y = $y + 70;

            foreach ($piePag as $index => $valueDet) {
                $newLine = explode("\r\n", $valueDet);
                if(is_array($newLine) && count($newLine) > 1){
                    foreach ($newLine as $indexLine => $line){

                        $y = $indexLine == 1? $y+ 10: $y;

                        if($indexLine < 4){

                            list($x) = centerText($line, $InterstateLight, 29, 500);
                            imagefttext($imageHandler, 29, 0, $x + 220, $y, $colors['gray'], $InterstateLight, $line );     
                            $y = $y + 37;
                        }
                    }
                }else{ 
                    if($index < 4){
                        list($x) = centerText($valueDet, $InterstateLight, 29, 500);
                        imagefttext($imageHandler, 29, 0, $x + 220, $y, $colors['gray'], $InterstateLight, $valueDet );  
                        $y = $y + 37;
                    }
                }
            }

        } else{

            $bgImage2        = imagecreatefromjpeg(BASE_TEMPLATE . '/templates/images/bg_convenios_news_sin_imagen.jpg');
            imagecopy($imageHandler, $bgImage2, 0, 0, 0, 0, 1920, 1080);

            $titulo = makeTextBlock(html_entity_decode(mb_strtoupper($dates->titulo)), $InterstateBlack, 55, 1100) ;
            $y = 350;

            foreach ($titulo as $index => $value) {


                $fontTitle = $index == 0? $InterstateLight : $InterstateBlack;
                list($x) = centerText($value, $fontTitle, 55, 1100);
                imagefttext($imageHandler, 55, 0, $x + 410, $y, $colors['lilac'], $fontTitle, $value ); 
                //imagefttext($imageHandler, 55, 0, 420, $y, $colors['lilac'], $fontTitle, mb_strtoupper($value) ); 
                $y = $y + 67;
            }

            $descripcion = makeTextBlock(html_entity_decode($dates->descripcion), $InterstateLight, 28, 1690);
            $y = $y + 25;

            foreach ($descripcion as $index => $valueDet) {
                $newLine = explode("\r\n", $valueDet);
                if(is_array($newLine) && count($newLine) > 1){
                    foreach ($newLine as $indexLine => $line){

                        $y = $indexLine == 1? $y+ 10: $y;

                        if($indexLine < 9){
                            list($x) = centerText($line, $InterstateLight, 28, 1690);
                            imagefttext($imageHandler, 29, 0, $x + 90, $y, $colors['gray'], $InterstateLight, $line );
                            //imagefttext($imageHandler, 29, 0, 90, $y, $colors['gray'], $InterstateLight, $line );    
                            $y = $y + 39;
                        }
                    }
                }else{ 
                    if($index < 9){
                        list($x) = centerText($valueDet, $InterstateLight, 28, 1690);
                        imagefttext($imageHandler, 29, 0, $x + 90, $y, $colors['gray'], $InterstateLight, $valueDet );
                        $y = $y + 39;
                    }
                }
            }

            $piePag = makeTextBlock(html_entity_decode($dates->pie_pag), $InterstateLight, 29, 1000);
            $y = $y + 70;

            foreach ($piePag as $index => $valueDet) {
                $newLine = explode("\r\n", $valueDet);
                if(is_array($newLine) && count($newLine) > 1){
                    foreach ($newLine as $indexLine => $line){

                        $y = $indexLine == 1? $y+ 10: $y;

                        if($indexLine < 4){
                            list($x) = centerText($line, $InterstateLight, 29, 1000);
                            imagefttext($imageHandler, 29, 0, $x + 450, $y, $colors['gray'], $InterstateLight, $line );  
                            $y = $y + 37;
                        }
                    }
                }else{ 
                    if($index < 4){
                        list($x) = centerText($valueDet, $InterstateLight, 29, 1000);
                        imagefttext($imageHandler, 29, 0, $x + 450, $y, $colors['gray'], $InterstateLight, $valueDet );  
                        $y = $y + 37;
                    }
                }
            }
        } 
        
        $dates->orden = $dates->orden < 10? '0' . $dates->orden : $dates->orden;
        
        if(AMBIENTE == 'test'){

            mkdir(BASE_TEMPLATE . DS . 'templates_images_test');
            mkdir(BASE_TEMPLATE . DS . 'templates_images_test' . DS . 'convenios_news');
            imagejpeg($imageHandler, BASE_TEMPLATE . DS .'templates_images_test' . DS . 'convenios_news' . DS . $dates->orden . '_' . $dateSplitStart . '_' . $dateSplitEnd . '_' . $i . '.jpg', 75);

        }else{

            mkdir(BASE_TEMPLATE . DS . 'templates_images');
            mkdir(BASE_TEMPLATE . DS . 'templates_images' . DS . 'convenios_news');
            imagejpeg($imageHandler, BASE_TEMPLATE . DS .'templates_images' . DS . 'convenios_news' . DS . $dates->orden . '_' . $dateSplitStart . '_' . $dateSplitEnd . '_' . $i . '.jpg', 75);

            $fileHandler = fopen(BASE_TEMPLATE . DS . 'templates_images' . DS . 'convenios_news' . DS .  'up.txt', 'w+' );
            fclose($fileHandler);

        }
      //  imagedestroy($imageHandler);
       // imagedestroy($bgImageThumb);
        $i++; 

    }

}else{
    $fileHandler = fopen(BASE_TEMPLATE . DS . 'templates_images' . DS . 'convenios_news' . DS . 'up.txt', 'w+' );
    fclose($fileHandler);
}

function sortByOrder($a, $b) {
    return $a->orden - $b->orden; 
    
} 

function makeTextBlock($text, $fontfile, $fontsize, $width) 
{    
    $words = explode(' ', $text); 
    $lines = array($words[0]); 
    $currentLine = 0; 
    for($i = 1; $i < count($words); $i++){ 
        $lineSize = imagettfbbox($fontsize, 0, $fontfile, $lines[$currentLine] . ' ' . $words[$i]); 
        if($lineSize[2] - $lineSize[0] < $width) { 
            $lines[$currentLine] .= ' ' . $words[$i]; 
        } 
        else { 
            $currentLine++; 
            $lines[$currentLine] = $words[$i]; 
        } 
    } 
    
    return $lines; 
} 

    
function centerText($text, $font, $size, $xi) {
    
    $box = ImageTTFBBox($size, 0, $font, $text);
    $xr = abs(max($box[2], $box[4]));
    $x = intval(($xi - $xr) / 2);

    return array($x);
}

?>