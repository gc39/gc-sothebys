<?php

$patterns[0] = 'á';
$patterns[1] = 'é';
$patterns[2] = 'í';
$patterns[3] = 'ó';
$patterns[4] = 'ú';

$replacements[0] = 'a';
$replacements[1] = 'e';
$replacements[2] = 'i';
$replacements[3] = 'o';
$replacements[4] = 'u';

define('BASE_TEMPLATE', WWW_ROOT .'files' . DS . $clientFolder); 

$jsonFile = file_get_contents(BASE_TEMPLATE . '/templates/json/' . AMBIENTE . '/contacto_cientifico-data.json');
$jsonData = json_decode($jsonFile);


$slides = false;
$current_time = date('d-m-Y');
$today_ts     = strtotime($current_time);
$a = 1;
foreach ($jsonData as &$slide) {  

    $date_ini = strtotime($slide->fecha_inicio);
    $date_ter = strtotime($slide->fecha_termino);

    if($date_ini <= $date_ter){
        
       // if($date_ini >= $today_ts  && $date_ter >= $today_ts){
        if(($date_ini >= $today_ts && $date_ter >= $today_ts ) || ($date_ini <= $today_ts && $date_ter >= $today_ts ) ){

          //  $slide->nombre = str_replace($patterns, $replacements, $slide->nombre);    
           // $slide->detalle = str_replace($patterns, $replacements, $slide->detalle);      
            $slides[$a]= $slide; 
            $a++;
  
        }
    } 
}

usort($slides, 'sortByOrder');

$ambiente = AMBIENTE == 'test' ? 'templates_images_test' : 'templates_images';

$directorio = BASE_TEMPLATE . DS . $ambiente . DS . 'contacto_cientifico';
$folderImageTest = scandir($directorio);

foreach ($folderImageTest as $filesTest){
    if(!in_array($filesTest,array(".",".."))  ){
        @unlink( $directorio . DS . $filesTest);
    }   
}


$InterstateBlack   = BASE_TEMPLATE . '/templates/fonts/Interstate Black.ttf';
$InterstateLight   = BASE_TEMPLATE . '/templates/fonts/Interstate Light.ttf';
$bgImage        = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/bg_contacto_cientifico.png');
$i = 1;

if(!empty($slides) && isset($slides)){
 
    foreach($slides as $dates) {

        $imageHandler    = @imagecreatetruecolor(1920, 1080);
        $colors['blue'] = imagecolorallocate($imageHandler, 67, 85, 131);
        $colors['gray'] = imagecolorallocate($imageHandler, 91, 91, 95);


        $colors['green'] = imagecolorallocate($imageHandler, 8, 116, 110);

        $colors['gray-desc'] = imagecolorallocate($imageHandler, 91, 91, 95);

        $bgImageThumb = imagecreatefromjpeg(BASE_TEMPLATE . '/' . $dates->imagen);
        $arrayImage = getimagesize(BASE_TEMPLATE . '/' . $dates->imagen);
        $bgLogo =  imagecreatefromjpeg(BASE_TEMPLATE . '/' . $dates->logo);
        $arrayLogo = getimagesize(BASE_TEMPLATE . '/' . $dates->logo);

        imagecopyresampled($imageHandler, $bgImageThumb, 37, 0, 0, 0, 923, 1018, $arrayImage[0],$arrayImage[1]);
        imagecopy($imageHandler, $bgImage, 0, 0, 0, 0, 1920, 1080);
        imagecopyresampled($imageHandler, $bgLogo, 1110, 65, 0, 0, 630, 125, $arrayLogo[0],$arrayLogo[1]);

        $dateSplitStart =  split( '-', $dates->fecha_inicio );
        $dateSplitStart = $dateSplitStart[2].'-'.$dateSplitStart[1].'-'.$dateSplitStart[0];
        $dateSplitEnd =  split( '-', $dates->fecha_termino );
        $dateSplitEnd = $dateSplitEnd[2].'-'.$dateSplitEnd[1].'-'.$dateSplitEnd[0];

        $y = 300;

        $articuloRevision = makeTextBlock(html_entity_decode($dates->articulo_revision), $InterstateLight, 35, 600) ;

        foreach ($articuloRevision as $value) {

            list($x) = centerText($value, $InterstateLight, 35, 600);
            imagefttext($imageHandler, 35, 0, $x + 1085, $y, $colors['blue'], $InterstateLight, $value ); 
            $y = $y + 80;
        }

        $titutlo= makeTextBlock(mb_strtoupper($dates->titulo), $InterstateBlack, 48, 790) ;

        foreach ($titutlo as $value) {

            list($x2) = centerText($value, $InterstateBlack, 48, 790);
            imagefttext($imageHandler, 48, 0, 975 + $x2, $y, $colors['blue'], $InterstateBlack, $value ); 
            $y = $y + 60;
        }

        $doc1 = makeTextBlock(html_entity_decode($dates->doctor_1), $InterstateBlack, 20, 830) ;
        $doc2 = makeTextBlock(html_entity_decode($dates->doctor_2), $InterstateBlack, 20, 830) ;
        $doc3 = makeTextBlock(html_entity_decode($dates->doctor_3), $InterstateBlack, 20, 830) ;
        $doc4 = makeTextBlock(html_entity_decode($dates->doctor_4), $InterstateBlack, 20, 830) ;
        $constEjeXDoctor = 960;
        $fontSizeDoctor = 22;
        $y = $y + 30;

        foreach ($doc1 as $value) {
            list($x) = centerText($value, $InterstateBlack, $fontSizeDoctor, 830);
            imagefttext($imageHandler, $fontSizeDoctor, 0, $x + $constEjeXDoctor, $y, $colors['gray'], $InterstateBlack, $value ); 
            $y = $y + 35;
        }

       if(isset($dates->doctor_2) && !empty($dates->doctor_2)){
            foreach ($doc2 as $value) {
                list($x) = centerText($value, $InterstateBlack, $fontSizeDoctor, 830);
                imagefttext($imageHandler, $fontSizeDoctor, 0, $x + $constEjeXDoctor, $y, $colors['gray'], $InterstateBlack, $value ); 
                $y = $y + 35;
            }
        }

        if(isset($dates->doctor_3) && !empty($dates->doctor_3)){
            foreach ($doc3 as $value) {
                list($x) = centerText($value, $InterstateBlack, $fontSizeDoctor, 830);
                imagefttext($imageHandler, $fontSizeDoctor, 0, $x + $constEjeXDoctor, $y, $colors['gray'], $InterstateBlack, $value ); 
                $y = $y + 35;
            }
        }

        if(isset($dates->doctor_4) && !empty($dates->doctor_4)){
            foreach ($doc4 as $value) {
                list($x) = centerText($value, $InterstateBlack, $fontSizeDoctor, 830);
                imagefttext($imageHandler, $fontSizeDoctor, 0, $x + $constEjeXDoctor, $y, $colors['gray'], $InterstateBlack, $value );   
                $y = $y + 35;
            }
        }

        $y = $y + 35;
        $descripcion = makeTextBlock(html_entity_decode($dates->descripcion), $InterstateLight, 22, 900);

        foreach ($descripcion as $index => $valueDet) {
            $newLine = explode("\r\n", $valueDet);

            if($index < 6){
                if(is_array($newLine) && count($newLine) > 1){
                    foreach ($newLine as $indexLine => $line){

                        $y = $indexLine == 1? $y+ 10: $y;
                        list($x) = centerText($line, $InterstateBlack, 22, 900);
                        imagefttext($imageHandler, 22, 0, $x + 970, $y, $colors['gray'], $InterstateLight, $line );   
                        $y = $y + 33;
                    }
                }else{ 
                    list($x) = centerText($valueDet, $InterstateBlack, 22, 900);
                    imagefttext($imageHandler, 22, 0, $x + 970, $y, $colors['gray'], $InterstateLight, $valueDet );   
                    $y = $y + 33;
                }
            }
        }
        
        $dates->orden = $dates->orden < 10? '0' . $dates->orden : $dates->orden;

        if(AMBIENTE == 'test'){

            mkdir(BASE_TEMPLATE . DS . 'templates_images_test');
            mkdir(BASE_TEMPLATE . DS . 'templates_images_test' . DS . 'contacto_cientifico');
            imagejpeg($imageHandler, BASE_TEMPLATE . DS .'templates_images_test' . DS . 'contacto_cientifico' . DS . $dates->orden . '_' . $dateSplitStart . '_' . $dateSplitEnd . '_' . $i . '.jpg', 75);

        }else{

            mkdir(BASE_TEMPLATE . DS . 'templates_images');
            mkdir(BASE_TEMPLATE . DS . 'templates_images' . DS . 'contacto_cientifico');
            imagejpeg($imageHandler, BASE_TEMPLATE . DS .'templates_images' . DS . 'contacto_cientifico' . DS . $dates->orden . '_' . $dateSplitStart . '_' . $dateSplitEnd . '_' . $i . '.jpg', 75);

            $fileHandler = fopen(BASE_TEMPLATE . DS . 'templates_images' . DS . 'contacto_cientifico' . DS .  'up.txt', 'w+' );
            fclose($fileHandler);

        }
        imagedestroy($imageHandler);
        imagedestroy($bgImageThumb);
        $i++;

    }

}else{
    $fileHandler = fopen(BASE_TEMPLATE . DS . 'templates_images' . DS . 'contacto_cientifico' . DS . 'up.txt', 'w+' );
    fclose($fileHandler);
}


function sortByOrder($a, $b) {
    return $a->orden - $b->orden; 
    
} 


function pc_ImageTTFCenter($text, $font, $size, $xi) {

    // find the size of the image
    //$xi = ImageSX($image);
    

    // find the size of the text
    $box = ImageTTFBBox($size, 0, $font, $text);

    $xr = abs(max($box[2], $box[4]));

    // compute centering
    $x = intval(($xi - $xr) / 2);

    return array($x);
}

function makeTextBlock($text, $fontfile, $fontsize, $width) 
{    
    $words = explode(' ', $text); 
    $lines = array($words[0]); 
    $currentLine = 0; 
    for($i = 1; $i < count($words); $i++) 
    { 
        $lineSize = imagettfbbox($fontsize, 0, $fontfile, $lines[$currentLine] . ' ' . $words[$i]); 
        if($lineSize[2] - $lineSize[0] < $width) 
        { 
            $lines[$currentLine] .= ' ' . $words[$i]; 
        } 
        else 
        { 
            $currentLine++; 
            $lines[$currentLine] = $words[$i]; 
        } 
    } 
    
    return $lines; 
} 

    
function centerText($text, $font, $size, $xi) {

    // find the size of the image
    //$xi = ImageSX($image);
    

    // find the size of the text
    $box = ImageTTFBBox($size, 0, $font, $text);

    $xr = abs(max($box[2], $box[4]));

    // compute centering
    $x = intval(($xi - $xr) / 2);


    return array($x);
}

?>