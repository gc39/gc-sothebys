<?php

$patterns[0] = 'á';
$patterns[1] = 'é';
$patterns[2] = 'í';
$patterns[3] = 'ó';
$patterns[4] = 'ú';

$replacements[0] = 'a';
$replacements[1] = 'e';
$replacements[2] = 'i';
$replacements[3] = 'o';
$replacements[4] = 'u';

define('BASE_TEMPLATE', WWW_ROOT .'files' . DS . $clientFolder); 

$jsonFile = file_get_contents(BASE_TEMPLATE . '/templates/json/' . AMBIENTE . '/cursos-data.json');
$jsonData = json_decode($jsonFile);

$slides = false;
$current_time = date('d-m-Y');
$today_ts     = strtotime($current_time);
$a = 1;
foreach ($jsonData as &$slide) {  

    $date_ini = strtotime($slide->fecha_inicio);
    $date_ter = strtotime($slide->fecha_termino);

    if($date_ini <= $date_ter){
        
        //if($date_ini >= $today_ts  && $date_ter >= $today_ts){
        if(($date_ini >= $today_ts && $date_ter >= $today_ts ) || ($date_ini <= $today_ts && $date_ter >= $today_ts ) ){

           // $slide->titulo = str_replace($patterns, $replacements, $slide->titulo);    
           // $slide->descripcion = str_replace($patterns, $replacements, $slide->descripcion);      
            $slides[$a]= $slide; 
            $a++;
  
        }
    } 
}

usort($slides, 'sortByOrder');

$ambiente = AMBIENTE == 'test' ? 'templates_images_test' : 'templates_images';

$directorio = BASE_TEMPLATE . DS . $ambiente . DS . 'cursos';
$folderImageTest = scandir($directorio);

foreach ($folderImageTest as $filesTest){
    if(!in_array($filesTest,array(".",".."))  ){
        @unlink( $directorio . DS . $filesTest);
    }   
}

$InterstateBlack   = BASE_TEMPLATE . '/templates/fonts/Interstate Black.ttf';
$InterstateLight   = BASE_TEMPLATE . '/templates/fonts/Interstate Light.ttf';
$i = 1;

if(!empty($slides) && isset($slides)){

    foreach($slides as $dates) {
        
        $imageHandler    = @imagecreatetruecolor(1920, 1080);

        $colors['gray'] = imagecolorallocate($imageHandler, 91, 91, 95);
        $colors['lilac'] = imagecolorallocate($imageHandler, 167, 54, 109);
        $colors['white'] = imagecolorallocate($imageHandler, 255, 255, 255);

        $dateSplitStart =  split( '-', $dates->fecha_inicio );
        $dateSplitStart = $dateSplitStart[2].'-'.$dateSplitStart[1].'-'.$dateSplitStart[0];
        $dateSplitEnd =  split( '-', $dates->fecha_termino );
        $dateSplitEnd = $dateSplitEnd[2].'-'.$dateSplitEnd[1].'-'.$dateSplitEnd[0];


        $bgImage        = imagecreatefrompng(BASE_TEMPLATE . '/templates/images/bg_cursos_' . $dates->estilo . '.png');
        $bgImageThumb = imagecreatefromjpeg(BASE_TEMPLATE . '/' . $dates->imagen);
        $arrayImage = getimagesize(BASE_TEMPLATE . '/' . $dates->imagen);

        imagecopyresampled($imageHandler, $bgImageThumb, 0, 0, 0, 0, 850, 1080, $arrayImage[0],$arrayImage[1]);
        imagecopy($imageHandler, $bgImage, 0, 0, 0, 0, 1920, 1080);

        $fechaDia = makeTextBlock(html_entity_decode(mb_strtoupper($dates->fecha_dia)), $InterstateBlack, 50, 400) ;
        $yFecha = 240;


        foreach ($fechaDia as $index => $value) {
            $dimensions = imagettfbbox(50, 0, $InterstateBlack, $value);
            $textWidth = abs($dimensions[4] - $dimensions[0]);
            $x = imagesx($im) - $textWidth;
            imagefttext($imageHandler, 50, 0, $x + 440, $yFecha, $colors['white'], $InterstateBlack, mb_strtoupper($value) ); 
            $yFecha = $yFecha + 70;
        }

        $fechaMes = makeTextBlock(html_entity_decode(mb_strtoupper($dates->fecha_mes)), $InterstateBlack, 30, 400) ;

        foreach ($fechaMes as $index => $value) {
            $dimensions = imagettfbbox(50, 0, $InterstateBlack, $value);
            $textWidth = abs($dimensions[4] - $dimensions[0]);
            $x = imagesx($im) - $textWidth;
            imagefttext($imageHandler, 50, 0, $x + 440, $yFecha, $colors['white'], $InterstateBlack, mb_strtoupper($value) ); 
        }

        $fechaAno = makeTextBlock(html_entity_decode(mb_strtoupper($dates->fecha_ano)), $InterstateBlack, 30, 400) ;

        foreach ($fechaAno as $index => $value) {
            $dimensions = imagettfbbox(30, 0, $InterstateBlack, $value);
            $textWidth = abs($dimensions[4] - $dimensions[0]);
            $x = imagesx($im) - $textWidth;
            imagefttext($imageHandler, 30, 0, $x + 440, 360, $colors['white'], $InterstateBlack, mb_strtoupper($value) ); 
        }

        $titulo = makeTextBlock(html_entity_decode(mb_strtoupper($dates->titulo)), $InterstateBlack, 41, 890) ;
        $y = 150;

        foreach ($titulo as $index => $value) {

            $fontTitle = $index == 0? $InterstateLight : $InterstateBlack;
            imagefttext($imageHandler, 41, 0, 945, $y, $colors['white'], $fontTitle, mb_strtoupper($value) ); 

            $y = $y + 55;
        }

        $descripcion = makeTextBlock(html_entity_decode($dates->descripcion), $InterstateLight, 24, 900);
        $y2 = 395;

        foreach ($descripcion as $index => $valueDet) {
            $newLine = explode("\r\n", $valueDet);
            if(is_array($newLine) && count($newLine) > 1){
                foreach ($newLine as $indexLine => $line){

                    $y2 = $indexLine == 1? $y2+ 10: $y2;

                    if($indexLine <15){
                        imagefttext($imageHandler, 24, 0, 915, $y2, $colors['gray'], $InterstateLight, $line );    
                        $y2 = $y2 + 36;
                    }
                }
            }else{ 
                if($index < 15){
                    imagefttext($imageHandler, 24, 0, 915, $y2, $colors['gray'], $InterstateLight, $valueDet );    
                    $y2 = $y2 + 36;
                }
            }
        }
        
        $dates->orden = $dates->orden < 10? '0' . $dates->orden : $dates->orden;

        if(AMBIENTE == 'test'){

            mkdir(BASE_TEMPLATE . DS . 'templates_images_test');
            mkdir(BASE_TEMPLATE . DS . 'templates_images_test' . DS . 'cursos');
            imagejpeg($imageHandler, BASE_TEMPLATE . DS .'templates_images_test' . DS . 'cursos' . DS . $dates->orden . '_' . $dateSplitStart . '_' . $dateSplitEnd . '_' . $i . '.jpg', 75);

        }else{

            mkdir(BASE_TEMPLATE . DS . 'templates_images');
            mkdir(BASE_TEMPLATE . DS . 'templates_images' . DS . 'cursos');
            imagejpeg($imageHandler, BASE_TEMPLATE . DS .'templates_images' . DS . 'cursos' . DS . $dates->orden . '_' . $dateSplitStart . '_' . $dateSplitEnd . '_' . $i . '.jpg', 75);

            $fileHandler = fopen(BASE_TEMPLATE . DS . 'templates_images' . DS . 'cursos' . DS .  'up.txt', 'w+' );
            fclose($fileHandler);

        }
        imagedestroy($imageHandler);
        imagedestroy($bgImageThumb);
        imagedestroy($bgImage);
        $i++;

    }

}else{
    $fileHandler = fopen(BASE_TEMPLATE . DS . 'templates_images' . DS . 'cursos' . DS . 'up.txt', 'w+' );
    fclose($fileHandler);
}

function sortByOrder($a, $b) {
    return $a->orden - $b->orden; 
    
} 

function makeTextBlock($text, $fontfile, $fontsize, $width) 
{    
    $words = explode(' ', $text); 
    $lines = array($words[0]); 
    $currentLine = 0; 
    for($i = 1; $i < count($words); $i++) 
    { 
        $lineSize = imagettfbbox($fontsize, 0, $fontfile, $lines[$currentLine] . ' ' . $words[$i]); 
        if($lineSize[2] - $lineSize[0] < $width) 
        { 
            $lines[$currentLine] .= ' ' . $words[$i]; 
        } 
        else 
        { 
            $currentLine++; 
            $lines[$currentLine] = $words[$i]; 
        } 
    } 
    
    return $lines; 
} 

    
function centerText($text, $font, $size, $xi) {
    
    $box = ImageTTFBBox($size, 0, $font, $text);
    $xr = abs(max($box[2], $box[4]));
    $x = intval(($xi - $xr) / 2);

    return array($x);
}

?>