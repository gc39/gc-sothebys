<?php

$patterns[0] = 'á';
$patterns[1] = 'é';
$patterns[2] = 'í';
$patterns[3] = 'ó';
$patterns[4] = 'ú';

$replacements[0] = 'a';
$replacements[1] = 'e';
$replacements[2] = 'i';
$replacements[3] = 'o';
$replacements[4] = 'u';

define('BASE_TEMPLATE', WWW_ROOT .'files' . DS . $clientFolder); 

$jsonFile = file_get_contents(BASE_TEMPLATE . '/templates/json/' . AMBIENTE . '/reuniones_semanales-data.json');
$jsonData = json_decode($jsonFile);

$slides = false;
$slidesOrde = false;
$datesSlideIni = false;
$datesSlideTer = false;
$current_time = date('d-m-Y');
$today_ts     = strtotime($current_time);
$days = array('DOMINGO', 'LUNES', 'MARTES', 'MIÉRCOLES', 'JUEVES', 'VIERNES', 'SÁBADO');
$monthsArray = array('', 'ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE');
$a = 1;
$aa = 0;

foreach ($jsonData as &$slide) { 
    $slides[] = $slide;
}

//usort($slides, 'sortByOrderDate');
usort($slides, 'sortByOrder');

foreach ($slides as &$slide) { 
    
    $dateIni = strtotime($slide->fecha_inicio);
    $dateTer = strtotime($slide->fecha_termino);
    
    if($today_ts >= $dateIni && $today_ts <= $dateTer){
        
        if($aa >= 3){
           $a++;
           $aa = 1;
           $slide->slide = $a;
        }else{
           $a = $a;
           $aa++;
            $slide->slide = $a;
        }
        
        if(isset($datesSlideIni[$a])){
            if(strtotime($slide->fecha_inicio) < strtotime($datesSlideIni[$a])){
                $datesSlideIni[$a] = $slide->fecha_inicio;
            }else{
                $datesSlideIni[$a] = $datesSlideIni[$a];
            }
            
        }else{
            $datesSlideIni[$a] = $slide->fecha_inicio;
        }
        
        if(isset($datesSlideTer[$a])){
            if(strtotime($slide->fecha_termino) > strtotime($datesSlideTer[$a])){
                $datesSlideTer[$a] = $slide->fecha_termino;
            }else{
                $datesSlideTer[$a] = $datesSlideTer[$a];
            }
            
        }else{
            $datesSlideTer[$a] = $slide->fecha_termino;
        }

        $slidesOrde[$a][$aa] = $slide;
    }
    
}

$ambiente = AMBIENTE == 'test' ? 'templates_images_test' : 'templates_images';

$directorio = BASE_TEMPLATE . DS . $ambiente . DS . 'reuniones_semanales';
$folderImageTest = scandir($directorio);

foreach ($folderImageTest as $filesTest){
    if(!in_array($filesTest,array(".",".."))  ){
        @unlink( $directorio . DS . $filesTest);
    }   
}

$InterstateBlack   = BASE_TEMPLATE . '/templates/fonts/Interstate Black.ttf';
$InterstateLight   = BASE_TEMPLATE . '/templates/fonts/Interstate Light.ttf';
$bgImage        = imagecreatefromjpeg(BASE_TEMPLATE . '/templates/images/bg_reuniones_semanales.jpg');
$i = 0;

if(!empty($slidesOrde) && isset($slidesOrde)){

    foreach($slidesOrde as $indexDates => $dates) {

        $imageHandler    = @imagecreatetruecolor(1920, 1080);
        $colors['white'] = imagecolorallocate($imageHandler, 255, 255, 255);
        $colors['gray'] = imagecolorallocate($imageHandler, 91, 91, 95);  
        $colors['yellow'] = imagecolorallocate($imageHandler, 247, 182, 62);

        imagecopy($imageHandler, $bgImage, 0, 0, 0, 0, 1920, 1080);

        $dateSplitStart =  split( '-', $datesSlideIni[$indexDates] );
        $dateSplitStart = $dateSplitStart[2].'-'.$dateSplitStart[1].'-'.$dateSplitStart[0];
        $dateSplitEnd =  split( '-', $datesSlideTer[$indexDates] );
        $dateSplitEnd = $dateSplitEnd[2].'-'.$dateSplitEnd[1].'-'.$dateSplitEnd[0];

        $y = (count($dates) == 3)? 350 : 470;
        $y = (count($dates) == 1)? 570 : $y;

        $yDate = (count($dates) == 3)? 235 : 355;
        $yDate = (count($dates) == 1)? 455 : $yDate;

        imagefttext($imageHandler, 35, 0, 255, 180, $colors['gray'], $InterstateLight, $monthsArray[date('n', strtotime($current_time))].' '.date('Y', strtotime($current_time)) );

        foreach($dates as $slide) {

            imagefilledrectangle($imageHandler, 0, $yDate, 1920, $yDate + 69, $colors['yellow']);
            imagefttext($imageHandler, 38, 0, 255, $yDate + 51, $colors['white'], $InterstateBlack, $slide->fecha );

            imagefttext($imageHandler, 28, 0, 245, $y, $colors['gray'], $InterstateBlack, $slide->titulo ); 

            $descripcion = makeTextBlock(html_entity_decode($slide->descripcion), $InterstateLight, 23, 1400);
            $y2 = $y + 45;

            foreach ($descripcion as $index2 => $valueDet) {
                $newLine = explode("\r\n", $valueDet);

                if($index2 < 3){
                    if(is_array($newLine) && count($newLine) > 1){

                        foreach ($newLine as $indexLine => $line){

                            // $y2 = $indexLine == 1? $y2+ 10: $y2;
                            if($indexLine < 3){
                                imagefttext($imageHandler, 24, 0, 245, $y2, $colors['gray'], $InterstateLight, $line );    
                                $y2 = $y2 + 35;
                            }
                        }

                    }else{   

                        imagefttext($imageHandler, 24, 0, 245, $y2, $colors['gray'], $InterstateLight, $valueDet );    
                        $y2 = $y2 + 35;
                    }
                }

            }

            $yDate = $yDate +260;
            $y = $y +260;

            if(AMBIENTE == 'test'){

                mkdir(BASE_TEMPLATE . DS . 'templates_images_test');
                mkdir(BASE_TEMPLATE . DS . 'templates_images_test' . DS . 'reuniones_semanales');
                imagejpeg($imageHandler, BASE_TEMPLATE . DS .'templates_images_test' . DS . 'reuniones_semanales' . DS . $indexDates . '_' . $dateSplitStart . '_' . $dateSplitEnd . '.jpg', 75);

            }else{

                mkdir(BASE_TEMPLATE . DS . 'templates_images');
                mkdir(BASE_TEMPLATE . DS . 'templates_images' . DS . 'reuniones_semanales');
                imagejpeg($imageHandler, BASE_TEMPLATE . DS .'templates_images' . DS . 'reuniones_semanales' . DS . $indexDates . '_' . $dateSplitStart . '_' . $dateSplitEnd . '.jpg', 75);

                $fileHandler = fopen(BASE_TEMPLATE . DS . 'templates_images' . DS . 'reuniones_semanales' . DS .  'up.txt', 'w+' );
                fclose($fileHandler);
            }
        }
        imagedestroy($imageHandler);
        imagedestroy($bgImageThumb);        
    }
    
}else{
    $fileHandler = fopen(BASE_TEMPLATE . DS . 'templates_images' . DS . 'reuniones_semanales' . DS . 'up.txt', 'w+' );
    fclose($fileHandler);
}

function sortByOrderDate($a,$b) {
    return strtotime($a->fecha_inicio) - strtotime($b->fecha_inicio);  
} 

function _data_last_month_day($date) { 
    
    $month = date('m', strtotime($date));
    $year = date('Y', strtotime($date));
    $day = date("d", mktime(0,0,0, $month+1, 0, $year));

    return date('Y-m-d', mktime(0,0,0, $month, $day, $year));
}

function _data_first_month_day($date) {
    $month = date('m', strtotime($date));
    $year = date('Y', strtotime($date));
    return date('Y-m-d', mktime(0,0,0, $month, 1, $year));
}


function sortByOrder($a, $b) {
    return $a->orden - $b->orden; 
    
} 

function makeTextBlock($text, $fontfile, $fontsize, $width) 
{    
    $words = explode(' ', $text); 
    $lines = array($words[0]); 
    $currentLine = 0; 
    for($i = 1; $i < count($words); $i++) 
    { 
        $lineSize = imagettfbbox($fontsize, 0, $fontfile, $lines[$currentLine] . ' ' . $words[$i]); 
        if($lineSize[2] - $lineSize[0] < $width) 
        { 
            $lines[$currentLine] .= ' ' . $words[$i]; 
        } 
        else 
        { 
            $currentLine++; 
            $lines[$currentLine] = $words[$i]; 
        } 
    } 
    
    return $lines; 
} 

    
function centerText($text, $font, $size, $xi) {

    $box = ImageTTFBBox($size, 0, $font, $text);
    $xr = abs(max($box[2], $box[4]));
    $x = intval(($xi - $xr) / 2);

    return array($x);
}

function imagettftextSp($image, $size, $angle, $x, $y, $color, $font, $text, $spacing = 0){        
    if ($spacing == 0)    {
        imagettftext($image, $size, $angle, $x, $y, $color, $font, $text);
    } else {
        $temp_x = $x;
        for ($i = 0; $i < strlen($text); $i++){
            $bbox = imagettftext($image, $size, $angle, $temp_x, $y, $color, $font, $text[$i]);
            $temp_x += $spacing + ($bbox[2] - $bbox[0]);
        }
    }
}

?>