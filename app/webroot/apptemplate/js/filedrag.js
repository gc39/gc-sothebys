/*
filedrag.js - HTML5 File Drag & Drop demonstration
Featured on SitePoint.com
Developed by Craig Buckler (@craigbuckler) of OptimalWorks.net
*/

	// getElementById
	function $id(id) {
		return document.getElementById(id);
	}


	// output information
	function Output(msg) {
		//var m = $id("messages");
		//m.innerHTML = msg + m.innerHTML;
	}

	// file selection
	function FileSelectHandler(e) {
            // fetch FileList object
            var files = e.target.files || e.dataTransfer.files;
		
            // process all File objects
            for (var i = 0, f; f = files[i]; i++) {
                randomNumber = new Date().getTime();
                formatImage= f.name.substring(f.name.lastIndexOf('.') + 1 );
                f.nameformat= randomNumber + '.' + formatImage;
                //ParseFile(f);
                UploadFile(f);
            }
	}

	// output file information
	function ParseFile(file) {

            Output(
                "<p>File information: <strong>" + file.name +
                "</strong> type: <strong>" + file.type +
                "</strong> size: <strong>" + file.size +
                "</strong> bytes</p>"
            );

            // display an image
            if (file.type.indexOf("image") == 0) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    Output(
                        $( '.selectedItem' ).attr('src', 'uploads/'+file.nameformat ),
                        $( '#fileImage2' ).val(file.nameformat)
                    );
                }
                reader.readAsDataURL(file);
            }

            // display text
            /*if (file.type.indexOf("text") == 0) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    Output(
                        "<p><strong>" + file.name + ":</strong></p><pre>" +
                        e.target.result.replace(/</g, "&lt;").replace(/>/g, "&gt;") +
                        "</pre>"
                    );
                }
                reader.readAsText(file);
            }*/
	}

	// upload JPEG files
	function UploadFile(file) {
            var xhr = new XMLHttpRequest();
            if ( xhr.upload ) {

                // create progress bar
                var o = $id("progress");

                // file received/failed
                xhr.onreadystatechange = function(e) {
                    if (xhr.readyState == 4) {
                        //progress.className = (xhr.status == 200 ? "success" : "failure");
                        $( '.selectedItem' ).attr('src', 'uploads/'+file.nameformat )
                    }
                };
                // start upload
                xhr.open("POST", $id("formImage").action + '?fn=' + file.nameformat, true);
                xhr.send(file);
            }
	}

	// initialize
	function Init() {

            var fileselect = $id("fileImage");
            //submitbutton = $id("submitbutton");

            // file select
            fileselect.addEventListener("change", FileSelectHandler, false);

            // is XHR2 available?
            var xhr = new XMLHttpRequest();
            if (xhr.upload) {
                // remove submit button
                //submitbutton.style.display = "none";
            }

	}
        
	// call initialization file
	if (window.File && window.FileList && window.FileReader) {
		//Init();
	}
