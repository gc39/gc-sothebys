<?php
/*
Server-side PHP file upload code for HTML5 File Drag & Drop demonstration
Featured on SitePoint.com
Developed by Craig Buckler (@craigbuckler) of OptimalWorks.net
*/
$fn = $_GET['fn'];

file_put_contents('uploads/' . $fn, file_get_contents('php://input'));
?>