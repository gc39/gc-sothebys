<?php

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');  
header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
 
setlocale(LC_ALL, 'es_ES.UTF-8');
//date_default_timezone_set('America/Argentina/Buenos_Aires');
date_default_timezone_set('America/Santiago');

$format_date = isset($_GET['format_date']) ? $_GET['format_date'] : 'normal';

//$hh = (date('H',  strtotime('-1 hours'))). date(':i');
$hh = (date('H')). date(':i');

if($format_date == 'full') {
    $date = strftime('%A %e').' de '.strftime('%B');
  
} elseif($format_date == 'medium') { 
    $date = strftime('%A %e').' de '.strftime('%B');
 } elseif($format_date == 'normal') { 
       $date = date('d/m/Y'); 
        
} elseif($format_date == 'mini') { 
    $date = date('d/m');
} 
 
echo  json_encode(array('date' => $date, 'hh' => $hh));

?>