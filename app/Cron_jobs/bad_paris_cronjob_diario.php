<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


error_reporting(E_ERROR | E_PARSE);

$fecha = new DateTime("now", new DateTimeZone('America/Santiago'));
echo  $fecha->format("Y-m-d H:m:s").' Inicio';

include_once("/home/digitalboard/public_html/digitalboard-ms-fisico/app/Config/config.php");

//$destinosSendMAil= "aga@grupoclan.cl,af@grupoclan.cl,dop@grupoclan.cl,oll@grupoclan.cl,lc@grupoclan.cl";
$destinosSendMAil= "";
$tituloSendMail='PARIS - Error CronJobs';

// //Envia email de alerta Error
// $mensajeSendMail = 'No se asuste es solo una prueba, favor responder ok.';
// file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
// die();

$server_db = LOCALSERVER;
$user_db = LOCALUSERDB;
$password_db = LOCALPASSDB;
$db_db = LOCALCRUZVERDE;

$obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
if (!$obj_conexion) {
    echo "Error de Base de Datos";
    //Registro Log
    $tipo = "Error";
    $comentario = "Diario: de Base de Datos";

    //Envia email de alerta Error
    $mensajeSendMail = $comentario;
    file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));

    die();
}

$select_qry = "SELECT `config_modulos`.`id` AS `id`,`config_modulos`.`origen` AS `origen`,`config_modulos`.`destino` AS `destino`,`config_modulos`.`logo_default` AS `logo_default`,`config_ftp`.`Ip` AS `Ip`,`config_ftp`.`Port` AS `PORT`,`config_ftp`.`Usuario` AS `Usuario`,`config_ftp`.`Clave` AS `Clave`,`config_modulos`.`tienda` AS `tienda` from (`config_modulos` join `config_ftp` on((`config_modulos`.`id_config_ftp` = `config_ftp`.`id`))) where (`config_modulos`.`activado` = 1)";

if (!$resultado = $obj_conexion->query($select_qry)) {
    echo "Error en query-1";

    //Registro Log
    $tipo = "Error";
    $comentario = "Diario: Error en la query";
    graba_log($id_cronjob_modulos, $tipo, $comentario);

    //Envia email de alerta Error
    $mensajeSendMail = $comentario;
    file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));

    die();
}
$tipo = "Tiempo";
$comentario = "Diario: SFTP INICIO";
graba_log(0, $tipo, $comentario);

//$dictionary_logo = array();
//$ftp_destino = '';

if ($resultado->num_rows != 0) {
    $LastUsuario = '';
    $LastOrigen  = '';

    $borrado_inicial = false;

    while ($rows = $resultado->fetch_assoc()) {
        // $Ip = $rows['Ip'];
        // $Port = $rows['Port'];
        // $Usuario = $rows['Usuario'];
        // $Clave = $rows['Clave'];
        $Ip = $rows['Ip'];
        $Port = $rows['Port'];
        $Usuario = $rows['Usuario'];
        $Clave = $rows['Clave'];
        $LogoCliente = RUTAORIGENLOGOS . $rows['logo_default'];

        //$dictionary_logo[$rows['carpeta']] = $LogoCliente;

        $xmlmodulo = $rows['tienda'];

        $ruta_origen = RUTAORIGENCARPETASCRONJOB . $rows['origen'];
        $ruta_destino = $rows['destino'];

        //$ftp_destino = $ruta_destino;

        $id_cronjob_modulos = $rows['id'];

        //si la ip de connexion es la misma no vuelve a conectar para el mismo usuario
        if ($LastUsuario !== $Usuario) {

            if ($LastUsuario != '') {
                ssh2_disconnect($sftp);
                $tipo = "Info";
                $comentario = "Diario: SFTP Desconectado al ftp [" . $LastUsuario . "] ";
                graba_log($Lastid_cronjob_modulos, $tipo, $comentario);
            }

            //Conecta al FTP
            $sftp_conn = ssh2_connect($Ip, $Port);
            if (!$sftp_conn) 
            {
                //Registro Log
                $tipo = "Error";
                $comentario = "Diario: Error al usar ssh2_connect";
                graba_log($id_cronjob_modulos, $tipo, $comentario);

                //Envia email de alerta Error
                $mensajeSendMail = $comentario;
                file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));

                die('Conexión fallida error ssh2_connect');
            }


            $login_res = ssh2_auth_password($sftp_conn, $Usuario, $Clave);
            $sftp = ssh2_sftp($sftp_conn);


            if ($login_res == 1) {
                //Registro Log
                $tipo = "Info";
                $comentario = "Diario: SFTP Conectado al ftp [" . $Usuario . "] ";
                graba_log($id_cronjob_modulos, $tipo, $comentario);
            } else {
                //Registro Log
                $tipo = "Error";
                $comentario = "Diario: SFTP Error al conectar al ftp [" . $Usuario . "]";
                graba_log($id_cronjob_modulos, $tipo, $comentario);

                //Envia email de alerta Error
                $mensajeSendMail = $comentario;
                file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
            }

            if (!$borrado_inicial) {
                //Funcion para borrar datos que deben eliminarse en el destino
                //ftp_delete_folder($ftp_conn, $ruta_destino);
                $dir = "ssh2.sftp://" . (int)$sftp . $ruta_destino;
                recursiveDelete($sftp, $dir, $id_cronjob_modulos);
                $borrado_inicial = true;
            }
        }



        if ($login_res) {

            $destinos = explode('/', $ruta_destino);

            // ftp_chdir($ftp_conn, '/');

            //crea la primera carpeta
            if (mkdir("ssh2.sftp://" . (int)$sftp . $ruta_destino)) {
                $tipo = "Alerta";
                $comentario = "Diario: SFTP Crea carpeta  $ruta_destino";
                graba_log($id_cronjob_modulos, $tipo, $comentario);
            }

            //crea carpeta
            if (mkdir("ssh2.sftp://" . (int)$sftp . $ruta_destino . $carpeta)) {
                $tipo = "Alerta";
                $comentario = "Diario: SFTP Crea carpeta  $ruta_destino/$carpeta";
                graba_log($id_cronjob_modulos, $tipo, $comentario);
            }

            //Funcion para copiar carpetas o poner logo si no existe contenido
            sftp_copy($sftp, $ruta_origen . $carpeta, $ruta_destino . $carpeta, $LogoCliente, $id_cronjob_modulos, $xmlmodulo);


            // copy($path_files, "ssh2.sftp://". (int)$sftp.$dest);
        }

        $LastUsuario = $Usuario;
        $LastOrigen = $ruta_origen;
        $Lastid_cronjob_modulos = $id_cronjob_modulos;
    }

    // Verifica el ftp destino y copia el logo donde esta vacio
    $qry = "SELECT DISTINCT CONCAT(destino,carpeta) AS ftp_dir, CONCAT(destino,carpeta,'/',cronjob_carpetas.logo_sodexo) AS destino, cronjob_carpetas.logo_sodexo
    FROM cronjob_carpetas INNER JOIN cronjob_modulos ON cronjob_carpetas.id_cronjob_modulos = cronjob_modulos.id 
    INNER JOIN cronjob_ftp ON cronjob_modulos.id_cronjob_ftp = cronjob_ftp.id WHERE cronjob_modulos.activado = 1";

    if (!$sql_result = $obj_conexion->query($qry)) {
        echo "Error en query.";

        //Envia email de  Error
        $mensajeSendMail = "Error en query";
        file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
        die();
    }

    if ($sql_result->num_rows != 0) {
        while ($rows_result = $sql_result->fetch_assoc()) {
            $ftp_dir = $rows_result["ftp_dir"];
            $destino = $rows_result["destino"];
            $logo_cliente = $rows_result["logo_sodexo"];

            //leer archivos.
            $dir = "ssh2.sftp://" . (int)$sftp . $ftp_dir;
            $files = scandir($dir);
            $cuentafile=0;
            foreach ($files as $file)
                $cuentafile++;
            if ($cuentafile == 2){

                copy(RUTAORIGENLOGOSCRUZVERDE . $logo_cliente, "ssh2.sftp://" . (int)$sftp_conn . $destino);

                $tipo = "Alerta";
                $comentario = "Diario: SFTP Carpeta [$destino] sin archivos, se copia [$logo_cliente]";
                graba_log($id_cronjob_modulos, $tipo, $comentario);

            }

        }
    }


    $tipo = "Info";
    $comentario = "Diario: SFTP Desconectado al ftp [" . $Usuario . "]";
    graba_log($id_cronjob_modulos, $tipo, $comentario);
} else {
    $id_cronjob_modulos = "00";
    $tipo = "Error";
    $comentario = "Diario: SFTP Sin Modulos Activados";
    graba_log($id_cronjob_modulos, $tipo, $comentario);

    //Envia email de alerta Error
    $mensajeSendMail = $comentario;
    file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
}

$tipo = "Tiempo";
$comentario = "Diario: SFTP FIN";
graba_log(0, $tipo, $comentario);

$obj_conexion->close();





function graba_log($id_cronjob_modulos, $tipo, $comentario)
{
    global $obj_conexion;
    $sql = "INSERT INTO cronjob_log (id_cronjob_modulos,fechahora,tipo,comentario) VALUES ($id_cronjob_modulos, NOW(), '$tipo', '$comentario')";
    $resultado = $obj_conexion->query($sql);
    return $resultado;
}


//Copia contenido
function sftp_copy($sftp_conn, $src_dir, $dst_dir, $LogoCliente, $id_cronjob_modulos, $xmlmodulo)
{

    $cdir = scandir($src_dir);
    if ($cdir == false || (count($cdir) == 2 && $cdir[0] == '.' && $cdir[1] == '..')) {
        $explode_src_dir = explode('/', $LogoCliente);
        $archivo =  $explode_src_dir[count($explode_src_dir) - 1];
        $resultadocopia = copy($LogoCliente, "ssh2.sftp://" . (int)$sftp_conn . $dst_dir . DIRECTORY_SEPARATOR . 'logo_cruzverde.jpg');
        // $resultadocopia = ftp_put($sftp_conn, $dst_dir . DIRECTORY_SEPARATOR . $archivo, $LogoCliente, FTP_BINARY);

        if ($resultadocopia == "") {
            $tipo = "Alerta";
            $comentario = "Diario: SFTP Sin contenido en carpeta [$dst_dir], configurada sin imagen";
            graba_log($id_cronjob_modulos, $tipo, $comentario);
        } else {
            $tipo = "Alerta";
            $comentario = "Diario: SFTP Sin contenido en carpeta [$dst_dir], se copia logo cruzverde";
            graba_log($id_cronjob_modulos, $tipo, $comentario);
        }
    } else {

        foreach ($cdir as $key => $value) {
            if (!in_array($value, array(".", ".."))) {
                if (is_file($src_dir . DIRECTORY_SEPARATOR . $value)) {
                    $en_rango_de_fecha = archivo_en_fecha($value);

                    if ($en_rango_de_fecha == '') {
                        $tipo = "Alerta";
                        $comentario = "Diario: SFTP Archivo fuera del rango de fechas [$value] no copiado";
                        graba_log($id_cronjob_modulos, $tipo, $comentario);
                    }
                    //echo $en_rango_de_fecha;
                    if ($en_rango_de_fecha){

                        //echo strpos($value, 'fullzona_');                       
                        
                        // $fue_copiado = ftp_put($sftp_conn, $dst_dir . "/" . $value, $src_dir . DIRECTORY_SEPARATOR . $value, FTP_BINARY);
                        $fue_copiado = copy($src_dir . DIRECTORY_SEPARATOR . $value,  "ssh2.sftp://" . (int)$sftp_conn . $dst_dir . "/" . $value);

                        $tipo = "Info";
                        $comentario = "Diario: SFTP Copia contenido [$value] ==> [$dst_dir]";
                        graba_log($id_cronjob_modulos, $tipo, $comentario);
                                
                    }
                }
            }
        }
    }
}

function archivo_en_fecha($filename)
{
    $filename_sin_extension = current(explode(".", $filename));

    $array_filename = explode('_', $filename_sin_extension);

    $desde = strtotime($array_filename[2]);
    $hasta = strtotime($array_filename[3]);

    $fecha = new DateTime("now", new DateTimeZone('America/Santiago'));
    $fecha_ts = strtotime($fecha->format("Y-m-d"));

    if ($fecha_ts >= $desde && $fecha_ts <= $hasta) {
        return true;
    } else {
        return false;
    }
}




function recursiveDelete($sftp, $dir, $id_cronjob_modulos) {
    
    $contents = scandir($dir);
    $ruta_destino = substr($dir, 13);
    array_splice($contents, 0,2);
    foreach ( $contents as $item ) {
        if ( is_dir("$dir/$item") && (substr($item, 0,1) != '.') ) {
            recursiveDelete($sftp, "$dir/$item", $id_cronjob_modulos);
        } else {
            ssh2_sftp_unlink($sftp, $ruta_destino . '/' . $item);
            $tipo = "Info";
            $comentario = "Diario: SFTP Borra contenido en [$ruta_destino/$item] ";
            graba_log($id_cronjob_modulos, $tipo, $comentario);
        }
    }
}
$fecha = new DateTime("now", new DateTimeZone('America/Santiago'));
echo  '<br>'. $fecha->format("Y-m-d H:i:s").' Fin';