<?php
//ejecuta por consola: /usr/local/bin/php /home/digitalboard/public_html/sub_dominios/paris/app/Cron_jobs/Paris_AutoPublicaCumpleanos_mensual.php


///error_reporting(E_ALL ^ E_WARNING);



include_once("/home/digitalboard/public_html/sub_dominios/paris/app/Config/config.php");

//Correos para envio de mail si existe un error
$destinosSendMAil= "aga@grupoclan.cl,af@grupoclan.cl,dop@grupoclan.cl";
$tituloSendMail = 'PARIS - Error de Publicacion';

//Datos para conectar a mysql se obtienen del Config
$server_db = LOCALSERVER;
$user_db = LOCALUSERDB;
$password_db = LOCALPASSDB;
$db_db = LOCALDB; 

//fecha y hora para centralizar fechas
$fecha = new DateTime("now", new DateTimeZone('America/Santiago'));
$fecha_hoy = $fecha->format('Y/m/j');
$numday = $fecha->format("N");
$hoy = $fecha->format("Ymd");
$current_time = $fecha->format('Y-m-d');
$date_parts = explode('-', $current_time);
$date_parts[2] = str_replace("0", "", $date_parts[2]);

// Conexion a mysql
$obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
if (!$obj_conexion) {
    echo "Error de Base de Daros";
    $comentario = "Error de Base de Datos";
    //Envia email de alerta Error
    $mensajeSendMail = $comentario;
    file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
    die();
}

//Consulta mysql
$select_qry = "SELECT config_modulos.id AS id, config_modulos.origen AS origen, config_modulos.destino AS destino, config_modulos.logo_default AS logo_default,
config_ftp.Ip AS Ip, config_ftp.Port AS Port, config_ftp.Usuario AS Usuario, config_ftp.Clave AS Clave, config_modulos.tienda AS tienda 
FROM (config_modulos JOIN config_ftp ON (config_modulos.id_config_ftp = config_ftp.id)) 
WHERE config_modulos.activado = 1 ORDER by Usuario, origen";


if (!$resultado = $obj_conexion->query($select_qry)) {
    echo "Error en query.";
    $comentario = "Error en la query";
    //Envia email de alerta Error
    $mensajeSendMail = $comentario;
    file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
    die();
}

//marca el inicio en logreport 
$tipo = "Tiempo";
$comentario = "INICIO";
graba_log(0, $tipo, $comentario);


if ($resultado->num_rows != 0) {
    
    $LastUsuario = '';
    while ($rows = $resultado->fetch_assoc()) {

        //Obtine datos de BD
        $Ip = $rows['Ip'];
        $Port = $rows['Port'];
        $Usuario = $rows['Usuario'];
        $Clave = $rows['Clave'];
        $Logo = RUTAORIGENLOGOS . $rows['logo_default'];
        $origen = RUTAORIGENCARPETASCRONJOB . $rows['origen'];
        $destino = $rows['destino'];
        $tienda = $rows['tienda'];
        $id_cronjob_modulos = $rows['id'];

        //si el usuario de connexion es  no vuelve a conectar
        if ($LastUsuario !== $Usuario) {

            if ($LastUsuario != '') {
                ftp_close($ftp_conn);
                $tipo = "Info";
                $comentario = "Desconectado al ftp [" . $LastUsuario . "]";
                graba_log($Lastid_cronjob_modulos, $tipo, $comentario);
            }

            //Conecta al FTP
            $ftp_conn = @ftp_connect($Ip, $Port);
            $login_res = ftp_login($ftp_conn, $Usuario, $Clave);

            if ($login_res == 1) {
                //Registro Log
                $tipo = "Info";
                $comentario = "Conectado al ftp [" . $Usuario . "]";
                graba_log($id_cronjob_modulos, $tipo, $comentario);
            } else {
                //Registro Log
                $tipo = "Error";
                $comentario = "Error al conectar al ftp [" . $Usuario . "]";
                graba_log($id_cronjob_modulos, $tipo, $comentario);

                //Envia email de alerta Error
                $mensajeSendMail = $comentario;
                file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
                die();
            }
        }

        if ($login_res) {

            // autopublicacion de cumpleaños
            echo "$tienda \r\n";
            echo file_get_contents("https://paris.digitalboard.app/".$tienda."_cumpleanos/makePublish?fromJOB=6");
            echo "\r\n";

            //marca el desconectado del ftp en logreport 
            $tipo = "Info";
            $comentario = "autopublica tienda [" . $tienda . "]";
            graba_log($id_cronjob_modulos, $tipo, $comentario);
        }

        $LastUsuario = $Usuario;
        $Lastid_cronjob_modulos = $id_cronjob_modulos;
    }


    //marca el desconectado del ftp en logreport 
    $tipo = "Info";
    $comentario = "Desconectado al ftp [" . $LastUsuario . "]";
    graba_log($id_cronjob_modulos, $tipo, $comentario);
    
    //desconecta del ftp
    ftp_close($ftp_conn);

} else {
    //marca ERROR en logreport
    $id_cronjob_modulos = "00";
    $tipo = "Error";
    $comentario = "Sin Modulos Activados";
    graba_log($id_cronjob_modulos, $tipo, $comentario);

    //Envia email de alerta Error
    $mensajeSendMail = $comentario;
    file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
}


//marca el fin en logreport 
$tipo = "Tiempo";
$comentario = "FIN";
graba_log(0, $tipo, $comentario);

//Cierra la conexion
$obj_conexion->close();


//Funciones

//Funcion que graba los Logreport
function graba_log($id_cronjob_modulos, $tipo, $comentario)
{
    global $obj_conexion;
    $sql = "INSERT INTO config_log (id_config_modulos,fechahora,tipo,comentario) VALUES ($id_cronjob_modulos, NOW(), '$tipo', 'Cumpleanos: $comentario')";
    $resultado = $obj_conexion->query($sql);
    return $resultado;
}

