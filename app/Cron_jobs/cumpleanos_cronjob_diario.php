<?php

error_reporting(E_ERROR | E_PARSE);

include_once("../../Config/config.php");

//$destinosSendMAil = "aga@grupoclan.cl,af@grupoclan.cl,dop@grupoclan.cl,oll@grupoclan.cl,lc@grupoclan.cl";
$tituloSendMail = 'CUMPLEAÑOS - Error Job Diario';

$server_db = LOCALSERVER;
$user_db = LOCALUSERDB;
$password_db = LOCALPASSDB;
$db_db = LOCALDB;


$fecha = new DateTime("now", new DateTimeZone('America/Santiago'));
$fecha_hoy = $fecha->format('Y/m/j');
$numday = $fecha->format("N");
$hoy = $fecha->format("Ymd");

$current_time = $fecha->format('Y-m-d');
$date_parts = explode('-', $current_time);
$date_parts[2] = str_replace("0", "", $date_parts[2]);


$obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
if(!$obj_conexion){
    echo "Error de Base de Daros";
    $comentario = "Semanal: de Base de Datos";
    //Envia email de alerta Error
    // $mensajeSendMail = $comentario;
    // file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
    die();
}

// $select_qry = "SELECT cronjob_modulos.id, cronjob_modulos.origen, cronjob_carpetas.carpeta
// AS carpeta, cronjob_modulos.destino, cronjob_carpetas.logo_sodexo, cronjob_ftp.Ip, cronjob_ftp.`Port`, cronjob_ftp.Usuario, cronjob_ftp.Clave 
// FROM cronjob_carpetas INNER JOIN cronjob_modulos ON cronjob_carpetas.id_cronjob_modulos = cronjob_modulos.id 
// INNER JOIN cronjob_ftp ON cronjob_modulos.id_cronjob_ftp = cronjob_ftp.id WHERE cronjob_modulos.activado = 1 order by Usuario, origen";

$select_qry = "SELECT `config_modulos`.`id` AS `id`,`config_modulos`.`origen` AS `origen`,`config_modulos`.`destino` AS `destino`,`config_modulos`.`logo_default` AS `logo_default`,`config_ftp`.`Ip` AS `Ip`,`config_ftp`.`Port` AS `Port`,`config_ftp`.`Usuario` AS `Usuario`,`config_ftp`.`Clave` AS `Clave`,`config_modulos`.`tienda` AS `tienda` from (`config_modulos` join `config_ftp` on((`config_modulos`.`id_config_ftp` = `config_ftp`.`id`))) where (`config_modulos`.`activado` = 1) order by Usuario, origen";

if (!$resultado = $obj_conexion->query($select_qry)) {
    echo "Error en query.";
    $comentario = "Semanal: Error en la query";
    //Envia email de alerta Error
    // $mensajeSendMail = $comentario;
    // file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
    die();
}

$tipo = "Tiempo";
$comentario = "Diario: INICIO";
graba_log(0, $tipo, $comentario);

if ($resultado->num_rows != 0) {
    $LastUsuario = '';

    while ($rows = $resultado->fetch_assoc()) {
        
        $Ip = $rows['Ip'];
        $Port = $rows['Port'];
        $Usuario = $rows['Usuario'];
        $Clave = $rows['Clave'];
        $LogoSodexo = RUTAORIGENLOGOS . $rows['logo_default'];

        $origen = RUTAORIGENCARPETASCRONJOB . $rows['origen'];
        // $carpeta = $rows['carpeta'];
        $destino = $rows['destino'];

        $id_cronjob_modulos = $rows['id'];

        //si la ip de connexion es la misma no vuelve a conectar para el mismo usuario
        //echo $LastUsuario .'!=='. $Usuario.'<br>';
        if ($LastUsuario !== $Usuario) {

            if ($LastUsuario != '') {
                ftp_close($ftp_conn);
                $tipo = "Info";
                $comentario = "Diario: Desconectado al ftp [" . $LastUsuario . "]";
                graba_log($Lastid_cronjob_modulos, $tipo, $comentario);
            }

            //Conecta al FTP
            $ftp_conn = @ftp_connect($Ip, $Port);
            $login_res = ftp_login($ftp_conn, $Usuario, $Clave);

            if ($login_res == 1) {
                //Registro Log
                $tipo = "Info";
                $comentario = "Diario: Conectado al ftp [" . $Usuario . "]";
                graba_log($id_cronjob_modulos, $tipo, $comentario);
            } else {
                //Registro Log
                $tipo = "Error";
                $comentario = "Diario: Error al conectar al ftp [" . $Usuario . "]";
                graba_log($id_cronjob_modulos, $tipo, $comentario);

                //Envia email de alerta Error
                // $mensajeSendMail = $comentario;
                // file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
            }
        }

        if ($login_res) {

            $destinos = explode('/', $destino);
            ftp_chdir($ftp_conn, '/');

            //crea la primera cliente 
            if (ftp_mkdir($ftp_conn, '/' . $destinos[1])) {
                $tipo = "Alerta";
                $comentario = "Diario: Crea carpeta  /$destinos[1]";
                graba_log($id_cronjob_modulos, $tipo, $comentario);
            }

            //si la segunda tienda
            if (isset($destinos[2])) {
                if (ftp_mkdir($ftp_conn, '/' . $destinos[1] . '/' . $destinos[2])) {
                    $tipo = "Alerta";
                    $comentario = "Diario: Crea carpeta  /$destinos[1]/$destinos[2]";
                    graba_log($id_cronjob_modulos, $tipo, $comentario);
                }

                //crea carpeta nombre de tienda
                if (ftp_mkdir($ftp_conn, '/' . $destinos[1] . '/' . $destinos[2] . '/' . $destinos[3])) {
                    $tipo = "Alerta";
                    $comentario = "Diario: Crea carpeta  /$destinos[1]/$destinos[2]/$destinos[3]";
                    graba_log($id_cronjob_modulos, $tipo, $comentario);
                }


            } 


            //Funcion para borrar carpetas
            ftp_delete_folder($ftp_conn, $destino, $id_cronjob_modulos);

            //Funcion para copiar carpetas o poner logo si no existe contenido
            ftp_copy($ftp_conn, $origen, $destino, $LogoSodexo, $id_cronjob_modulos);
        }

        $LastUsuario = $Usuario;
        $Lastid_cronjob_modulos = $id_cronjob_modulos;
        //$LastOrigen = $origen;
    }

    ftp_close($ftp_conn);
    $tipo = "Info";
    $comentario = "Diario: Desconectado al ftp [" . $LastUsuario . "]";
    graba_log($id_cronjob_modulos, $tipo, $comentario);
} else {
    $id_cronjob_modulos = "00";
    $tipo = "Error";
    $comentario = "Diario: Sin Modulos Activados";
    graba_log($id_cronjob_modulos, $tipo, $comentario);

    //Envia email de alerta Error
    // $mensajeSendMail = $comentario;
    // file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
}

$tipo = "Tiempo";
$comentario = "Diario: FIN";
graba_log(0, $tipo, $comentario);

$obj_conexion->close();



function graba_log($id_cronjob_modulos, $tipo, $comentario)
{
    global $obj_conexion;
    $sql = "INSERT INTO config_log (id_config_modulos,fechahora,tipo,comentario) VALUES ($id_cronjob_modulos, NOW(), '$tipo', '$comentario')";
    $resultado = $obj_conexion->query($sql);
    return $resultado;
}



function ftpConnect($Ip)
{
    echo 'ftp conectado ' . $Ip . PHP_EOL;
}

//Copia contenido
function ftp_copy($ftp_conn, $src_dir, $dst_dir, $LogoSodexo, $id_cronjob_modulos)
{
    $cdir = scandir($src_dir);
    if ($cdir == false || (count($cdir) == 2 && $cdir[0] == '.' && $cdir[1] == '..')) {
        $explode_src_dir = explode('/', $LogoSodexo);
        $archivo =  $explode_src_dir[count($explode_src_dir) - 1];
        $resultadocopia = ftp_put($ftp_conn, $dst_dir . DIRECTORY_SEPARATOR . $archivo, $LogoSodexo, FTP_BINARY);

        if ($resultadocopia == "") {
            $tipo = "Alerta";
            $comentario = "Diario: Sin contenido en carpeta [$dst_dir], configurada sin imagen";
            graba_log($id_cronjob_modulos, $tipo, $comentario);
        } else {
            $tipo = "Alerta";
            $comentario = "Diario: Sin contenido en carpeta [$dst_dir], se copia logo sodexo [$LogoSodexo]";
            graba_log($id_cronjob_modulos, $tipo, $comentario);
        }
    } else {
        foreach ($cdir as $key => $value) {
            if (!in_array($value, array(".", ".."))) {
                if (is_file($src_dir . DIRECTORY_SEPARATOR . $value)) {
                    $src_dir . DIRECTORY_SEPARATOR . $value . PHP_EOL;
                    $fue_copiado = ftp_put($ftp_conn, $dst_dir . "/" . $value, $src_dir . DIRECTORY_SEPARATOR . $value, FTP_BINARY);

                    $tipo = "Info";
                    $comentario = "Diario: Copia contenido [$value] ==> [$dst_dir]";
                    graba_log($id_cronjob_modulos, $tipo, $comentario);
                }
            }
        }
    }
}

//Borra contenido viejo
function ftp_delete_folder($ftp_conn, $dst_dir, $id_cronjob_modulos)
{
    ftp_chdir($ftp_conn, $dst_dir);
    $files = ftp_nlist($ftp_conn, ".");
    foreach ($files as $file) {
        if ($file != "." && $file != "..") {
            if (substr($file, 0, 4) != 'Gnr_') {
                $borra_contenido = ftp_delete($ftp_conn, $file);
                if ($borra_contenido == "1") {
                    $tipo = "Info";
                    $comentario = "Diario: Borra contenido viejo [$file] ==> [$dst_dir] ";
                    graba_log($id_cronjob_modulos, $tipo, $comentario);
                }
            } else {
                $tipo = "Alerta Gnr";
                $comentario = "Diario: Archivo Gnr_ agregado manual no borrado [$file] ==> [$dst_dir] ";
                graba_log($id_cronjob_modulos, $tipo, $comentario);
            }
        }
    }
}
