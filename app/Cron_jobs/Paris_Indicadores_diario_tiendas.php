<?php
//        /usr/local/bin/php /home/digitalboard/public_html/sub_dominios/paris/app/Cron_jobs/Paris_Indicadores_diario.php

//error_reporting(E_ALL ^ E_WARNING);

// Nos coloca en la carpeta del Cliente (en este caso: Paris)
define('BASE_TEMPLATE', '/home/digitalboard/public_html/sub_dominios/paris/app/webroot/files/paris');

//Carpeta donde deja er acrhivo
$carpeta = 'medioscompartidos';


// Nombre del módulo
$modulo = 'indicadores_tiendas';

// Índice para ordenar las imágenes creadas por módulo en la playlist
$bloque = 1;

// Array con elementos para borrar contenido
$borrar_archivos = ['indicadores_tiendas', 'bg_logo'];

// Tipografia
$RuiAbreuBold = BASE_TEMPLATE . '/templates/fonts/RuiAbreu-AzoSans-Bold.otf';
$RuiAbreuRegular = BASE_TEMPLATE . '/templates/fonts/RuiAbreu-AzoSans-Regular.otf';
$Calibri = BASE_TEMPLATE . '/templates/fonts/calibri.ttf';
$CalibriLight = BASE_TEMPLATE . '/templates/fonts/calibril.ttf';
$CalibriBold = BASE_TEMPLATE . '/templates/fonts/calibrib.ttf';
$MyriadProCond           = BASE_TEMPLATE . '/templates/fonts/otf/MyriadPro-Cond.otf';
$MyriadProSemiBoldCond   = BASE_TEMPLATE . '/templates/fonts/otf/MyriadPro SemiboldCond.otf';

// Todos los formatos de horas que se utilizan dentro del doc

$fecha = new DateTime("now", new DateTimeZone('America/Santiago'));
$fecha_hora = $fecha->format('ymd-His');
$current_time = $fecha->format('Y-m-d');
$today_ts     = strtotime($current_time);
$mesActual = $fecha->format('m');
$anioActual = $fecha->format('Y');
$fecha_hoy = $fecha->format('Y-m-d');
$today =  $fecha->format("Y-m-d-N");
$fecha_hoy_junta =  $fecha->format("Ymd");
$control = false;

$arrayMonth = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
$arrayDayFecha = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');
$arrayDay   = array('Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom');

//CLIMA

//Ciudades y su latitud y longitud para obtener el clima
$ubicacion[1]['ciudad'] = 'Arica';
$ubicacion[1]['lat'] = '-18.4724735';
$ubicacion[1]['lon'] = '-70.3241671';
$ubicacion[2]['ciudad'] = 'Antofagasta';
$ubicacion[2]['lat'] = '-23.8891525';
$ubicacion[2]['lon'] = '-71.6209002';
$ubicacion[3]['ciudad'] = 'La Serena';
$ubicacion[3]['lat'] = '-29.9059241';
$ubicacion[3]['lon'] = '-71.2794671';
$ubicacion[4]['ciudad'] = 'Temuco';
$ubicacion[4]['lat'] = '-38.7290843';
$ubicacion[4]['lon'] = '-72.6377404';
$ubicacion[5]['ciudad'] = 'Concepción';
$ubicacion[5]['lat'] = '-36.8260216';
$ubicacion[5]['lon'] = '-73.1030853';
$ubicacion[6]['ciudad'] = 'Valdivia';
$ubicacion[6]['lat'] = '-39.8341233';
$ubicacion[6]['lon'] = '-73.3060079';
$ubicacion[7]['ciudad'] = 'Valparaiso';
$ubicacion[7]['lat'] = '-33.0774531';
$ubicacion[7]['lon'] = '-71.107221';
$ubicacion[8]['ciudad'] = 'Santiago';
$ubicacion[8]['lat'] = '-33.458038';
$ubicacion[8]['lon'] = '-70.641161';
$ubicacion[9]['ciudad'] = 'Rancagua';
$ubicacion[9]['lat'] = '-34.1594625';
$ubicacion[9]['lon'] = '-70.7737186';


//CLIMA
for ($i = 1; $i <= 9; $i++) {

    // https://api.openweathermap.org/data/2.5/onecall?lat=-33.0774531&lon=-71.107221&exclude=hourly,alerts,minutely&appid=9194d97b22abf9e83b6de064199f700a&units=metric&lang=es
    $url_api = 'https://api.openweathermap.org/data/2.5/onecall?lat=' . $ubicacion[$i]['lat'] . '&lon=' . $ubicacion[$i]['lon'] . '&exclude=hourly,alerts,minutely&appid=9194d97b22abf9e83b6de064199f700a&units=metric&lang=es';
    $api_response = file_get_contents($url_api);
    $api_response = json_decode($api_response);


    $clima[$i]['id'] = $i;
    $clima[$i]['ciudad'] = $ubicacion[$i]['ciudad'];

    // For para guardar climas diarios
    for ($d = 1; $d <= 7; $d++) {
        $clima[$i][$d]['id_dias'] =  $d;

        // strtotime($current_time);
        $clima[$i][$d]['fecha'] = $api_response->daily[$d - 1]->dt;
        // $clima[$i]['dias']['id_dias'] =  $d;

        $main = $api_response->daily[$d - 1]->weather[0]->main;
        $description = $api_response->daily[$d - 1]->dt;

        switch ($main) {
            case 'Clear':
                $clima[$i][$d]['imagen'] = 'Clear.png';
                break;
            case 'Clouds':
                $clima[$i][$d]['imagen']  = $description == 'nubes dispersas' ? 'Clouds_broken.png' : 'Clouds.png';
                $clima[$i][$d]['imagen']  = $description == 'muy nuboso' ? 'Clouds_cloud.png' : $clima[$i][$d]['imagen'];
                break;
            case 'Drizzle':
                $clima[$i][$d]['imagen'] = 'Drizzle.png';
                break;
            case 'Rain':
                $clima[$i][$d]['imagen']  = 'Rain.png';
                break;
            case 'Snow':
                $clima[$i][$d]['imagen']  = 'Snow.png';
                break;
            case 'Thunderstorm':
                $clima[$i][$d]['imagen']  = 'Thunderstorm.png';
                break;
            case 'Mist':
                $clima[$i][$d]['imagen']  = 'Clouds_cloud.png';
                break;
            default:
                $clima[$i][$d]['imagen']  = 'Clouds_broken.png';
                break;
        }

        $clima[$i][$d]['minima'] = round($api_response->daily[$d - 1]->temp->min);
        $clima[$i][$d]['maxima'] = round($api_response->daily[$d - 1]->temp->max);
    }
}


//Santoral
$jsonFileSantoral    = file_get_contents('/home/digitalboard/public_html/sub_dominios/paris/app/webroot/files/paris/templates/json/indicadores/santoral-data.json');
$jsonDataSantoral    = json_decode($jsonFileSantoral);

foreach ($jsonDataSantoral as &$slide) {
    // Creacion de array.
    $slidesSantoral[$a] = $slide;
    $a++;
}

foreach ($slidesSantoral as $dates) {

    // Se deja cada dato en una variable y se agrega un cero si es menor a diez para poder comparar las fechas
    $dia = $dates->dia;
    $dia = $dia < 10 ? sprintf("%02d", $dia) : $dia;
    $mes = $dates->mes;
    $mes = $mes < 10 ? sprintf("%02d", $mes) : $mes;
    $fechaSantoral = $anioActual . '-' . $mes . '-' . $dia;

    $contador = 1;

    foreach ($period as $day) {

        $compFecha = $day->format('Y-m-d');


        if ($compFecha == $fechaSantoral) {

            $santoral[$contador]['fecha'] = $compFecha;
            $santoral[$contador]['santoral'] = $dates->santoral;
        }
        $contador++;
    }
}


// CREAR IMAGEN /////////////////////////////////////////////////////////////////////////////////////////
$pags = ['01', '02', '03'];


$directorio = BASE_TEMPLATE . '/' . 'templates_images/' . $carpeta;
$files = glob($directorio . '/*');
foreach ($files as $file) {
    foreach ($borrar_archivos as $n_arch) {
        if (is_file(($file))) {
            if (strpos($file, $n_arch) !== false) {
                unlink($file);
            }
        }
    }
}

// Inicialización función coordenadas
$control['x'] = 800; // mueve en el eje X los textos
$control['y'] = 770; // mueve en el eje Y los textos
$i = 0;

foreach ($pags as $paginas => $pag) {

    // Crea la imagen de fondo
    $imageHandler       = imagecreatetruecolor(1920, 1080);
    $bgImage            = imagecreatefromjpeg(BASE_TEMPLATE . '/templates/img/indicadores.jpg');
    imagecopy($imageHandler, $bgImage, 0, 0, 0, 0, 1920, 1080);
    $colors['blanco']    = imagecolorallocate($imageHandler, 255, 255, 255);

    // CLIMA /////////////////////////////////////////////////////////////////////////////////////////
    // Primera columna

    // define los ejes de x e y
    $control['x'] = 800;
    $control['y'] = 780;
    $i++;

    $imagenTiempo = imagecreatefrompng(BASE_TEMPLATE . '/templates/img/clima/' . $clima[$i]['imagen']);
    if ($clima[$i]['ciudad'] == 'Arica') {
        imagefttext($imageHandler, 35, 0, $control['x'] + 40, $control['y'] - 20, $colors['blanco'], $CalibriLight, $clima[$i]['ciudad']);
    }
    if (($clima[$i]['ciudad'] == 'Temuco') || ($clima[$i]['ciudad'] == 'Valparaiso')) {
        imagefttext($imageHandler, 35, 0, $control['x'], $control['y'] - 20, $colors['blanco'], $CalibriLight, $clima[$i]['ciudad']);
    }
    imagecopyresized($imageHandler, $imagenTiempo, $control['x'], $control['y'], 0, 0, 160, 140, 190, 170);
    imagefttext($imageHandler, 40, 0, $control['x'], $control['y'] + 180, $colors['blanco'], $CalibriBold, $clima[$i]['minima'] . '° - ' . $clima[$i]['maxima'] . '°');


    // Segunda columna
    $control['x'] = 1200;
    $control['y'] = 780;
    $i++;

    $imagenTiempo = imagecreatefrompng(BASE_TEMPLATE . '/templates/img/clima/' . $clima[$i]['imagen']);
    if ($clima[$i]['ciudad'] == 'Antofagasta') {
        imagefttext($imageHandler, 35, 0, $control['x'] - 10, $control['y'] - 20, $colors['blanco'], $CalibriLight, $clima[$i]['ciudad']);
    } else if (($clima[$i]['ciudad'] == 'Santiago') || ($clima[$i]['ciudad'] == 'Concepción')) {
        imagefttext($imageHandler, 35, 0, $control['x'], $control['y'] - 20, $colors['blanco'], $CalibriLight, $clima[$i]['ciudad']);
    }
    imagecopyresized($imageHandler, $imagenTiempo, $control['x'] - 10, $control['y'] - 20, 0, 0, 160, 140, 190, 170);
    imagefttext($imageHandler, 40, 0, $control['x'], $control['y'] + 180, $colors['blanco'], $CalibriBold, $clima[$i]['minima'] . '° - ' . $clima[$i]['maxima'] . '°');


    // Tercera columna
    $control['x'] = 1600;
    $control['y'] = 780;
    $i++;

    $imagenTiempo = imagecreatefrompng(BASE_TEMPLATE . '/templates/img/clima/' . $clima[$i]['imagen']);
    if ($clima[$i]['ciudad'] == 'La Serena') {
        imagefttext($imageHandler, 35, 0, $control['x'] + 5, $control['y'] - 20, $colors['blanco'], $CalibriLight, $clima[$i]['ciudad']);
    } else if (($clima[$i]['ciudad'] == 'Rancagua') || ($clima[$i]['ciudad'] == 'Valdivia')) {
        imagefttext($imageHandler, 35, 0, $control['x'] + 5, $control['y'] - 20, $colors['blanco'], $CalibriLight, $clima[$i]['ciudad']);
    }
    imagecopyresized($imageHandler, $imagenTiempo, $control['x'], $control['y'], 0, 0, 160, 140, 190, 170);
    imagefttext($imageHandler, 40, 0, $control['x'], $control['y'] + 180, $colors['blanco'], $CalibriBold, $clima[$i]['minima'] . '° - ' . $clima[$i]['maxima'] . '°');


    // SANTORAL /////////////////////////////////////////////////////////////////////////////////////////

    // ajusta los santorales en el eje y

    $xS = 540;
    $xAux = 370;
    for ($contSanto = 1; $contSanto <= 7; $contSanto++) {
        $yS = 975;
        $fechaSantos = explode("-", $santoral[$contSanto]['fecha']);
        $CopiaSanto = $santoral[$contSanto]['santoral'];
        $cont = 0;
        $arrayEquis = 0;

        // crea bloque de texto
        $CopiaSanto = makeTextBlock($CopiaSanto, $MyriadProSemiBoldCond, 20, 200);

        // Cuenta lineas en el bloque
        foreach ($CopiaSanto as $line) {
            $cont++;
        }

        // Imprime por linea en el jpg

        imagefttext($imageHandler, 30, 0, $xS, $yS, $colors['blanco'], $MyriadProSemiBoldCond, $fechaSantos[2]);
        foreach ($CopiaSanto as $line) {
            if ($cont == 1) {
                // Imprime en el jpg
                $arrayEquis = centerText($line, $MyriadProSemiBoldCond, 30, 400);
                imagefttext($imageHandler, 20, 0, $xAux + $arrayEquis[0], $yS + 40, $colors['blanco'], $MyriadProSemiBoldCond, $line);
            }
            if ($cont >= 2) {
                // Imprime en el jpg
                // imagettftextSp($image, $size (tamaño), $angle (ángulo de inclinación), $x (mover en eje x), $y (mover en eje y), $color, $font, $text, $spacing)
                $arrayEquis = centerText($line, $MyriadProSemiBoldCond, 30, 400);
                // echo $arrayEquis[0] . '<br>';
                // echo '<pre>' . var_dump($arrayEquis, true) . ' - ' . $line . '</pre>';
                imagefttext($imageHandler, 20, 0, $xAux  + $arrayEquis[0], $yS + 40, $colors['blanco'], $MyriadProSemiBoldCond, $line);
                // echo "- 2 - " . $line;
                $yS = $yS + 30;
            }
        }
        $xS += 195;
        $xAux += 210;
    }


    // HORA /////////////////////////////////////////////////////////////////////////////////////////
    // Pone la hora
    $date_parts = split('-', $today);
    // Define el texto de la fecha en formato largo
    $stringDate = $arrayMonth[$date_parts[1] - 1];
    imagettftextSp($imageHandler, 25, 0, 152, 1035, $colors['blanco'], $MyriadProCond, $stringDate, 4);


    // Crea la imagen y la guarda en la carpeta correspondiente
    mkdir(BASE_TEMPLATE . '/' . 'templates_images' . '/' . $carpeta);
    imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images' . '/' . $carpeta . '/' . $bloque . $pag . '_' . $modulo . '_' . $fecha_hoy_junta . '_' . $fecha_hoy_junta . '.jpg', 75);

    // Crea imagenes en previsualizacion
    imagejpeg($imageHandler, BASE_TEMPLATE . '/' . 'templates_images_test' . '/' . $carpeta . '/' . $bloque . $pag . '_' . $modulo . '.jpg', 75);
}



// Libera memoria de sistema
imagedestroy($bgImage);
imagedestroy($imageHandler);


// funcion para centrar texto
function centerText($text, $font, $size, $xi)
{
    $box = ImageTTFBBox($size, 0, $font, $text);
    $xr = abs(max($box[2], $box[4]));
    $x = intval(($xi - $xr) / 2);

    return array($x);
}

// funcion para crear bloque de texto
function makeTextBlock($text, $fontfile, $fontsize, $width)
{
    $words = explode(' ', $text);
    $lines = array($words[0]);
    $currentLine = 0;
    for ($i = 1; $i < count($words); $i++) {
        $lineSize = imagettfbbox($fontsize, 0, $fontfile, $lines[$currentLine] . ' ' . $words[$i]);
        if ($lineSize[2] - $lineSize[0] < $width) {
            $lines[$currentLine] .= ' ' . $words[$i];
        } else {
            $currentLine++;
            $lines[$currentLine] = $words[$i];
        }
    }

    return $lines;
}

// funcion para pasar texto a la imagen
function imagettftextSp($image, $size, $angle, $x, $y, $color, $font, $text, $spacing)
{
    if ($spacing == 0) {
        imagettftext($image, $size, $angle, $x, $y, $color, $font, $text);
    } else {
        $temp_x = $x;
        for ($i = 0; $i < strlen($text); $i++) {
            $bbox = imagettftext($image, $size, $angle, $temp_x, $y, $color, $font, $text[$i]);
            $temp_x += $spacing + ($bbox[2] - $bbox[0]);
        }
    }
}
