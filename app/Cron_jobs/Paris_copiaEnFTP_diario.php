<?php

//ejecuta por consola: /usr/local/bin/php /home/digitalboard/public_html/sub_dominios/paris/app/Cron_jobs/Paris_copiaEnFTP_diario.php ok

if (isset($argv[1]) and $argv[1]=="ok") {

    //$tienda = $argv[1];
    //echo $tienda;

    ///error_reporting(E_ALL ^ E_WARNING);



    include_once("/home/digitalboard/public_html/sub_dominios/paris/app/Config/config.php");

    //Correos para envio de mail si existe un error
    $destinosSendMAil= "aga@grupoclan.cl,af@grupoclan.cl,dop@grupoclan.cl";
    $tituloSendMail = 'PARIS - Error de Publicacion';

    //Datos para conectar a mysql se obtienen del Config
    $server_db = LOCALSERVER;
    $user_db = LOCALUSERDB;
    $password_db = LOCALPASSDB;
    $db_db = LOCALDB; 

    //fecha y hora para centralizar fechas
    $fecha = new DateTime("now", new DateTimeZone('America/Santiago'));
    $fecha_hoy = $fecha->format('Y/m/j');
    $numday = $fecha->format("N");
    $hoy = $fecha->format("Ymd");
    $current_time = $fecha->format('Y-m-d');
    $date_parts = explode('-', $current_time);
    $date_parts[2] = str_replace("0", "", $date_parts[2]);

    // Conexion a mysql
    $obj_conexion = new mysqli($server_db, $user_db, $password_db, $db_db) or die("db_error");
    if (!$obj_conexion) {
        echo "Error de Base de Daros";
        $comentario = "Error de Base de Datos";
        //Envia email de alerta Error
        $mensajeSendMail = $comentario;
        file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
        die();
    }

    //Consulta mysql
    $select_qry = "SELECT config_modulos.id AS id, config_modulos.origen AS origen, config_modulos.destino AS destino, config_modulos.logo_default AS logo_default,
    config_ftp.Ip AS Ip, config_ftp.Port AS Port, config_ftp.Usuario AS Usuario, config_ftp.Clave AS Clave, config_modulos.tienda AS tienda 
    FROM (config_modulos JOIN config_ftp ON (config_modulos.id_config_ftp = config_ftp.id)) 
    WHERE config_modulos.activado = 1 ORDER by Usuario, origen";


    if (!$resultado = $obj_conexion->query($select_qry)) {
        echo "Error en query.";
        $comentario = "Error en la query";
        //Envia email de alerta Error
        $mensajeSendMail = $comentario;
        file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
        die();
    }

    //marca el inicio en logreport 
    $tipo = "Tiempo";
    $comentario = "INICIO";
    graba_log(0, $tipo, $comentario);


    if ($resultado->num_rows != 0) {
        
        $LastUsuario = '';
        while ($rows = $resultado->fetch_assoc()) {

            //Obtine datos de BD
            $Ip = $rows['Ip'];
            $Port = $rows['Port'];
            $Usuario = $rows['Usuario'];
            $Clave = $rows['Clave'];
            $Logo = RUTAORIGENLOGOS . $rows['logo_default'];
            $origen = RUTAORIGENCARPETASCRONJOB . $rows['origen'];
            $destino = $rows['destino'];
            $id_cronjob_modulos = $rows['id'];

            //si el usuario de connexion es  no vuelve a conectar
            if ($LastUsuario !== $Usuario) {

                if ($LastUsuario != '') {
                    ftp_close($ftp_conn);
                    $tipo = "Info";
                    $comentario = "Desconectado al ftp [" . $LastUsuario . "]";
                    graba_log($Lastid_cronjob_modulos, $tipo, $comentario);
                }

                //Conecta al FTP
                $ftp_conn = @ftp_connect($Ip, $Port);
                $login_res = ftp_login($ftp_conn, $Usuario, $Clave);

                if ($login_res == 1) {
                    //Registro Log
                    $tipo = "Info";
                    $comentario = "Conectado al ftp [" . $Usuario . "]";
                    graba_log($id_cronjob_modulos, $tipo, $comentario);
                } else {
                    //Registro Log
                    $tipo = "Error";
                    $comentario = "Error al conectar al ftp [" . $Usuario . "]";
                    graba_log($id_cronjob_modulos, $tipo, $comentario);

                    //Envia email de alerta Error
                    $mensajeSendMail = $comentario;
                    file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
                    die();
                }
            }

            if ($login_res) {

                $destinos = explode('/', $destino);
                ftp_chdir($ftp_conn, '/');

                //crea la primera cliente 
                if (ftp_mkdir($ftp_conn, '/' . $destinos[1])) {
                    $tipo = "Alerta";
                    $comentario = "Crea carpeta  /$destinos[1]";
                    graba_log($id_cronjob_modulos, $tipo, $comentario);
                }

                //si la segunda tienda
                if (isset($destinos[2])) {
                    if (ftp_mkdir($ftp_conn, '/' . $destinos[1] . '/' . $destinos[2])) {
                        $tipo = "Alerta";
                        $comentario = "Crea carpeta  /$destinos[1]/$destinos[2]";
                        graba_log($id_cronjob_modulos, $tipo, $comentario);
                    }

                    //crea carpeta nombre de tienda
                    if (ftp_mkdir($ftp_conn, '/' . $destinos[1] . '/' . $destinos[2] . '/' . $destinos[3])) {
                        $tipo = "Alerta";
                        $comentario = "Crea carpeta  /$destinos[1]/$destinos[2]/$destinos[3]";
                        graba_log($id_cronjob_modulos, $tipo, $comentario);
                    }
                }

                //Funcion para borrar carpetas
                ftp_delete_folder($ftp_conn, $destino, $id_cronjob_modulos);

                //Funcion para copiar carpetas o poner logo si no existe contenido
                ftp_copy($ftp_conn, $origen, $destino, $Logo, $id_cronjob_modulos,$hoy);
            }

            $LastUsuario = $Usuario;
            $Lastid_cronjob_modulos = $id_cronjob_modulos;

            //Verifica si la carpeta en ftp esta vacia copia el logo.
            verifica_carpeta_vacia($ftp_conn, $destino, $Logo, $id_cronjob_modulos);
        }

        //marca el desconectado del ftp en logreport 
        $tipo = "Info";
        $comentario = "Desconectado al ftp [" . $LastUsuario . "]";
        graba_log($id_cronjob_modulos, $tipo, $comentario);
        
        //desconecta del ftp
        ftp_close($ftp_conn);

    } else {
        //marca ERROR en logreport
        $id_cronjob_modulos = "00";
        $tipo = "Error";
        $comentario = "Sin Modulos Activados ($tienda)";
        graba_log($id_cronjob_modulos, $tipo, $comentario);

        //Envia email de alerta Error
        $mensajeSendMail = $comentario;
        file_get_contents("https://digitalboard.cl/api/SendMail/?destinos=".urlencode($destinosSendMAil)."&titulo=".urlencode($tituloSendMail)."&mensaje=".urlencode($mensajeSendMail));
    }

    
    //marca el fin en logreport 
    $tipo = "Tiempo";
    $comentario = "FIN";
    graba_log(0, $tipo, $comentario);
    
    //Cierra la conexion
    $obj_conexion->close();
}

//Funciones

//Funcion que graba los Logreport
function graba_log($id_cronjob_modulos, $tipo, $comentario)
{
    global $obj_conexion;
    $sql = "INSERT INTO config_log (id_config_modulos,fechahora,tipo,comentario) VALUES ($id_cronjob_modulos, NOW(), '$tipo', 'Diario: $comentario')";
    $resultado = $obj_conexion->query($sql);
    return $resultado;
}


//Funcion Copia el contenido al FTP
function ftp_copy($ftp_conn, $src_dir, $dst_dir, $Logo, $id_cronjob_modulos, $hoy)
{
    //escanea el directorio de origen
    $cdir = scandir($src_dir);

    //Lee la carpeta
    foreach ($cdir as $key => $value) {
        //deja fuera el punto y el punto punto
        if (!in_array($value, array(".", ".."))) {
            //si existe el archivo
            if (is_file($src_dir . DIRECTORY_SEPARATOR . $value)) {

                //Extrae el nombre sin extension 00_nombre_20200101_20200101
                $filename = current(explode(".", $value));

                //Extrae la fecha desde hasta del nombre de los archivos 20200101_20200101
                $filename = substr($filename, -17);

                //separa el la facha incluidas en el nombre del archivo en 2 variables: $desde $hasta
                $array_filename = explode('_', $filename);
                
                //convierte las fechas a numeros TS para poder compararlas 
                $desde = strtotime($array_filename[0]);
                $hasta = strtotime($array_filename[1]);
                $hoy_ts = strtotime($hoy);

                //compara si hoy estamos en el rango de fechas valido 
                if ($hoy_ts >= $desde && $hoy_ts <= $hasta) {
                    //si cumple copia el archivo
                    $src_dir . DIRECTORY_SEPARATOR . $value . PHP_EOL;
                    $fue_copiado = ftp_put($ftp_conn, $dst_dir . "/" . $value, $src_dir . DIRECTORY_SEPARATOR . $value, FTP_BINARY);

                    //marca en logreport 
                    $tipo = "Info";
                    $comentario = "Copia contenido [$value] ==> [$dst_dir]";
                    graba_log($id_cronjob_modulos, $tipo, $comentario);
                } else {
                    //marca en logreport 
                    $tipo = "Alerta";
                    $comentario = "Archivo no copiado fuera del rango de fechas [$value]";
                    graba_log($id_cronjob_modulos, $tipo, $comentario);
                }
            }
        }
    }
}

//Funcion Borra contenido en FTP
function ftp_delete_folder($ftp_conn, $dst_dir, $id_cronjob_modulos)
{
    ftp_chdir($ftp_conn, $dst_dir);
    $files = ftp_nlist($ftp_conn, ".");
    foreach ($files as $file) {
        if ($file != "." && $file != "..") {
            if (substr($file, 0, 4) != 'Gnr_') {
                $borra_contenido = ftp_delete($ftp_conn, $file);
                if ($borra_contenido == "1") {
                    $tipo = "Info";
                    $comentario = "Borra contenido viejo [$file] ==> [$dst_dir] ";
                    graba_log($id_cronjob_modulos, $tipo, $comentario);
                }
            } else {
                $tipo = "Alerta Gnr";
                $comentario = "Archivo Gnr_ agregado manual no borrado [$file] ==> [$dst_dir] ";
                graba_log($id_cronjob_modulos, $tipo, $comentario);
            }
        }
    }
}

//Funcion verifica si la carpeta en ftp esta vacia copia el logo.
function verifica_carpeta_vacia($ftp_conn, $dst_dir, $Logo, $id_cronjob_modulos)
{
    $cont=0;
    ftp_chdir($ftp_conn, $dst_dir);
    $files = ftp_nlist($ftp_conn, ".");
    //var_dump($files);
    foreach ($files as $file) {
        if ($file != "." && $file != "..") {
            $cont++;
        }
    }
    if($cont==0){
        $explode_src_dir = explode('/', $Logo);
        $archivo_logo =  $explode_src_dir[count($explode_src_dir) - 1];
        $resultadocopia = ftp_put($ftp_conn, $dst_dir . DIRECTORY_SEPARATOR . $archivo_logo, $Logo, FTP_BINARY);
        if ($resultadocopia == "") {
            $tipo = "Alerta";
            $comentario = "Sin contenido en carpeta [$dst_dir], configurada sin imagen";
            graba_log($id_cronjob_modulos, $tipo, $comentario);
        } else {
            $tipo = "Alerta";
            $comentario = "Sin contenido en carpeta [$dst_dir], se copia logo [$archivo_logo]";
            graba_log($id_cronjob_modulos, $tipo, $comentario);
        }
    }
}
